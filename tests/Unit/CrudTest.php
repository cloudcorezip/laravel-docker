<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Mahasiswa;
use App\Programs;
use App\Applications;
use App\Person_in_charges;
use App\Application_statuses;
class CrudTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    // use DatabaseTransactions;
    public function testCreate() {                 
        $program = new Programs();
        $program->name = 'Test';
        $program->description = 'bdsjf';
        $program->price ='15000';
        $program->discount ='10';
        $program->institution_id=1;
        $program->save();
        $this->assertTrue(True);   

    }
    public function testUpdate() {                 
        $program = Programs::find(1);
        $program->name = 'Contoh';
        $program->description = 'bdsjf';
        $program->price ='15000';
        $program->discount ='10';
        $program->institution_id=1;
        $program->save();
        $this->assertTrue(True);  
    }

    public function testDelete() {                 
        $program = Programs::destroy(5);
        $this->assertTrue(True);  
    }
    public function testDetail() {
    	$program = Programs::find(1);
        $model_program = $program->name;
        $this->assertEquals('Contoh',$model_program);

    }
    public function testDetail_pengajuan()
    {
        $applications = Applications::find(1);
        $programs = Programs::all();
        $statuses = Application_statuses::all();
        $charges = Person_in_charges::all();
        $model_program = $applications->programs->name;
        $this->assertEquals('Contoh',$model_program);
    }
}
