<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Http\Controllers\PerhitunganController;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
    }
    public function testPertambahan()
    {
    	$perhitungan =  new PerhitunganController();
    	$hasil_perhitungan = $perhitungan->pertambahan(2,2);
    	$this->assertEquals(4,$hasil_perhitungan);
    }
    

}
