@extends('layouts.blank')

@push('stylesheets')
<link href="{{ asset("css/animate.min.css") }}" rel="stylesheet">
<link href="{{ asset("css/custom.css") }}" rel="stylesheet">
<link href="{{ asset("css/icheck/flat/green.css") }}" rel="stylesheet" />
<link href="{{ asset("css/datatables/tools/css/dataTables.tableTools.css") }}" rel="stylesheet">
@endpush

@section('main_container')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        @if (Auth::user()->hasRole('Admin Cluever'))
                        <h3 class="text-center">Selamat datang di Cluever</h3>
                        <div class="clearfix"></div>
                        @endif
                        @if (Auth::user()->hasRole('Admin Institusi'))
                        <h3 class="text-center">Selamat datang di Institusi</h3>
                        <div class="clearfix"></div>
                        @endif
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset("js/nicescroll/jquery.nicescroll.min.js") }}"></script>
<script src="{{ asset("js/icheck/icheck.min.js") }}"></script>
<script src="{{ asset("js/custom.js") }}"></script>
<script src="{{ asset("js/datatables/js/jquery.dataTables.js") }}"></script>
<script src="{{ asset("js/datatables/tools/js/dataTables.tableTools.js") }}"></script>
@endpush
