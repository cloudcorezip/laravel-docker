<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Cluever | Smart Education Payment</title>
    
    <!-- Bootstrap -->
    <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset("css/gentelella.min.css") }}" rel="stylesheet">

</head>

<body class="login" style="background-image: url('https://images.unsplash.com/photo-1532012197267-da84d127e765?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80')">
<div>
    <div class="login_wrapper">
        <div class="animate form login_form">
			<h1>
				<div align="center">
					<img style="width: 50%; display: block;" src="{{ asset("images/logoHeader.png") }}" alt="">
				</div>
			</h1>
			<h5 class="text-center" style="color: #fff;">Smart Education Payment</h5>
            <section class="login_content bg-white" style="border-radius: 8px; padding-left: 12px; padding-right: 12px; margin-top: 32px;">
				{!! BootForm::open(['url' => url('/login'), 'method' => 'post']) !!}


				<h1>Login</h1>
			
				{!! BootForm::text('nik', 'NIK', old('nik'), ['placeholder' => 'NIK', 'afterInput' => '<span>test</span>'] ) !!}
			
				{!! BootForm::password('password_digest', 'Password', ['placeholder' => 'Password']) !!}
				
				<div>
					{!! BootForm::submit('Masuk', ['class' => 'btn btn-default submit']) !!}
					<a class="reset_pass" href="{{  url('/password/reset') }}">Lupa Kata Sandi ?</a>
				</div>
                    
				<div class="clearfix"></div>
                    
				{!! BootForm::close() !!}
            </section>
			<div> 
				<p class="text-center" style="margin-top: 32px; color: #fff;">©2019</p>
			</div>
        </div>
    </div>
</div>
</body>
</html>