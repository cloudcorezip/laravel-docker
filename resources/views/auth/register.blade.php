<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Gentellela Alela! | </title>
    
    <!-- Bootstrap -->
    <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset("css/gentelella.min.css") }}" rel="stylesheet">

</head>

<body class="login">
<div class="login_wrapper">
    <div class="animate form login_form">
        <section class="login_content">
			{!! BootForm::open(['url' => url('register'), 'method' => 'post']) !!}
			
			<h1>Daftar</h1>

			{!! BootForm::text('name', 'Name', old('name'), ['placeholder' => 'Full Name']) !!}
			{!! BootForm::text('username', 'Username', old('name'), ['placeholder' => 'Username']) !!}
			{!! BootForm::text('phone_no', 'Telephone', old('phone_no'), ['placeholder' => 'Telephone']) !!}

			{!! BootForm::email('email', 'Email', old('email'), ['placeholder' => 'Email']) !!}

			{!! BootForm::password('password', 'Password', ['placeholder' => 'Password']) !!}

			{!! BootForm::password('password_confirmation', 'Password confirmation', ['placeholder' => 'Confirmation']) !!}

			<!-- {!! BootForm::text('role', 'Role', old('role'), ['placeholder' => 'Role']) !!} -->

			<select class="select2_single form-control" name="role" tabindex="-1">
                <option>Pilih </option>
                <option value="Admin">Admin</option>
                <option value="CLuever">CLuever</option>
                <option value="Educator">Educator</option>
            </select>
            <br>

			{!! BootForm::submit('Register', ['class' => 'btn btn-default']) !!}
		   
			<div class="clearfix"></div>
			
			<div class="separator">
				<p class="change_link">Sudah punya akun?
					<a href="{{ url('/login') }}" class="to_register"> Masuk di sini </a>
				</p>
				
				<div class="clearfix"></div>
				<br />
				
				<div> 
						<h1><div align="center">
							<img style="width: 50%; display: block;" src="{{ asset("images/logo.png") }}" alt=""></div></h1>
						<p>©2019</p>
					</div>
			</div>
			{!! BootForm::close() !!}
        </section>
    </div>
</div>
</body>
</html>