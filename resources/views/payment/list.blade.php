@extends('layouts.list')

@section('title')
<h3 class="text-center">Daftar Pembayaran</h3>
@endsection

@section('add-button')
<a href="{{ route('payment.create') }}" 

    class="btn btn-success new-data-btn"
>
    Tambah Pembayaran Baru
</a>
@endsection

@section('content')
<table id="example" class="table table-striped responsive-utilities jambo_table">
    <thead>
        <tr class="headings">
            <th>No</th>
            <th>Nama Program</th>
            <th class="text-right">Nominal</th>
            <th>Status</th>
            <th class=" no-link last"><span class="nobr">Action</span>
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach($payments as $index => $payment)
        
        <tr class="odd pointer">
            <td>{{ $index + 1 }}</td>
            <td>{{ $payment->program->name }}</td>
            <td class="text-right">Rp.{{ $payment->amount ? number_format($payment->amount, 0, ',','.') : 0 }}</td>
            <td>
                <p class="label label-{{ $payment->status->id == 1 ? 'success' : 'danger' }}">{{ $payment->status->name }}</p>
            </td>
            <td class=" last">
                <a href="/payment/{{ $payment->id }}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Detail </a>
                <a href="/payment/edit/{{ $payment->id }}" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                <a
                    href="#"
                    onclick="var c = confirm('Apakah Anda yakin ingin menghapus pembayaran ini?'); if (c) {$('#delete-payment-{{ $payment->id }}').submit()}"
                    class="btn btn-danger btn-xs"
                >
                    <i class="fa fa-trash"></i> Delete
                </a>
                <form id="delete-payment-{{ $payment->id }}" method="POST" action="/payment/delete/{{ $payment->id }}">
                    {{ csrf_field() }}
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
