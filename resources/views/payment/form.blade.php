@extends('layouts.form')

@section('title')
<h3 class="text-center">Pembayaran Baru</h3>
@endsection

@section('form-open')
    @if ($payment->id)
    {!!
        Form::open([
            'url' => '/payment/update/' . $payment->id,
            'class' => 'form-horizontal form-label-left',
            'method' => 'PUT'
        ])
    !!}
    @else
    {!!
        Form::open([
            'url' => '/payment/store',
            'class' => 'form-horizontal form-label-left',
            'method' => 'POST'
        ])
    !!}
    @endif
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label" for="details">Rincian Pembayaran</label>
            <textarea name="details" placeholder="Rincian Pembayaran" class="form-control" rows="3">{{ old('details', $payment->details) }}</textarea>
            @if($errors->has('details'))
            <div class="text-danger">
                {{ $errors->first('details')}}
            </div>
            @endif
        </div>
        <div class="form-group">
            <label class="control-label" for="amount">Nominal Pembayaran</label>
            <input type="number" name="amount" placeholder="Nominal Pembayaran" class="form-control" value="{{ old('amount', $payment->amount) }}">
            @if($errors->has('details'))
            <div class="text-danger">
                {{ $errors->first('details')}}
            </div>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label" for="type">Jenis Pembayaran</label>
            {!! Form::select('type', $paymentTypes->pluck('type_name', 'id'), old('type', $payment->payment_type_id), ['class' => 'form-control select2_single']) !!}
            @if($errors->has('type'))
            <div class="text-danger">
                {{ $errors->first('type')}}
            </div>
            @endif
        </div>
        <div class="form-group">
            <label class="control-label" for="status">Status Pembayaran</label>
            {!! Form::select('status', $paymentStatuses->pluck('name', 'id'), old('type', $payment->payment_status_id), ['class' => 'form-control select2_single']) !!}
            @if($errors->has('status'))
            <div class="text-danger">
                {{ $errors->first('status')}}
            </div>
            @endif
        </div>
        <div class="form-group">
            <label class="control-label" for="program">Program Terkait</label>
            {!! Form::select('program', $programs->pluck('name', 'id'), old('type', $payment->program_id), ['class' => 'form-control select2_single']) !!}
            @if($errors->has('program'))
            <div class="text-danger">
                {{ $errors->first('program')}}
            </div>
            @endif
        </div>
    </div>
</div>
<hr>
<input type="submit" value="Kirim" class="btn btn-success">
@endsection
