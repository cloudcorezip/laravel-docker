@extends('layouts.list')

@section('title')
<h3 class="text-center">Daftar Pengajuan</h3>
@endsection

@section('add-button')
<a
    href="{{ URL('pengajuan/create') }}"
    class="btn btn-success new-data-btn"
>
    Tambah Pengajuan Baru
</a>
@endsection

@section('content')
<table id="example" class="table table-striped responsive-utilities jambo_table">
    <thead>
        <tr class="headings">
            <th>No</th>
            <th>Nama Siswa </th>
            <th>Institusi </th>
            <th>Program Belajar</th>
            <th>Biaya Pendidikan </th>
            <th>Status </th>
            <th class=" no-link last"><span class="nobr">Action</span>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
            @foreach($applications as $a)
        
        <tr class="odd pointer">
            <td>{{$no++}}</td>
            <td class=" ">{{ $a->student->user_detail->fullname }}</td>
            <td class=" ">{{ $a->programs ? $a->programs->institutions->name : '-' }}</td>
            <td class=" ">{{ $a->programs ? $a->programs->name : '-' }}</td>
            <td class=" ">Rp. {{ $a->programs ? number_format($a->programs->price, 0, ',','.') : '-' }}</td>
            <td class=" "><span class="label label-{{ ($a->statuses->id == 1 ? 'warning' : ($a->statuses->id == 2 ? 'success' : 'danger')) }}">{{  $a->statuses->name  }}</span></td>
            <td class=" last">
                <a href="/pengajuan/{{ $a->id }}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Detail </a>
                <a href="/pengajuan/edit/{{ $a->id }}" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                <a href="#" onclick="var c = confirm('Apakah Anda yakin ingin menghapus pengajuan ini?'); if (c) {$('#delete-pengajuan-{{ $a->id }}').submit()}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete </a>
                <form id="delete-pengajuan-{{ $a->id }}" method="POST" action="/pengajuan/delete/{{ $a->id }}">
                    {{ csrf_field() }}
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
