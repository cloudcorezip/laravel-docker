@extends('layouts.form')

@section('title')
<h3 class="text-center">Pembayaran Baru</h3>
@endsection

@section('form-open')
    @if ($pengajuan->id)
    {!!
        Form::open([
            'url' => '/pengajuan/update/' . $pengajuan->id,
            'class' => 'form-horizontal form-label-left',
            'method' => 'PUT'
        ])
    !!}
    @else
    {!!
        Form::open([
            'url' => '/pengajuan/store',
            'class' => 'form-horizontal form-label-left',
            'method' => 'POST'
        ])
    !!}
    @endif
@endsection

@section('content')
<div class="row">
    <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="x_content">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Siswa</label>
                       <div class="col-md-9 col-sm-9 col-xs-12">
                            <select id="heard" name="student_id" class="select2_single form-control" >
                                <option value="">Pilih..</option>
                                @foreach($users as $i)
                                <option value="{{ $i->id }}">{{ $i->user_detail->fullname }} ({{ $i->nik }})</option>
                                @endforeach
                            </select>
                            @if($errors->has('student_id'))
                            <div class="text-danger">
                                {{ $errors->first('student_id')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Penanggungjawab</label>
                       <div class="col-md-9 col-sm-9 col-xs-12">
                            <select id="heard" name="pic_id" class="select2_single form-control" >
                                <option value="">Pilih..</option>
                                @foreach($users as $i)
                                <option value="{{ $i->id }}">{{ $i->user_detail->fullname }} ({{ $i->nik }})</option>
                                @endforeach
                            </select>
                            @if($errors->has('pic_id'))
                            <div class="text-danger">
                                {{ $errors->first('pic_id')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Institusi</label>
                       <div class="col-md-9 col-sm-9 col-xs-12">
                            <select id="heard" name="institution_id" class="select2_single form-control" >
                                <option value="">Pilih..</option>
                                @foreach($institution as $i)
                                <option value="{{ $i->id }}">{{ $i->name }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('institution_id'))
                            <div class="text-danger">
                                {{ $errors->first('institution_id')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Program Belajar</label>
                       <div class="col-md-9 col-sm-9 col-xs-12">
                            <select id="heard" name="program_id" class="select2_single form-control" >
                                <option value="">Pilih..</option>
                                @foreach($programs as $p)
                                <option value="{{ $p->id }}">{{ $p->name }} (Rp. {{ number_format($p->price, 0, ',','.') }})</option>
                                @endforeach
                            </select>
                            @if($errors->has('program_id'))
                            <div class="text-danger">
                                {{ $errors->first('program_id')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-3">Metode Pembayaran</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                    <p>
                        <select id="heard" name="installment_period" class="select2_single form-control" >
                            <option value="">Pilih..</option>
                            <option value="0">Cash</option>
                            <option value="1">1 Bulan</option>
                            <option value="2">2 Bulan</option>
                            <option value="3">3 Bulan</option>
                            <option value="4">4 Bulan</option>
                            <option value="5">5 Bulan</option>
                            <option value="6">6 Bulan</option>
                            <option value="7">7 Bulan</option>
                            <option value="8">8 Bulan</option>
                            <option value="9">9 Bulan</option>
                            <option value="10">10 Bulan</option>
                            <option value="11">11 Bulan</option>
                            <option value="12">12 Bulan</option>
                        </select>
                        @if($errors->has('installment_period'))
                            <div class="text-danger">
                                {{ $errors->first('installment_period')}}
                            </div>
                            @endif
                    </p>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <hr>
    <input type="submit" value="Kirim" class="btn btn-success">
</div>
@endsection
