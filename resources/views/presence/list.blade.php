@extends('layouts.list')
@section('add-link')
<style type="text/css">
    @media (max-width: 1000px) {
        .non {
            display: block;
        }
        .table.no-border tr td, .table.no-border tr th {
            border-width: 0;
        }
    }

    @media (min-width: 1000px) {
        .non {
            display: none;
        }
    }
</style>
@endsection
@section('title')
<h3 class="text-center">Rekap Sesi Pembelajaran</h3>
@endsection

@section('content')
<h4>Jadwal Mengajar Saya</h4>
<div class="hide-table">
    <table id="example" class="table table-striped responsive-utilities jambo_table">
        <thead>
            <tr class="headings">
                <th>No</th>
                <th>Jenis Jadwal</th>
                <th>Kelas</th>
                <th>Program Belajar</th>
                <th>Tanggal</th>
                <th>Jam Mulai</th>
                <th>Jam Selesai</th>
                <th>Mata Pelajaran</th>
                <th class=" no-link last"><span class="nobr">Action</span></th>
            </tr>
        </thead>
        <tbody>
            <?php $no=1;?>
            @foreach($availableClass as $a)
            <tr>
                <td>{{$no++}}</td>
                <td>{{$a->types->name}}</td>
                <td>{{$a->class_id ? $a->classes->class_code : '-'}}</td>
                <td>{{$a->classes ? $a->classes->programs->name : '-'}}</td>
                <td>{{$a->day}}</td>
                <td>{{ date("H:i",strtotime($a->start_hour)) }}</td>
                <td>{{ date("H:i",strtotime($a->finish_hour)) }}</td>
                <td>{{$a->subject_id ? $a->subjects->name : '-'}}</td>
                @if($a->types->admin_input== "t")
                <td class=" last">
                    <a href="/presence/create/{{ $a->id }}/{{$a->class_id}}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Presensi </a>
                </td>
                @else
                <td></td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div class="non">
    <?php $no=1;?>
    @foreach($availableClass as $a)
    <div class="list-group">
        <div class="list-group-item col-md-12 look">
            <table class="table no-border table-condensed table-hover table-sm">
                <tr>
                    <td style="text-align: left"><p
                        style="font-size: 10pt; font-weight: bold; margin: 0px 0px 0px 0px">Mata Pelajaran</p>
                    </td>
                    <td style="text-align: left"><p
                        style="font-size: 10pt;  margin: 0px 0px 0px 0px">{{$a->subject_id ? $a->subjects->name : '-'}}</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left"><p
                        style="font-size: 10pt; font-weight: bold;  margin: 0px 0px 0px 0px">Program</p>
                    </td>
                    <td style="text-align: left"><p
                        style="font-size: 10pt; font-weight:  margin: 0px 0px 0px 0px">{{$a->classes ? $a->classes->programs->name : '-'}}</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left"><p
                        style="font-size: 10pt; font-weight: bold;  margin: 0px 0px 0px 0px">Tanggal</p>
                    </td>
                    <td style="text-align: left"><p
                        style="font-size: 10pt; font-weight:  margin: 0px 0px 0px 0px">{{$a->day}}</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left"><p
                        style="font-size: 10pt;font-weight: bold;  margin: 0px 0px 0px 0px">Jam</p>
                    </td>
                    <td style="text-align: left"><p
                        style="font-size: 10pt; font-weight:  margin: 0px 0px 0px 0px">{{ date("H:i",strtotime($a->start_hour)) }} - {{ date("H:i",strtotime($a->finish_hour)) }}</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left"><p
                        style="font-size: 10pt;font-weight: bold;  margin: 0px 0px 0px 0px">Catatan</p>
                    </td>
                    <td style="text-align: left"><p
                        style="font-size: 10pt; font-weight:  margin: 0px 0px 0px 0px">{{$a->note}}</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left"><p
                        style="font-size: 10pt; font-weight: bold; margin: 0px 0px 0px 0px">Kelas</p>
                    </td>
                    <td style="text-align: left"><p
                        style="font-size: 10pt; font-weight:  margin: 0px 0px 0px 0px">{{$a->class_id ? $a->classes->class_code : '-'}} ({{$a->types->name}})</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <a href="/presence/create/{{ $a->id }}/{{$a->class_id}}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Presensi </a>
                    </td>
                    <td style="text-align: left">
                        
                    </td>
                </tr>
                </table>
            </div>
        </div>
        @endforeach
    </div>
    @endsection
