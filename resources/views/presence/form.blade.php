@extends('layouts.form')

@section('title')
<h3 class="text-center">Presensi ({{ $class->class_code }})</h3>
@endsection

@section('content')
    <table class="table table-striped responsive-utilities jambo_table">
        <thead>
            <th>No.</th>
            <th>Nama</th>
            <th class="text-center">Action</th>
        </thead>
        @if($data->count() > 0)
        <tbody>
            @foreach ($data as $index => $item)
            @php
            $presented = App\Models\Presence::where('schedule_id',$id)->where('student_id',$item->student_id)->get()
            @endphp
            <tr>
                <td>{{$index +1}}</td>
                <td>{{ $item->users->user_detail->fullname }}</td>
                <td>
                    @if ($presented->count() > 0)
                    @php
                    $present = $presented->first()
                    @endphp

                    <form method="POST" action="/presence/undo_presence/{{ $id }}/{{ $item->student_id }}">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-{{ $present->status_id == 1 ? 'success' : 'danger' }} btn-sm btn-block" value="{{ $present->status->name }}">
                    </form>
                    @else
                    <div class="btn-group">
                        <button type="button" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Isi Presensi <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <form method="POST" action="/presence/1/{{ $id }}/{{$item->student_id}}">
                                    {{ csrf_field() }}
                                    <input type="submit" class="btn btn-success btn-sm btn-block" value="Masuk">
                                </form>
                            </li>
                            <li>
                                <form method="POST" action="/presence/2/{{ $id }}/{{ $item->student_id }}">
                                    {{ csrf_field() }}
                                    <input type="submit" class="btn btn-dark btn-sm btn-block" value="Izin">
                                </form>
                            </li>
                            <li>
                                <form method="POST" action="/presence/3/{{ $id }}/{{ $item->student_id}}">
                                    {{ csrf_field() }}
                                    <input type="submit" class="btn btn-warning btn-sm btn-block" value="Sakit">
                                </form>
                            </li>
                            <li>
                                <form method="POST" action="/presence/4/{{ $id }}/{{ $item->student_id }}">
                                    {{ csrf_field() }}
                                    <input type="submit" class="btn btn-danger btn-sm btn-block" value="Alpa">
                                </form>
                            </li>
                        </ul>
                    </div>
                    @endif
                </td>
            </tr>
        @endforeach
        @else
        <tr>
            <td colspan="3" style="text-align: center">Data Tidak Ditemukan</td>
        </tr>
        @endif
        </tbody>
    </table>
@endsection
