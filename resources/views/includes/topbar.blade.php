<style type="text/css">
    .bg-color-red {
        background-color: rgba(255, 20, 20, 0.85);
    }
    .color-white {
        color: #fff;
    }
    .py-2 {
        padding-top: 16px;
        padding-bottom: 16px;
    }
    .logout-btn {
        padding-top: 12px;
        padding-right: 120px;
    }
    .mb-4 {
        margin-bottom: 54px;
    }
</style>
<!-- top navigation -->
<div class="row mb-4">
    <div class="navbar navbar-fixed-top bg-color-red">
        <div class="col-md-2 col-xs-1">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars color-white"></i></a>
            </div>
        </div>
        <div class="col-md-9 col-xs-10 text-center">
            <img src="{{ asset('images/logoHeader.png') }}" class="py-2">
        </div>
       
    </div>
</div>
<div class="top_nav">
    <div class="nav_menu">
        <nav class="" role="navigation">
            <ul class="nav navbar-nav navbar-right">
                <li role="presentation" class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-bell"></i>
                        <span class="badge bg-red">{{$notifs->count()}}</span>
                    </a>
                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                        @foreach($notifs as $a)
                        <li>
                            <a href="#">
                                <b>
                                    <span>
                                        <div class="col-md-12">
                                            <div class="col-md-6"><span class="message">{{$a->description}}</span></div>
                                            <div class="col-md-6"><span class="time">3 mins ago</span></div>
                                        </div>
                                    </span>
                                </b>
                            </a>
                        </li>
                        @endforeach
                        <li>
                            <div class="text-center">
                                <a>
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>
