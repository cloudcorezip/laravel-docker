<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="profile">
            <div class="profile_pic">
                <img src="{{ Gravatar::src('asset/images/img.jpg') }}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome, </span>
                <h2 style="width:130px;">{{ Auth::user()->user_detail->fullname }}</h2>
            </div>
        </div>
        <br>
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <br>
                <br><br><br>
                <ul class="nav side-menu">
                    <li><a href="{{ URL('/') }}"><i class="fa fa-home"></i> Home</a></li>

                    @if (Auth::user()->hasRole('Admin Cluever') || Auth::user()->hasRole('Admin Institusi'))
                    <li><a href="{{ URL('payment') }}"><i class="fa fa-money"></i> Pembayaran </a></li>
                    <li><a href="{{ URL('proposal') }}"><i class="fa fa-level-up"></i> Follow Up </a></li>
                    @endif

                    @if (Auth::user()->hasRole('Siswa'))
                        <li><a href="{{ URL('flexi') }}"><i class="fa fa-male"></i> Flexi Max </a></li>
                        <li><a href="{{ URL('flexi/schedule') }}"><i class="fa fa-calendar"></i> Jadwal </a></li>
                        <li><a href="{{ URL('flexi/rapor') }}"><i class="fa fa-book"></i> Rapor </a></li>
                    @endif
                    
                    @if (Auth::user()->hasRole('Admin Cluever') || Auth::user()->hasRole('Verifikator'))
                    <li><a href="{{ URL('pengajuan') }}"><i class="fa fa-clipboard"></i> Pengajuan </a></li>
                    @endif

                    @if (Auth::user()->hasRole('Admin Cluever') || Auth::user()->hasRole('Admin Institusi') || Auth::user()->hasRole('Educator'))
                    <li><a href="{{ URL('/presence') }}"><i class="fa fa-edit"></i> Presensi</a></li>
                    @endif

                    @if (Auth::user()->hasRole('Admin Cluever') || Auth::user()->hasRole('Admin Institusi'))
                    <li><a><i class="fa fa-folder"></i> Master Data <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="display: none">
                            <li><a href="{{ URL('user') }}">Users</a></li>
                            @if (Auth::user()->hasRole('Admin Cluever'))
                            <li><a href="{{ URL('institusi') }}">Institusi</a></li>
                            <li><a href="{{ URL('tahun_ajaran/institusi') }}">Tahun Ajaran</a></li>
                            <li><a href="{{ URL('jenis_jadwal/institusi') }}">Jenis Jadwal</a></li>
                            <li><a href="{{ URL('matpel/institusi') }}">Mata Pelajaran</a></li>
                            <li><a href="{{ URL('kelas/institusi') }}">Manajemen Kelas</a></li>
                            <li><a href="{{ URL('penjadwalan/institusi') }}">Penjadwalan</a></li>
                            @else (Auth::user()->hasRole('Admin Institusi'))
                            <li><a href="{{ URL('program') }}">Program Belajar</a></li>
                            <li><a href="{{ URL('tahun_ajaran') }}">Tahun Ajaran</a></li>
                            <li><a href="{{ URL('jenis_jadwal') }}">Jenis Jadwal</a></li>
                            <li><a href="{{ URL('matpel') }}">Mata Pelajaran</a></li>
                            <li><a href="{{ URL('kelas') }}">Manajemen Kelas</a></li>
                            <li><a href="{{ URL('penjadwalan') }}">Penjadwalan</a></li>
                            @endif
                        </ul>
                    </li>
                    @endif

                    @if (Auth::user()->hasRole('Admin Cluever') || Auth::user()->hasRole('Admin Institusi'))
                        <li><a><i class="fa fa-file-excel-o"></i> Import Data <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                                <li><a href="{{ URL('import/user')}}">Import User</a></li>
                                <li><a href="{{ URL('import/presensi') }}">Import Presensi</a></li>
                            </ul>
                        </li>
                    @endif

                    <li><a><i class="fa fa-cog"></i> Pengaturan <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="display: none">
                            <li><a href="{{ URL('user/edit/' . Auth::user()->id) }}">Edit Profile</a></li>
                            <li><a href="{{ URL('user/change_password') }}">Ganti Password</a></li>
                            <li>
                                <form method="POST" action="/logout">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-power-off"></i>Logout</button>
                                </form>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>