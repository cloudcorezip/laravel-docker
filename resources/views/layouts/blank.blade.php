<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sistem Informasi</title>
    <link href="{{ asset("css/con-bootstrap/bootstrap.min.css") }}" rel="stylesheet">
    <link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">
    <link href="{{ asset("css/custom.css") }}" rel="stylesheet">
    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">

    @stack('stylesheets')
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">

            @include('includes.topbar')
            
            @include('includes.sidebar')

            @yield('main_container')

            @include('includes.footer')
        </div>
    </div>
</div>
<script src="{{ asset("js/jquery.min.js") }}"></script>
<script src="{{ asset("js/bootstrap.min.js") }}"></script>
@stack('scripts')
<script>
  function initFreshChat() {
    window.fcWidget.init({
      token: "1330619a-9c5f-40f3-a8bd-389c1877c057",
      host: "https://wchat.freshchat.com"
  });
}
function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
</script>
</body>

</html>