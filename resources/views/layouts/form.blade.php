@extends('layouts.blank')

@push('stylesheets')
<link href="{{ asset("css/animate.min.css") }}" rel="stylesheet">
<link href="{{ asset("css/custom.css") }}" rel="stylesheet">
<link href="{{ asset("css/icheck/flat/green.css") }}" rel="stylesheet" />
<link href="{{ asset("css/datatables/tools/css/dataTables.tableTools.css") }}" rel="stylesheet">
<link href="{{ asset("css/select/select2.min.css") }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset("css/switchery/switchery.min.css") }}" />
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
@endpush

@section('main_container')
    <div class="right_col" role="main">
        <div class="">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  @if (\Session::has('success'))
                      <div class="alert alert-success alert-dismissable fade in">
                          {!! \Session::get('success') !!}</div>
                  @endif

                  @if (\Session::has('danger'))
                      <div class="alert alert-danger alert-dismissable fade in">
                          {!! \Session::get('danger') !!}</div>
                  @endif

                  @if (\Session::has('warning'))
                      <div class="alert alert-warning alert-dismissable fade in">
                          {!! \Session::get('warning') !!}</div>
                  @endif
                    <div class="x_panel">
                        <div class="x_title">
                            <h3 class="back-btn">
                                <a onclick="goBack()">
                                    <i class="fa fa-arrow-left"></i>
                                </a>
                            </h3>
                            @yield('title')
                            @yield('add-button')
                            <div class="clearfix"></div>
                        </div>
                        @yield('form-open')
                        {{ csrf_field() }}
                        @yield('content')

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ asset("js/nicescroll/jquery.nicescroll.min.js") }}"></script>
<script src="{{ asset("js/icheck/icheck.min.js") }}"></script>
<script src="{{ asset("js/custom.js") }}"></script>
<script src="{{ asset("js/datatables/js/jquery.dataTables.js") }}"></script>
<script src="{{ asset("js/datatables/tools/js/dataTables.tableTools.js") }}"></script>
<script type="text/javascript" src="{{ asset("js/moment.min2.js") }}"></script>
<script type="text/javascript" src="{{ asset("js/datepicker/daterangepicker.js") }}"></script>
<script src="{{ asset("js/select/select2.full.js") }}"></script>
<script>
function goBack() {
  window.history.back();
}
</script>
@yield('add-script')

<script type="text/javascript">
    $('#provinces').on('change', function(e){
        var province_id = e.target.value;
        $.get('/json-regencies?province_id=' + province_id,function(data) {
          $('#regencies').empty();
          $('#regencies').append('<option value="" disable="true" selected="true">Pilih Kota/Kab</option>');

          $('#districts').empty();
          $('#districts').append('<option value="" disable="true" selected="true">Pilih Kecamatan</option>');

          $('#postcodes').empty();
          $('#postcodes').append('<option value="" disable="true" selected="true">Pilih Kode Pos</option>');

          $.each(data, function(index, regenciesObj){
            $('#regencies').append('<option value="'+ regenciesObj.id +'">'+ regenciesObj.name +'</option>');
          })
        });
      });

      $('#regencies').on('change', function(e){
        var regencies_id = e.target.value;
        $.get('/json-districts?regencies_id=' + regencies_id,function(data) {
          $('#districts').empty();
          $('#districts').append('<option value="" disable="true" selected="true">Pilih Kecamatan</option>');

          $.each(data, function(index, districtsObj){
            $('#districts').append('<option value="'+ districtsObj.id +'">'+ districtsObj.name +'</option>');
          })
        });
      });

      $('#districts').on('change', function(e){
        var districts_id = e.target.value;
        $.get('/json-postcodes?districts_id=' + districts_id,function(data) {
          $('#postcode_id').empty();
          $('#postcode_id').append('<option value="" disable="true" selected="true">Pilih Kelurahan</option>');

          $.each(data, function(index, villagesObj){
            $('#postcode_id').append('<option value="'+ villagesObj.id +'">'+ villagesObj.name +'</option>');
          })
        });
      });

</script>
<script>
    $(document).ready(function () {
        $(".slip").click(function(){ 
            var html = $(".clone").html();
            $(".increment").after(html);
        });

        $("body").on("click",".btn-danger",function(){ 
            $(this).parents(".control-group").remove();
        });

        $(".select2_single").select2({
            placeholder: "Pilih",
            allowClear: false
        });
        $(".select2_group").select2({});
        $(".select2_multiple").select2({
            maximumSelectionLength: 4,
            placeholder: "With Max Selection limit 4",
            allowClear: true
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#single_cal1').daterangepicker({
            singleDatePicker: true,
            format: 'YYYY/MM/DD',
            minDate: '2017/01/01',
            maxDate: '3017/01/01',
            calender_style: "picker_1",
            showDropdowns: true,
            minYear: 1961,
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
        $('#single_cal2').daterangepicker({
            singleDatePicker: true,
            format: 'YYYY/MM/DD',
            minDate: '2017/01/01',
            maxDate: '3017/01/01',
            calender_style: "picker_2",
            showDropdowns: true,
            minYear: 1961,
            maxYear: parseInt(moment().format('YYYY'),10)
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
        $('#single_cal3').daterangepicker({
            singleDatePicker: true,
            format: 'YYYY/MM/DD',
            minDate: '2017/01/01',
            maxDate: '3017/01/01',
            calender_style: "picker_3",
            showDropdowns: true,
            minYear: 1961,
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
        $('#single_cal4').daterangepicker({
            singleDatePicker: true,
            format: 'YYYY/MM/DD',
            minDate: '2017/01/01',
            maxDate: '3017/01/01',
            calender_style: "picker_4",
            showDropdowns: true,
            minYear: 1961,
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
    });
</script>
@endpush
