@extends('layouts.blank')

@push('stylesheets')
    <link href="{{ asset("css/animate.min.css") }}" rel="stylesheet">
    <link href="{{ asset("css/custom.css") }}" rel="stylesheet">
    <link href="{{ asset("css/icheck/flat/green.css") }}" rel="stylesheet"/>
    <link href="{{ asset("css/datatables/tools/css/dataTables.tableTools.css") }}" rel="stylesheet">
    @yield('add-link')
@endpush

@section('main_container')
    <div class="right_col" role="main">
        <div class="">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            @yield('title')
                            @yield('add-button')
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @if (\Session::has('success'))
                                <div class="alert alert-success alert-dismissable fade in">
                                    {!! \Session::get('success') !!}</div>
                            @endif

                            @if (\Session::has('danger'))
                                <div class="alert alert-danger alert-dismissable fade in">
                                    {!! \Session::get('danger') !!}</div>
                            @endif

                            @if (\Session::has('warning'))
                                <div class="alert alert-warning alert-dismissable fade in">
                                    {!! \Session::get('warning') !!}</div>
                            @endif

                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ asset("js/nicescroll/jquery.nicescroll.min.js") }}"></script>
<script src="{{ asset("js/icheck/icheck.min.js") }}"></script>
<script src="{{ asset("js/custom.js") }}"></script>
<script src="{{ asset("js/datatables/js/jquery.dataTables.js") }}"></script>
<script src="{{ asset("js/datatables/tools/js/dataTables.tableTools.js") }}"></script>
<script>
    $(document).ready(function () {
        $('input.tableflat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });
    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('#example').dataTable({
            "oLanguage": {
                "sSearch": "Cari:"
            },
            "aoColumnDefs": [
            {
                'bSortable': false,
                'aTargets': [0]
                        } //disables sorting for column one
                        ],
                        'iDisplayLength': 10,
                        "sPaginationType": "full_numbers",

                    });
        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });
</script>
@yield('add-script')
@endpush