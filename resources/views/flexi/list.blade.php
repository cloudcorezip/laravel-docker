@extends('layouts.list')

@section('title')
    <h3 class="text-center">Flexi Max</h3>
@endsection

@section('add-link')
    <style type="text/css">
        @media (max-width: 1000px) {
            .non {
                display: block;
            }

            .look {
                width: 100%;
            }

            .table.no-border tr td, .table.no-border tr th {
                border-width: 0;
            }

            .tab-small {
                width: 16.5%;
            }
        }

        @media (min-width: 1000px) {
            .non {
                display: none;
            }

            .look {
                width: 100%;
            }
        }
    </style>
@endsection

@section('content')
    {{--    <h4>{{$flexi_date}}</h4>--}}
    <h4>Tahun {{date('Y')}}</h4>
    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
        @php $arrayHari = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu']; $i = 0; @endphp
        @foreach($arrayHari as $hari)
            <li role="presentation" class="{{($hari == "Senin") ? ("active") : ("")}} tab-small">
                <a href="#{{$hari}}_content" id="{{$hari}}-tab" role="tab" data-toggle="tab"
                   aria-expanded="{{($hari == "Senin") ? ("true") : ("")}}">{{$hari}}
                    ({{DateTime::createFromFormat('Y-m-d', $dataTanggal[$i])->format('d/m')}})</a>
            </li>
            @php $i++ @endphp
        @endforeach
    </ul>
    <div id="myTabContent" class="tab-content">
        @foreach($arrayHari as $idx => $hari2)
            <div role="tabpanel" class="tab-pane fade {{($hari2 == "Senin") ? ("active in") : ("")}}"
                 id="{{$hari2}}_content" aria-labelledby="{{$hari2}}-tab">
                <div class="hide-table">
                    <table class="table table-striped responsive-utilities jambo_table example">
                        <thead>
                        <tr class="headings">
                            <th>No</th>
                            <th>Mata Pelajaran</th>
                            <th>Note</th>
                            <th>Educator</th>
                            <th>Kelas</th>
                            <th>Jam Mulai</th>
                            <th>Jam Selesai</th>
                            <th>Sisa Kapasitas</th>
                            <th class=" no-link last"><span class="nobr">Action</span></th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $no = 1 @endphp
                        @foreach($flexi as $f)
                            @if($f->day == $dataTanggal[$idx])
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$f->subjects->name}}</td>
                                    <td>{{$f->note}}</td>
                                    <td>{{$f->users->user_detail->fullname}}</td>
                                    <td>{{$f->classes->class_code}}</td>
                                    <td>{{$f->start_hour}}</td>
                                    <td>{{$f->finish_hour}}</td>
                                    <td>{{$f->capacity}} orang</td>
                                    <td>
                                        @if(in_array($f->id, $dataFlexiArr))
                                            <form method="POST"
                                                  action="/flexi/unbook/{{ $f->id }}/{{Auth::user()->id}}">
                                                {{ csrf_field() }}
                                                @if(in_array($f->day, $dataAfterDate) or in_array($f->day, $dataBeforeDate))
                                                    <input type="submit" class="btn btn-default"
                                                           value="Unbook" disabled>
                                                @else
                                                    <input type="submit" class="btn btn-dark"
                                                           value="Unbook">
                                                @endif
                                            </form>
                                            {{--@endif--}}
                                        @else
                                            <form method="POST"
                                                  action="/flexi/{{ $f->id }}/{{Auth::user()->id}}">
                                                {{ csrf_field() }}
                                                @if($f->day == date('Y-m-d') or in_array($f->day, $dataBeforeDate))
                                                    <input type="submit" class="btn btn-default" value="Book" disabled>
                                                @else
                                                    <input type="submit" class="btn btn-info" value="Book">
                                                @endif
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="non">
                    {{--@if(sizeof($flexi) > 0)--}}
                    @foreach($flexi as $fe)
                        @if ($flexi->filter(function($value) use ($dataTanggal, $idx) { return $value->day == $dataTanggal[$idx];})->count() > 0)
                            @if($fe->day == $dataTanggal[$idx])
                                {{--@if((sizeof($fe->id)) > 0)--}}
                                <div class="list-group">
                                    <div class="list-group-item col-md-12 look">
                                        <table class="table no-border table-condensed table-hover table-sm">
                                            <tr>
                                                <td style="text-align: left"><p
                                                            style="font-size: 13pt; font-weight: bold; margin: 0px 0px 0px 0px">{{$fe->subjects->name}}</p>
                                                </td>
                                                <td style="text-align: right"><p
                                                            style="font-size: 11pt; font-weight: bold; margin: 0px 0px 0px 0px">{{$fe->start_hour}}</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left"><p
                                                            style="font-size: 10pt; margin: 0px 0px 0px 0px">{{$fe->note}}</p>
                                                </td>
                                                <td style="text-align: right"><p
                                                            style="font-size: 11pt; font-weight: bold; margin: 0px 0px 0px 0px">{{$fe->finish_hour}}</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left"><p
                                                            style="font-size: 8pt; font-style: italic; margin: 0px 0px 0px 0px">
                                                        With {{$fe->users->user_detail->fullname}}</p></td>
                                                <td style="text-align: right"><p
                                                            style="font-size: 8pt; color: red; font-style: italic; margin: 0px 0px 0px 0px">{{$fe->capacity}}
                                                        left</p></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left"><p
                                                            style="margin: 0px 0px 0px 0px"></p>{{$fe->classes->class_code}}
                                                </td>
                                                <td style="text-align: right">
                                                    @if(in_array($fe->id, $dataFlexiArr))
                                                        <form method="POST"
                                                              action="/flexi/unbook/{{ $fe->id }}/{{Auth::user()->id}}">
                                                            {{ csrf_field() }}
                                                            @if(in_array($fe->day, $dataAfterDate) or in_array($fe->day, $dataBeforeDate))
                                                                <input type="submit" class="btn btn-default"
                                                                       value="Unbook" disabled>
                                                            @else
                                                                <input type="submit" class="btn btn-dark"
                                                                       value="Unbook">
                                                            @endif
                                                        </form>
                                                    @else
                                                        <form method="POST"
                                                              action="/flexi/{{ $fe->id }}/{{Auth::user()->id}}">
                                                            {{ csrf_field() }}
                                                            @if($fe->day == date('Y-m-d') or in_array($fe->day, $dataBeforeDate))
                                                                <input type="submit" class="btn btn-default"
                                                                       value="Book"
                                                                       disabled>
                                                            @else
                                                                <input type="submit" class="btn btn-info" value="Book">
                                                            @endif
                                                        </form>
                                                    @endif
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            @endif
                        @else
                            <div class="list-group">
                                <div class="list-group-item col-md-12 look">
                                    <table class="table no-border table-condensed table-hover table-sm">
                                        <tr>
                                            <td>Tidak Ada Jadwal Hari Ini.</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            @php
                                break;
                            @endphp
                        @endif
                    @endforeach

                </div>
            </div>
        @endforeach
    </div>
@endsection
@section('add-script')
    <script>
        $(document).ready(function () {
            $('input.tableflat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });
        var asInitVals = new Array();
        $(document).ready(function () {
            var oTable = $('.example').dataTable({
                "oLanguage": {
                    "sSearch": "Cari:"
                },
                "aoColumnDefs": [
                    {
                        'bSortable': false,
                        'aTargets': [0]
                    } //disables sorting for column one
                ],
                'iDisplayLength': 10,
                "sPaginationType": "full_numbers",

            });
            $("tfoot input").keyup(function () {
                /* Filter on the column based on the index of this element's parent <th> */
                oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
            });
            $("tfoot input").each(function (i) {
                asInitVals[i] = this.value;
            });
            $("tfoot input").focus(function () {
                if (this.className == "search_init") {
                    this.className = "";
                    this.value = "";
                }
            });
            $("tfoot input").blur(function (i) {
                if (this.value == "") {
                    this.className = "search_init";
                    this.value = asInitVals[$("tfoot input").index(this)];
                }
            });
        });
    </script>
@endsection