@extends('layouts.list')

@section('title')
    <h3 class="text-center">Rapor Kehadiran Flexi Max</h3>
@endsection

@section('add-link')
    <style type="text/css">
        @media (max-width: 1000px) {
            .non {
                display: block;
            }

            .look {
                width: 100%;
            }

            .table.no-border tr td, .table.no-border tr th {
                border-width: 0;
            }
        }

        @media (min-width: 1000px) {
            .non {
                display: none;
            }

            .look {
                width: 100%;
            }
        }
    </style>
@endsection

@section('content')
    <p><b>Filter Tanggal : </b></p>
    <div>
        <input id="reportrange"
               style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%"
               placeholder="Masukan tanggal"/>
    </div>
    <br>
    {{--<p><b>Filter Tanggal : </b></p>--}}
    {{--<div class="input-group">--}}
    {{--<input placeholder="Start Date" type="text" class="form-control" id="startDate">--}}
    {{--<div class="input-group-addon">to</div>--}}
    {{--<input placeholder="End Date" type="text" class="form-control" id="endDate">--}}
    {{--</div>--}}
    <div class="well" style="overflow: auto">
        <div class="row">
            <div class="col-md-2 col-xs-2">
                <a href="#" onclick="filterList('all')" class="btn btn-round btn-default btn-sm">All ({{$countAll}})</a>
            </div>
            <div class="col-md-2 col-xs-2">
                <a href="#" onclick="filterList('hadir')" class="btn btn-round btn-success btn-sm">Hadir
                    ({{$countHadir}})</a>
            </div>
            <div class="col-md-2 col-xs-2">
                <a href="#" onclick="filterList('sakit')" class="btn btn-round btn-warning btn-sm">Sakit
                    ({{$countSakit}})</a>
            </div>
            <div class="col-md-2 col-xs-2">
                <a href="#" onclick="filterList('izin')" class="btn btn-round btn-primary btn-sm">Izin
                    ({{$countIzin}})</a>
            </div>
            <div class="col-md-2 col-xs-2">
                <a href="#" onclick="filterList('alpa')" class="btn btn-round btn-danger btn-sm">Alpa
                    ({{$countAlpa}})</a>
            </div>
            <div class="col-md-2 col-xs-2">
                <a href="#" onclick="filterList('belum_hadir')" class="btn btn-round btn-dark btn-sm">Belum Hadir
                    ({{$countBelum}})</a>
            </div>
        </div>
    </div>
    <div class="hide-table">
        <table id="example" class="table table-striped responsive-utilities jambo_table">
            <thead>
            <tr class="headings">
                <th>No</th>
                <th>Mata Pelajaran</th>
                <th>Catatan</th>
                <th>Educator</th>
                <th>Kelas</th>
                <th>Tanggal</th>
                <th>Jam Mulai</th>
                <th>Jam Selesai</th>
                <th>Keterangan</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $no => $d)
                <tr>
                    <td>{{$no + 1}}</td>
                    <td>{{$d->name}}</td>
                    <td>{{$d->note}}</td>
                    <td>{{$d->fullname}}</td>
                    <td>{{$d->class_code}}</td>
                    <td>{{$d->day}}</td>
                    <td>{{$d->start_hour}}</td>
                    <td>{{$d->finish_hour}}</td>
                    @if($d->status == 'Hadir')
                        <td><span class="label label-success">{{$d->status}}</span></td>
                    @elseif($d->status == 'Izin')
                        <td><span class="label label-primary">{{$d->status}}</span></td>
                    @elseif($d->status == 'Sakit')
                        <td><span class="label label-warning">{{$d->status}}</span></td>
                    @elseif($d->status == 'Alpa')
                        <td><span class="label label-danger">{{$d->status}}</span></td>
                    @elseif(empty($d->status))
                        <td><span class="label label-default">Belum Hadir</span></td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="non">
        @foreach($data as $de)
            <div class="list-group">
                <div class="list-group-item col-md-12 look">
                    <table class="table no-border table-condensed table-hover table-sm">
                        <tr>
                            <td style="text-align: left"><p
                                        style="font-size: 13pt; font-weight: bold; margin: 0px 0px 0px 0px">{{$de->name}}</p>
                            </td>
                            <td style="text-align: right"><p
                                        style="font-size: 11pt; font-weight: bold; margin: 0px 0px 0px 0px">{{$de->start_hour}}</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left"><p
                                        style="font-size: 10pt; margin: 0px 0px 0px 0px">{{$de->note}}</p>
                            </td>
                            <td style="text-align: right"><p
                                        style="font-size: 11pt; font-weight: bold; margin: 0px 0px 0px 0px">{{$de->finish_hour}}</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left"><p
                                        style="font-size: 8pt; font-style: italic; margin: 0px 0px 0px 0px">
                                    With {{$de->fullname}}</p></td>
                        </tr>
                        <tr>
                            <td style="text-align: left"><p
                                        style="margin: 0px 0px 0px 0px"></p>{{$de->class_code}}
                            </td>
                            @if($de->status == 'Hadir')
                                <td style="text-align: right"><span class="label label-success">{{$de->status}}</span>
                                </td>
                            @elseif($de->status == 'Izin')
                                <td style="text-align: right"><span class="label label-primary">{{$de->status}}</span>
                                </td>
                            @elseif($de->status == 'Sakit')
                                <td style="text-align: right"><span class="label label-warning">{{$de->status}}</span>
                                </td>
                            @elseif($de->status == 'Alpa')
                                <td style="text-align: right"><span class="label label-danger">{{$de->status}}</span>
                                </td>
                            @elseif(empty($de->status))
                                <td style="text-align: right"><span class="label label-default">Belum Hadir</span></td>
                            @endif
                        </tr>
                    </table>
                </div>
            </div>
        @endforeach
        {{--@endif--}}
    </div>
@endsection

@section('add-script')
    <script src="{{ asset("js/timepicker/timepicker.min.js") }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset("js/moment.min2.js") }}"></script>
    <script type="text/javascript" src="{{ asset("js/datepicker/daterangepicker.js") }}"></script>
    <script type="text/javascript">
        var startDate1, endDate1;
        var dataStart, dataEnd;
        var url = new URL(window.location.href);
        var urlStart = url.searchParams.get('start');
        var urlEnd = url.searchParams.get('end');
        var urlFilter = url.searchParams.get('filter');
        var startDate = urlStart ? urlStart : moment().subtract(7, 'days').format('YYYY-MM-DD'),
            endDate = urlEnd ? urlEnd : moment().format('YYYY-MM-DD'),
            filter = urlFilter ? urlFilter : 'all';
        $('#reportrange').val(startDate + ' s.d. ' + endDate);

        function filterList(_filter) {
            filter = _filter;
            window.location.href = '{{ URL('flexi/rapor?filter=') }}' + filter + '&start=' + startDate + '&end=' + endDate;
        }

        $(document).ready(function () {
            var start = moment().subtract(7, 'days');
            var end = moment();

            function cb(start, end) {
                // $('#reportrange').val(start.format('DD MMMM YYYY') + ' - ' + end.format('DD MMMM YYYY'));
                $('#reportrange').val(startDate + ' - ' + endDate);
                startDate1 = start;
                endDate1 = end;
                dataStart = startDate = startDate1.format('YYYY-MM-DD');
                dataEnd = endDate = endDate1.format('YYYY-MM-DD');
                window.location.href = '{{ URL('flexi/rapor?filter=') }}' + filter + '&start=' + startDate + '&end=' + endDate;

                // console.log($('#reportrange').val());
                // console.log(startDate1.format('D MMMM YYYY') + ' - ' + endDate1.format('D MMMM YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);
            // cb(start, end);
        });
    </script>
@endsection