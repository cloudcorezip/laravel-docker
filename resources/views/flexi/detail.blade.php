@extends('layouts.detail')

@section('title')
<h3 class="text-center">Detail Pembayaran</h3>
@endsection

@section('right-header')
<h3><a href="#" class="right-header-btn"><i class="fa fa-print"></i></a></h3>
@endsection

@section('content')
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <table class="table">
            <tr>
                <td>Tanggal Pembayaran</td>
                <td>{{ $payment->created_at }}</td>
            </tr>
            <tr>
                <td>Nominal Pembayaran</td>
                <td>{{ $payment->amount }}</td>
            </tr>
            <tr>
                <td>Program</td>
                <td>{{ $payment->program->name }}</td>
            </tr>
            <tr>
                <td>Status Pembayaran</td>
                <td>{{ $payment->status->name }}</td>
            </tr>
        </table>
    </div>
</div>
@endsection
