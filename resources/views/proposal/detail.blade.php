@extends('layouts.detail')

@section('title')
<h3 class="text-center">Detail Follow Up</h3>
@endsection

@section('right-header')
<h3><a href="#" class="right-header-btn"><i class="fa fa-print"></i></a></h3>
@endsection

@section('content')
<div class="row">
    {{ Form::open([
        'url' => '/proposal/acceptance/' . $proposal->id,
        'method' => 'POST',
        'class' => 'form-horizontal form-label-left'
        ])}}
        <div class="row">
            <div class="col-md-6">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Siswa</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="form-group">
                            <label class="control-label col-md-4" for="first-name">NIK</label>
                            <p class="control-label col-md-8" for="first-name">: {{ $proposal->student->nik }}</p>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="first-name">Nama</label>
                            <p class="control-label col-md-8" for="first-name">: {{ $proposal->student->user_detail->fullname }}</p>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Tempat Lahir</label>
                            <p class="control-label col-md-8" for="first-name">: {{ $proposal->student->user_detail->birthplace }}</p>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Tanggal Lahir</label>
                            <p class="control-label col-md-8" for="first-name">: {{ $proposal->student->user_detail->birthdate }}</p>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Alamat</label>
                            <p class="control-label col-md-8" for="first-name">: {{ $proposal->student->user_detail->address }}</p>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="last-name">No HP</label>
                            <p class="control-label col-md-8" for="first-name">: {{ $proposal->student->phone_no }}</p>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="last-name">Email</label>
                            <p class="control-label col-md-8" for="first-name">: {{ $proposal->student->user_detail->email }}</p>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="last-name">Program Belajar</label>
                            <p class="control-label col-md-8" for="first-name">: {{ $proposal->programs->name }}</p>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="last-name">Biaya Pendidikan</label>
                            <p class="control-label col-md-8" for="first-name">: Rp. {{ number_format($proposal->programs->price, 0, ',','.') }}</p>
                        </div>
                                    <!-- <div class="form-group">
                                        <label class="control-label col-md-4" for="last-name">Jumlah Cicilan</label>
                                        <label class="control-label col-md-8" for="first-name">: 10 x</label>
                                    </div> -->

                                </div>
                            </div>
                            @if ($proposal->application_status_id == 1)
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <br>
                                        <div id="status" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                <input type="radio" name="status" value="terima" data-parsley-multiple="status" data-parsley-id="12"> 
                                                <p class="text-success">Terima</p>
                                            </label>
                                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                <input type="radio" name="status" value="tolak" data-parsley-multiple="status"> <p class="text-danger">Tolak</p>
                                            </label>
                                        </div>
                                        @if($errors->has('status'))
                                        <div class="text-danger">
                                            {{ $errors->first('status')}}
                                        </div>
                                        @endif
                                    </div>
                                        <div class="col-md-12">
                                            <br>
                                            <input type="submit" class="btn btn-primary" value="Follow Up" />
                                        </div>
                                </div>
                            </div>
                            
                            @else
                            <b>Alasan {{ $proposal->application_status_id == 2 ? 'penerimaan' : 'penolakan' }}: {{ $proposal->acceptance_msg }}</b>
                            @endif
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Data Orang Tua</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    
                                    <div class="form-group">
                                        <label class="control-label col-md-4" for="first-name">Nama</label>
                                        <p class="control-label col-md-8" for="first-name">: {{  $proposal->pic->user_detail->fullname  }}</p>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-4">Tempat Lahir</label>
                                        <p class="control-label col-md-8" for="first-name">: {{ $proposal->pic->user_detail->birthplace }} </p>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Tanggal Lahir</label>
                                        <p class="control-label col-md-8" for="first-name">: {{ $proposal->pic->user_detail->birthdate }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4" for="last-name">Alamat</label>
                                        <p class="control-label col-md-8" for="first-name">: {{ $proposal->pic->user_detail->address }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4" for="last-name">No HP</label>
                                        <p class="control-label col-md-8" for="first-name">: {{ $proposal->pic->phone_no }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Email</label>
                                        <p class="control-label col-md-8" for="first-name">: {{ $proposal->pic->user_detail->email }}</p>
                                    </div>
                                    <span><i class="fa fa-paperclip"></i> 2 File — </span>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <img src="{{ asset('images/'.$proposal->pic->user_detail->self_image) }}" style="width: 210px; height:120px;"  />
                                        </div>
                                        <div class="col-md-6">
                                            <img src="{{ asset('images/'.$proposal->pic->user_detail->id_card_image) }}" style="width: 210px; height:120px;" />
                                        </div>
                                    </div>
                                    <span><i class="fa fa-paperclip"></i> Slip Gaji — </span>
                                    <div class="form-group">
                                        @foreach($salary as $a)
                                        <div class="atch-thumb col-md-6">
                                            <img src="{{ asset('images/'.$a->path) }}"  style="width: 210px; height:120px;" />
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
@endsection