@extends('/cluever/layouts.blank')

@push('stylesheets')
    <link href="{{ asset("css/animate.min.css") }}" rel="stylesheet">
    <link href="{{ asset("css/custom.css") }}" rel="stylesheet">
    <link href="{{ asset("css/icheck/flat/green.css") }}" rel="stylesheet" />
    <link href="{{ asset("css/datatables/tools/css/dataTables.tableTools.css") }} rel="stylesheet">
    <link rel="stylesheet" href="{{ asset("css/normalize.css") }}" />
    <link rel="stylesheet" href="{{ asset("css/ion.rangeSlider.css") }}" />
    <link rel="stylesheet" href="{{ asset("css/ion.rangeSlider.skinFlat.css") }}" />
        <link href="{{ asset("css/select/select2.min.css") }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset("css/switchery/switchery.min.css") }}" />

    <!-- colorpicker -->
    <link href="css/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">
@endpush

@section('main_container')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left"></div>
            </div>
                <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h3 class="back-btn">
                                        <a href="javascript:history.go(-1)">
                                            <i class="fa fa-arrow-left"></i>
                                        </a>
                                    </h3>
                                    <h3 class="text-center">Edit Pengajuan<small></small></h3>
                                    <div class="clearfix"></div>
                                </div>
                                {!! Form::open(['url' => '/pengajuan/update','method' => 'PUT','class' => 'form-horizontal form-label-left']) !!}
                                    
                                    @if (\Session::has('success'))
                                    <div class="alert alert-success alert-dismissable fade in">{!! \Session::get('success') !!}</div>
                                    @endif
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="x_panel">
                                                <div class="x_title">
                                                    <h2>Data Pendidikan</h2>
                                                    
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="x_content">
                                                    <br />
                                                        <div class="form-group">
                                                            <input type="text" style="display: none" value="{{ $applications->id }}" id="last-name" name="id" class="form-control col-md-7 col-xs-12">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Siswa</label>
                                                           <div class="col-md-9 col-sm-9 col-xs-12">
                                                                <select id="heard" name="student_id" class="select2_single form-control" >
                                                                @foreach ($users as $p)
                                                                <option value="{{ $p->id }}" {{ ( $p->id == $applications->student_id) ? 'selected' : '' }}> {{ $p->user_detail->fullname }} ({{$p->nik}})</option>
                                                                @endforeach   
                                                                </select>
                                                                @if($errors->has('student_id'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('student_id')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                        </div> 
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Penanggungjawab</label>
                                                           <div class="col-md-9 col-sm-9 col-xs-12">
                                                                <select id="heard" name="pic_id" class="select2_single form-control" >
                                                                @foreach ($users as $p)
                                                                <option value="{{ $p->id }}" {{ ( $p->id == $applications->pic_id) ? 'selected' : '' }}> {{ $p->user_detail->fullname }} ({{$p->nik}})</option>
                                                                @endforeach   
                                                                </select>
                                                                @if($errors->has('pic_id'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('pic_id')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Institusi</label>
                                                           <div class="col-md-9 col-sm-9 col-xs-12">
                                                                <select id="heard" name="institution_id" class="select2_single form-control" >
                                                                    <option value="">Pilih..</option>
                                                                    @foreach($institution as $i)
                                                                    <option value="{{ $i->id }}" {{ ( $i->id == $applications->programs->institution_id) ? 'selected' : '' }}> {{ $i->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                @if($errors->has('institution_id'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('institution_id')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Program Belajar</label>
                                                           <div class="col-md-9 col-sm-9 col-xs-12">
                                                                <select id="heard" name="program_id" class="select2_single form-control" >
                                                                @foreach ($programs as $p)
                                                                <option value="{{ $p->id }}" {{ ( $p->id == $applications->programs_id) ? 'selected' : '' }}> {{ $p->name }} (Rp. {{ number_format($p->price, 0, ',','.') }})</option>
                                                                @endforeach   
                                                                </select>
                                                                @if($errors->has('program_id'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('program_id')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-3">Metode Pembayaran</label>
                                                            <div class="col-md-9 col-sm-9 col-xs-9">
                                                        <p>
                                                            <select id="heard" name="installment_period" class="select2_single form-control" >
                                                                <option value="">Pilih..</option>
                                                                <? if($applications->installment_period==1)
                                                                {
                                                                   echo '<option selected value="0">Cash</option>';
                                                                }
                                                                ?>
                                                                <option value="1">1 Bulan</option>
                                                                <option value="2">2 Bulan</option>
                                                                <option value="3">3 Bulan</option>
                                                                <option value="4">4 Bulan</option>
                                                                <option value="5">5 Bulan</option>
                                                                <option value="6">6 Bulan</option>
                                                                <option value="7">7 Bulan</option>
                                                                <option value="8">8 Bulan</option>
                                                                <option value="9">9 Bulan</option>
                                                                <option value="10">10 Bulan</option>
                                                                <option value="11">11 Bulan</option>
                                                                <option value="12">12 Bulan</option>
                                                            </select>
                                                            @if($errors->has('installment_period'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('installment_period')}}
                                                                </div>
                                                                @endif
                                                        </p>
                                                            </div>
                                                        </div>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"></label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <button type="submit" class="btn btn-md btn-success"><i class="fa fa-save"> Simpan</i></button>
                                                </div>
                                            </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset("js/nicescroll/jquery.nicescroll.min.js") }}"></script>
    <script src="{{ asset("js/icheck/icheck.min.js") }}"></script>
    <script src="{{ asset("js/custom.js") }}"></script>
    <script src="{{ asset("js/datatables/js/jquery.dataTables.js") }}"></script>
    <script src="{{ asset("js/datatables/tools/js/dataTables.tableTools.js") }}"></script>
    <script type="text/javascript" src="{{ asset("js/moment.min2.js") }}"></script>
    <script type="text/javascript" src="{{ asset("js/datepicker/daterangepicker.js") }}"></script>
    <!-- input mask -->
    <script src="{{ asset("js/input_mask/jquery.inputmask.js") }}"></script>
    <!-- knob -->
    <script src="{{ asset("js/knob/jquery.knob.min.js") }}"></script>
    <!-- range slider -->
    <script src="{{ asset("js/ion_range/ion.rangeSlider.min.js") }}"></script>
    <!-- color picker -->
    <script src="{{ asset("js/colorpicker/bootstrap-colorpicker.js") }}"></script>
    <script src="{{ asset("js/colorpicker/docs.js") }}"></script>

    <!-- image cropping -->
    <script src="{{ asset("js/cropping/cropper.min.js") }}"></script>
    <script src="{{ asset("js/cropping/main2.js") }}"></script>
    <script src="{{ asset("js/select/select2.full.js") }}"></script>
    <script>
            $(document).ready(function () {
                $(".select2_single").select2({
                    placeholder: "Pilih",
                    allowClear: false
                });
                $(".select2_group").select2({});
                $(".select2_multiple").select2({
                    maximumSelectionLength: 4,
                    placeholder: "With Max Selection limit 4",
                    allowClear: true
                });
            });
        </script>

    <script type="text/javascript">
        $(document).ready(function () {

            var cb = function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
                $('#reportrange_right span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
            }

            var optionSet1 = {
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2012',
                maxDate: '12/31/2015',
                dateLimit: {
                    days: 60
                },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'right',
                buttonClasses: ['btn btn-default'],
                applyClass: 'btn-small btn-primary',
                cancelClass: 'btn-small',
                format: 'MM/DD/YYYY',
                separator: ' to ',
                locale: {
                    applyLabel: 'Submit',
                    cancelLabel: 'Clear',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            };

            $('#reportrange_right span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

            $('#reportrange_right').daterangepicker(optionSet1, cb);

            $('#reportrange_right').on('show.daterangepicker', function () {
                console.log("show event fired");
            });
            $('#reportrange_right').on('hide.daterangepicker', function () {
                console.log("hide event fired");
            });
            $('#reportrange_right').on('apply.daterangepicker', function (ev, picker) {
                console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
            });
            $('#reportrange_right').on('cancel.daterangepicker', function (ev, picker) {
                console.log("cancel event fired");
            });

            $('#options1').click(function () {
                $('#reportrange_right').data('daterangepicker').setOptions(optionSet1, cb);
            });

            $('#options2').click(function () {
                $('#reportrange_right').data('daterangepicker').setOptions(optionSet2, cb);
            });

            $('#destroy').click(function () {
                $('#reportrange_right').data('daterangepicker').remove();
            });

        });
    </script>
    <!-- datepicker -->
    <script type="text/javascript">
        $(document).ready(function () {

            var cb = function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
            }

            var optionSet1 = {
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2012',
                maxDate: '12/31/2015',
                dateLimit: {
                    days: 60
                },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'left',
                buttonClasses: ['btn btn-default'],
                applyClass: 'btn-small btn-primary',
                cancelClass: 'btn-small',
                format: 'MM/DD/YYYY',
                separator: ' to ',
                locale: {
                    applyLabel: 'Submit',
                    cancelLabel: 'Clear',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            };
            $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
            $('#reportrange').daterangepicker(optionSet1, cb);
            $('#reportrange').on('show.daterangepicker', function () {
                console.log("show event fired");
            });
            $('#reportrange').on('hide.daterangepicker', function () {
                console.log("hide event fired");
            });
            $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
                console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
            });
            $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
                console.log("cancel event fired");
            });
            $('#options1').click(function () {
                $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
            });
            $('#options2').click(function () {
                $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
            });
            $('#destroy').click(function () {
                $('#reportrange').data('daterangepicker').remove();
            });
        });
    </script>
    <!-- /datepicker -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#single_cal1').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
            $('#single_cal2').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_2"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
            $('#single_cal3').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_3"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
            $('#single_cal4').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#reservation').daterangepicker(null, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
        });
    </script>
    <!-- /datepicker -->
    <!-- input_mask -->
    <script>
        $(document).ready(function () {
            $(":input").inputmask();
        });
    </script>
    <!-- /input mask -->
    <!-- ion_range -->
    <script>
        $(function () {
            $("#range_27").ionRangeSlider({
                type: "double",
                min: 1000000,
                max: 2000000,
                grid: true,
                force_edges: true
            });
            $("#range").ionRangeSlider({
                hide_min_max: true,
                keyboard: true,
                min: 0,
                max: 5000,
                from: 1000,
                to: 4000,
                type: 'double',
                step: 1,
                prefix: "$",
                grid: true
            });
            $("#range_25").ionRangeSlider({
                type: "double",
                min: 1000000,
                max: 2000000,
                grid: true
            });
            $("#range_26").ionRangeSlider({
                type: "double",
                min: 0,
                max: 10000,
                step: 500,
                grid: true,
                grid_snap: true
            });
            $("#range_31").ionRangeSlider({
                type: "double",
                min: 0,
                max: 100,
                from: 30,
                to: 70,
                from_fixed: true
            });
            $(".range_min_max").ionRangeSlider({
                type: "double",
                min: 0,
                max: 100,
                from: 30,
                to: 70,
                max_interval: 50
            });
            $(".range_time24").ionRangeSlider({
                min: +moment().subtract(12, "hours").format("X"),
                max: +moment().format("X"),
                from: +moment().subtract(6, "hours").format("X"),
                grid: true,
                force_edges: true,
                prettify: function (num) {
                    var m = moment(num, "X");
                    return m.format("Do MMMM, HH:mm");
                }
            });
        });
    </script>
    <!-- /ion_range -->
    <!-- knob -->
    <script>
        $(function ($) {

            $(".knob").knob({
                change: function (value) {
                    //console.log("change : " + value);
                },
                release: function (value) {
                    //console.log(this.$.attr('value'));
                    console.log("release : " + value);
                },
                cancel: function () {
                    console.log("cancel : ", this);
                },
                /*format : function (value) {
                 return value + '%';
                 },*/
                draw: function () {

                    // "tron" case
                    if (this.$.data('skin') == 'tron') {

                        this.cursorExt = 0.3;

                        var a = this.arc(this.cv) // Arc
                            ,
                            pa // Previous arc
                            , r = 1;

                        this.g.lineWidth = this.lineWidth;

                        if (this.o.displayPrevious) {
                            pa = this.arc(this.v);
                            this.g.beginPath();
                            this.g.strokeStyle = this.pColor;
                            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, pa.s, pa.e, pa.d);
                            this.g.stroke();
                        }

                        this.g.beginPath();
                        this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, a.s, a.e, a.d);
                        this.g.stroke();

                        this.g.lineWidth = 2;
                        this.g.beginPath();
                        this.g.strokeStyle = this.o.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                        this.g.stroke();

                        return false;
                    }
                }
            });

            // Example of infinite knob, iPod click wheel
            var v, up = 0,
                down = 0,
                i = 0,
                $idir = $("div.idir"),
                $ival = $("div.ival"),
                incr = function () {
                    i++;
                    $idir.show().html("+").fadeOut();
                    $ival.html(i);
                },
                decr = function () {
                    i--;
                    $idir.show().html("-").fadeOut();
                    $ival.html(i);
                };
            $("input.infinite").knob({
                min: 0,
                max: 20,
                stopper: false,
                change: function () {
                    if (v > this.cv) {
                        if (up) {
                            decr();
                            up = 0;
                        } else {
                            up = 1;
                            down = 0;
                        }
                    } else {
                        if (v < this.cv) {
                            if (down) {
                                incr();
                                down = 0;
                            } else {
                                down = 1;
                                up = 0;
                            }
                        }
                    }
                    v = this.cv;
                }
            });
        });
    </script>

@endpush
