@extends('layouts.list')

@section('title')
<h3 class="text-center">Follow Up</h3>
@endsection

@section('content')
<table id="example" class="table table-striped responsive-utilities jambo_table">
    <thead>
        <tr class="headings">
            <th>No</th>
            <th>Nama Siswa </th>
            <th>Program Belajar</th>
            <th>Biaya Pendidikan </th>
            <th>Status </th>
            <th class=" no-link last"><span class="nobr">Action</span>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
            @foreach($proposals as $a)
        
        <tr class="odd pointer">
            <td>{{$no++}}</td>
            <td class=" ">{{ $a->student->user_detail->fullname }}</td>
            <td class=" ">{{ $a->programs ? $a->programs->name : '-' }}</td>
            <td class=" ">Rp. {{ $a->programs ? number_format($a->programs->price, 0, ',','.') : '-' }}</td>
            <td class=" "><span class="label label-{{ ($a->statuses->id == 1 ? 'warning' : ($a->statuses->id == 2 ? 'success' : 'danger')) }}">{{  $a->statuses->name  }}</span></td>
            <td class=" last">
                <a href="/proposal/{{ $a->id }}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Detail </a>
                <a href="#" onclick="var c = confirm('Apakah Anda yakin ingin menghapus proposal ini?'); if (c) {$('#delete-proposal-{{ $a->id }}').submit()}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete </a>
                <form id="delete-proposal-{{ $a->id }}" method="POST" action="/proposal/delete/{{ $a->id }}">
                    {{ csrf_field() }}
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
