@extends('layouts.list')
@if(Auth::user()->hasRole('Admin Cluever'))
    @section('title')
    <h3 class="back-btn">
        <a href="javascript:history.go(-1)">
            <i class="fa fa-arrow-left"></i>
        </a>
    </h3>
    <h3 class="text-center">Penjadwalan {{$Institusi->name}}</h3>
    @endsection
    @section('add-button')
    <a href="{{ URL('penjadwalan/create/'.$Institusi->id) }}" class="btn btn-success new-data-btn"> Tambah Penjadwalan Baru </a>
    @endsection
@else
    @section('title')
    <h3 class="text-center">Penjadwalan</h3>
    @endsection
    @section('add-button')
    <a href="{{ URL('penjadwalan/create/'. Auth::user()->institution_user->institution_id) }}" class="btn btn-success new-data-btn"> Tambah Penjadwalan Baru </a>
    @endsection
@endif
@section('content')
<table id="example" class="table table-striped responsive-utilities jambo_table">
    <thead>
        <tr class="headings">
            <th style="width: 5%">No </th>
            <th style="width: 10%">Tanggal </th>
            <th style="width: 10%">Jenis Jadwal </th>
            <th style="width: 10%">Kode Kelas </th>
            <th style="width: 10%">Jam Mulai </th>
            <th style="width: 10%">Jam Selesai </th>
            <th style="width: 15%">Mata Pelajaran</th>
            <th style="width: 5%">Kapasitas</th>
            <th style="width: 20%">Educator</th>
            <th style="width: 10%">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; ?>
        @foreach($schedules as $a)
        <tr>
            <td>{{$no++}}</td>
            <td>{{$a->day}}</td>
            <td>{{$a->types->name}}</td>
            <td>{{$a->classes ? $a->classes->class_code : '-'}}</td>
            <td>{{ date("H:i",strtotime($a->start_hour)) }}</td>
            <td>{{ date("H:i",strtotime($a->finish_hour)) }}</td>
            <td>{{$a->subject_id ? $a->subjects->name : '-'}}</td>
            <td>{{$a->capacity}} orang</td>
            <td>{{$a->users->user_detail->fullname}}</td>
            <td class=" last">
                <a href="/penjadwalan/{{ $a->id }}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Detail </a>
                <a href="/penjadwalan/edit/{{ $a->id }}" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                <a href="#" onclick="var c = confirm('Apakah Anda yakin ingin menghapus institusi ini?'); if (c) {$('#delete-penjadwalan-{{ $a->id }}').submit()}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete </a>
                <form id="delete-penjadwalan-{{ $a->id }}" method="POST" action="/penjadwalan/delete/{{ $a->id }}">
                    {{ csrf_field() }}
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
