@extends('layouts.form')
@section('title')
@if ($schedule->id)
<h3 class="text-center">Edit Penjadwalan</h3>
@else
<h3 class="text-center">Tambah Penjadwalan Baru</h3>
@endif
@endsection

@section('form-open')
    @if ($schedule->id)
    {!! Form::open([
            'url' => '/penjadwalan/update/'.$schedule->id,
            'enctype' =>'multipart/form-data',
            'method' => 'PUT','class' => 'form-horizontal form-label-left'
        ]) 
    !!}
    @else
    {!! Form::open([
        'url' => '/penjadwalan/store',
        'enctype' =>'multipart/form-data',
        'class' => 'form-horizontal form-label-left'
        ]) 
    !!}
    <input type="text" id="last-name" name="institution_id" style="display: none" value="{{$id}}" class="form-control col-md-7 col-xs-12">
    @endif
@endsection
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_content">
            <div class="col-md-6">
                <div class="form-group">
                    <label
                        class="control-label col-md-3 col-sm-3 col-xs-12"
                        for="schedule_type_id"
                    >
                        Jenis Jadwal
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select id="schedule-type"  name="schedule_type_id" class="select2_single form-control" >
                            <option value="">Pilih jenis jadwal</option>
                            @foreach ($scheduletype as $p)
                            <option value="{{$p->id}}" {{old('schedule_type_id',isset($schedule->schedule_type_id) ? $schedule->schedule_type_id : null) == $p->id ? 'selected' : ''}}>{{$p->name}}</option>
                            @endforeach 
                            
                        </select>
                        @if($errors->has('schedule_type_id'))
                        <div class="text-danger">
                            {{ $errors->first('schedule_type_id')}}
                        </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                        Tanggal
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <div
                            role="wrapper"
                            class="gj-timepicker gj-timepicker-bootstrap gj-unselectable input-group"
                        >
                            <input
                                type="text"
                                name="day"
                                value="{{ old('day',  isset($schedule->day) ? $schedule->day : null) }}"
                                class="form-control has-feedback-right"
                                id="single_cal4"
                                placeholder="Tanggal mengajar"
                                aria-describedby="inputSuccess2Status"
                                role="input"
                                readonly
                            >
                            <span
                                class="input-group-addon"
                                role="right-icon"
                                id="span_single_cal4"
                            >
                                <i class="fa fa-calendar-o"></i>
                            </span>
                        </div>
                        @if($errors->has('day'))
                        <div class="text-danger">
                            {{ $errors->first('day')}}
                        </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                        Jam Mulai
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <input
                            id="timepicker1"
                            name="start_hour"
                            value="{{ old('start_hour',  isset($schedule->start_hour) ? $schedule->start_hour : null) }}"
                            readonly
                            placeholder="Jam mulai"
                            class="form-control"
                            style="width:215px;"
                        />
                        @if($errors->has('start_hour'))
                        <div class="text-danger">
                            {{ $errors->first('start_hour')}}
                        </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                        Jam Selesai
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <input
                            id="timepicker2"
                            name="finish_hour"
                            value="{{ old('finish_hour',  isset($schedule->finish_hour) ? $schedule->finish_hour : null) }}"
                            class="form-control"
                            readonly
                            placeholder="Jam selesai"
                            style="width:215px;"
                        />
                        @if($errors->has('finish_hour'))
                        <div class="text-danger">
                            {{ $errors->first('finish_hour')}}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                        Mata Pelajaran
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <select id="heard"  name="subject_id" class="select2_single form-control" >
                            <option value="">Pilih mata pelajaran</option>
                            @foreach($subjects as $a)
                            <option value="{{$a->id}}" {{old('subject_id',isset($schedule->subject_id) ? $schedule->subject_id : null) == $a->id ? 'selected' : ''}}>{{$a->name}}</option>
                            @endforeach 
                            
                        </select>
                        @if($errors->has('subject_id'))
                        <div class="text-danger">
                            {{ $errors->first('subject_id')}}
                        </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                        Pengajar
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <select id="heard"  name="educator_id" class="select2_single form-control" >
                            <option value="">Pilih pengajar</option>
                            @foreach ($user_roles as $p)
                            <option value="{{$p->users->id}}" {{old('educator_id',isset($schedule->educator_id) ? $schedule->educator_id : null) == $p->users->id ? 'selected' : ''}}>{{$p->users->user_detail->fullname}} ({{$p->users->nik}})</option>
                            @endforeach 
                            
                        </select>
                        @if($errors->has('educator_id'))
                        <div class="text-danger">
                            {{ $errors->first('educator_id')}}
                        </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kelas</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <select id="heard"  name="class_id" class="select2_single form-control" >
                            <option value="">Pilih..</option>
                            @foreach ($classes as $p)
                            <option value="{{$p->id}}" {{old('class_id',isset($schedule->class_id) ? $schedule->class_id : null) == $p->id ? 'selected' : ''}}>{{$p->class_code}}</option>
                            @endforeach 
                            
                        </select>
                        @if($errors->has('class_id'))
                        <div class="text-danger">
                            {{ $errors->first('class_id')}}
                        </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                        Catatan
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <textarea type="text" name="note" placeholder="Contoh: Gaya Pegas" class="form-control">{{ old('note',  isset($schedule->note) ? $schedule->note : null) }}</textarea>
                        @if($errors->has('note'))
                            <div class="text-danger">
                                {{ $errors->first('note')}}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kapasitas</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" name="capacity" value="{{ old('capacity',  isset($schedule->capacity) ? $schedule->capacity : null) }}"  placeholder="Kapasitas Jadwal" class="form-control"/>
                        @if($errors->has('capacity'))
                            <div class="text-danger">
                                {{ $errors->first('capacity')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
    </div>
</div>
<hr>
<input type="submit" value="Simpan" class="btn btn-primary">
@endsection

@section('add-script')
<script src="{{ asset("js/timepicker/timepicker.min.js") }}" type="text/javascript"></script>
<script type="text/javascript">
    $(".select2_single").select2({
        placeholder: "Pilih",
        allowClear: false
    });
    $("#select2_single1").select2({
        placeholder: "Pilih",
        allowClear: false
    });
    $("#select2_single2").select2({
        placeholder: "Pilih",
        allowClear: false
    });
    $('#timepicker1').timepicker({
        uiLibrary: 'bootstrap',
        footer: false,
        modal: false
    });
    $('#timepicker2').timepicker({
        uiLibrary: 'bootstrap',
        footer: false,
        modal: false
    });
    $('#single_cal1').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        format: 'YYYY/MM/DD',
        minDate: '2017/01/01',
        maxDate: '3017/01/01',
    },  function (start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
    });
    $('#single_cal2').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_2",
        format: 'YYYY/MM/DD',
        minDate: '2017/01/01',
        maxDate: '3017/01/01',
    },  function (start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
    });
    $('#single_cal3').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_3",
        format: 'YYYY/MM/DD',
        minDate: '2017-01-01',
        maxDate: '3017-01-01',
    },  function (start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
    });
    $('#single_cal4').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_4",
        format: 'YYYY/MM/DD',
        minDate: '2017/01/01',
        maxDate: '3017/01/01',
    },  function (start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
    });
</script>
@endsection