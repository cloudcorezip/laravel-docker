@extends('layouts.form')

@section('title')
@if ($users->id)
<h3 class="text-center">Edit Siswa</h3>
@else
<h3 class="text-center">Tambah Siswa<small></small></h3>
@endif
@endsection

@section('form-open')
    @if ($users->id)
    {!! Form::open([

            'url' => '/user/update_student/' . $users->id,
            'enctype' =>'multipart/form-data',
            'method' => 'PUT','class' => 'form-horizontal form-label-left'
        ]) 
    !!}
    @else
    {!! Form::open([
        'url' => '/user/store_student',
        'enctype' =>'multipart/form-data',
        'class' => 'form-horizontal form-label-left'
        ]) 
    !!}
    @endif
@endsection
@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">NIK</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" value="{{$id}}" id="last-name" name="parent_id" style="display: none"  class="form-control col-md-7 col-xs-12">
                <input type="number" id="last-name" name="nik" value="{{ old('nik',  isset($users->nik) ? $users->nik : null) }}"  class="form-control col-md-7 col-xs-12" >
                @if($errors->has('nik'))
                <div class="text-danger">
                    {{ $errors->first('nik')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Lengkap</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" id="last-name" name="fullname" value="{{ old('fullname',  isset($users->user_detail->fullname) ? $users->user_detail->fullname : null) }}"  class="form-control col-md-7 col-xs-12">
                @if($errors->has('fullname'))
                <div class="text-danger">
                    {{ $errors->first('fullname')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-3" for="last-name">Tempat Lahir</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
                <input type="text" id="last-name" name="birthplace"  value="{{ old('birthplace',  isset($users->user_detail->birthplace) ? $users->user_detail->birthplace : null) }}" class="form-control col-md-7 col-xs-12">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Lahir</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
                <input type="text" name="birthdate" value="{{ old('birthdate',  isset($users->user_detail->birthdate) ? $users->user_detail->birthdate : null) }}" class="form-control has-feedback-left" id="single_cal4" placeholder="Tanggal" aria-describedby="inputSuccess2Status">
                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                @if($errors->has('birthdate'))
                <div class="text-danger">
                    {{ $errors->first('birthdate')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Kelas</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select id="heard" class="select2_single form-control" name="class_name" >
                    <option value="0"  selected="true">Pilih Kelas</option>
                    @foreach ($SchoolGrades as $key => $value)
                    <option value="{{$value->id}}">{{$value->grade . " " . $value->level . " " . $value->major}}</option>
                    @endforeach
                </select>
                @if($errors->has('class_name'))
                <div class="text-danger">
                    {{ $errors->first('class_name')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Sekolah Asal</label>
            <div id="siswa" class="col-md-9 col-sm-9 col-xs-12">
                <select  name="school_id" class="select2_single form-control col-md-7 col-xs-12"  >
                    <option value="0"  selected="true">Pilih Sekolah</option>
                    @foreach ($Schools as $key => $value)
                    <option value="{{$value->id}}">{{$value->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">No HP</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="number" id="last-name" name="phone_no" value="{{ old('phone_no',  isset($users->phone_no) ? $users->phone_no : null) }}" class="form-control col-md-7 col-xs-12">
                @if($errors->has('phone_no'))
                <div class="text-danger">
                    {{ $errors->first('phone_no')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Email</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="email" id="last-name" name="email" value="{{ old('email',  isset($users->user_detail->email) ? $users->user_detail->email : null) }}" class="form-control col-md-7 col-xs-12">
                @if($errors->has('email'))
                <div class="text-danger">
                    {{ $errors->first('email')}}
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Provinsi</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="select2_single form-control" name="provinces" id="provinces">
                    <option value="0" disabled="true" selected="true">Pilih Provinsi</option>
                    @foreach ($province as $key => $value)
                    <option value="{{$value->id}}">{{$value->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kota / Kabupaten</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="select2_single form-control" name="regencies" id="regencies">
                  <option value="0" disable="true" selected="true">Pilih Kota/Kab</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kecamatan</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="select2_single form-control" name="districts" id="districts">
                  <option value="0" disable="true" selected="true">Pilih Kecamatan</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kelurahan</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="select2_single form-control" name="postcode_id" id="postcode_id">
                  <option value="" disable="true" selected="true">Pilih</option>
                </select>
                <!-- <select id="heard"  name="postcode_id" class="select2_single form-control" >
                    <option value="">Pilih..</option>
                    @foreach ($postcode as $p)
                    <option value="{{$p->id}}" {{old('postcode_id',isset($users->user_detail->postcode_id) ? $users->user_detail->postcode_id : null) == $p->id ? 'selected' : ''}}>{{$p->code}}</option>
                    @endforeach 
                    
                </select> -->
                @if($errors->has('postcode_id'))
                <div class="text-danger">
                    {{ $errors->first('postcode_id')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat Lengkap</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <textarea id="message"  class="form-control" name="address" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10">{{ old('address',  isset($users->user_detail->address) ? $users->user_detail->address : null) }}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">KTP</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="file" id="last-name" name="id_card_image"  class="form-control col-md-7 col-xs-12">
            </div>
        </div>
    </div>
</div>
<hr>
<input type="submit" value="Simpan" class="btn btn-primary">
@endsection
