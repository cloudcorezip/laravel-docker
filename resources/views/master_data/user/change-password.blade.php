@extends('layouts.form')

@section('title')
<h3 class="text-center">Ubah Password</h3>
@endsection

@section('form-open')
    {!! Form::open([
            'url' => 'user/update_password',
            'enctype' =>'multipart/form-data',
            'method' => 'PUT','class' => 'form-horizontal form-label-left'
        ]) 
    !!}
@endsection
@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Password lama</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="password" required id="old-password" name="old_password" class="form-control col-md-7 col-xs-12">
                @if($errors->has('old_password'))
                <div class="text-danger">
                    {{ $errors->first('old_password')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Password Baru</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="password" required id="password" name="password" class="form-control col-md-7 col-xs-12">
                @if($errors->has('password'))
                <div class="text-danger">
                    {{ $errors->first('password')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Konfirmasi Password Baru</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="password" required id="password_confirm" name="password_confirm" class="form-control col-md-7 col-xs-12">
                @if($errors->has('password_confirm'))
                <div class="text-danger">
                    {{ $errors->first('password_confirm')}}
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-6">
        <input type="submit" value="Simpan" class="btn btn-primary">
    </div>
</div>
@endsection
