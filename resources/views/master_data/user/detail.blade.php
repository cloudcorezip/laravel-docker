@extends('layouts.detail')

@section('title')
<h3 class="text-center">Detail User</h3>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Biodata</a>
                </li>
                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Siswa</a>
                </li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                    <form class="form-horizontal form-label-left">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">NIK</label>
                                <label class="control-label col-md-8 col-sm-8 col-xs-12" for="first-name">: {{  $users->nik  }}</label>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-4" for="first-name">Nama</label>
                                <label class="control-label col-md-8 col-sm-8 col-xs-8" for="first-name">: {{  $users->user_detail->fullname  }}</label>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Tempat Lahir</label>
                                <label class="control-label col-md-8 col-sm-8 col-xs-12" for="first-name">: {{ $users->user_detail->birthplace }} </label>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Tanggal Lahir</label>
                                <label class="control-label col-md-8 col-sm-8 col-xs-12" for="first-name">: {{ $users->user_detail->birthdate }}</label>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="last-name">Alamat</label>
                                <label class="control-label col-md-8 col-sm-8 col-xs-12" for="first-name">: {{ $users->user_detail->address }}</label>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="last-name">No HP</label>
                                <label class="control-label col-md-8 col-sm-8 col-xs-12" for="first-name">: {{ $users->phone_no }}</label>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Email</label>
                                <label class="control-label col-md-8 col-sm-8 col-xs-12" for="first-name">: {{ $users->user_detail->email }}</label>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <span><i class="fa fa-paperclip"></i> 2 File — </span>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <img src="{{ asset('images/'.$users->user_detail->self_image) }}" style="width:  280px; height:170px;" alt="img" />
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <img src="{{ asset('images/'.$users->user_detail->id_card_image) }}" style="width:  280px; height:170px;" alt="img" />
                                </div>
                            </div>
                            <span><i class="fa fa-paperclip"></i> Slip Gaji — </span>
                            <div class="form-group">
                                @foreach($salary as $a)
                                <div class="atch-thumb col-md-6 col-sm-6 col-xs-12">
                                    <img src="{{ asset('images/'.$a->path) }}" alt="img" style="width:  280px; height:170px;" />
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                    <a href="/user/create_student/{{$picid->id}}" class="btn btn-success"><i class="fa fa-plus"> Tambah Siswa</i></a>
                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                        <thead>
                            <tr class="headings">
                                <th>No</th>
                                <th>NIK</th>
                                <th>Nama </th>
                                <th>No HP </th>
                                <th>Email</th>
                                <th>Alamat</th>
                                <th class=" no-link last"><span class="nobr">Action</span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                            @foreach($user_detail as $a)
                            <tr class="odd pointer">
                                <td>{{$no++}}</td>
                                <td class=" ">{{ $a->users->nik }}</td>
                                <td class=" ">{{ $a->fullname }}</td>
                                <td class=" ">{{ $a->users->phone_no }}</td>
                                <td class=" ">{{ $a->email }}</td>
                                <td class=" ">{{ $a->address }}</td>
                                <td class=" last">
                                    <a href="/user/detail_student{{ $a->users->id }}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Detail </a>
                                    <a href="/user/edit_student/{{ $a->users->id }}" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                    <a href="#" onclick="var c = confirm('Apakah Anda yakin ingin menghapus pengajuan ini?'); if (c) {$('#delete-user-{{ $a->users->id }}').submit()}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete </a>
                                    <form id="delete-user-{{ $a->id }}" method="POST" action="/user/delete_student/{{ $a->users->id }}">
                                        {{ csrf_field() }}
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

