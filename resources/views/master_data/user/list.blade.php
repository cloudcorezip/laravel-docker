@extends('layouts.list')

@section('title')
<h3 class="text-center">Users</h3>
@endsection

@section('add-button')
<a href="{{ URL('user/create') }}" class="btn btn-success new-data-btn"> Tambah User Baru </a>
@endsection

@section('content')
<table id="example" class="table table-striped responsive-utilities jambo_table">
    <thead>
        <tr class="headings">
            <th>No</th>
            <th>NIK</th>
            <th>Nama </th>
            <th>No HP </th>
            <th>Email</th>
            <th>Alamat</th>
            <th class=" no-link last"><span class="nobr">Action</span>
            </th>
        </tr>
    </thead>
    @if (Auth::user()->hasRole('Admin Cluever'))
    <tbody>
         <?php $no = 1; ?>
         @foreach($users as $a)
         <tr class="odd pointer">
            <td>{{$no++}}</td>
            <td class=" ">{{ $a->nik }}</td>
            <td class=" ">{{ $a->user_detail->fullname }}</td>
            <td class=" ">{{ $a->phone_no }}</td>
            <td class=" ">{{ $a->user_detail->email }}</td>
            <td class=" ">{{ $a->user_detail->address }}</td>
            <!-- <td><input type="checkbox" {{ $a->hasRole('Educator') ? 'checked': ""}} name="">{{ $a->hasRole('Admin Cluever') ? $a->hasRole('Admin Cluever') : ""}}<input type="checkbox" {{ $a->hasRole('Admin Institusi') ? 'checked': ""}} name=""></td> -->
            <td class=" last">
                <!-- <a href="student/{{ $a->user_detail->id }}" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Tambah Siswa </a> -->
                <a href="user/{{ $a->id }}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Detail </a>
                <a href="user/edit/{{ $a->id }}" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                <a href="delete/{{ $a->id }}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete </a>
            </td>
        </tr>
        @endforeach
    </tbody>
    @elseif (Auth::user()->hasRole('Admin Institusi'))
    <tbody>
        <?php $no = 1; ?>
        @foreach($users as $a)
        <tr class="odd pointer">
            <td>{{$no++}}</td>
            <td class=" ">{{ $a->users->nik }}</td>
            <td class=" ">{{ $a->users->user_detail->fullname }}</td>
            <td class=" ">{{ $a->users->phone_no }}</td>
            <td class=" ">{{ $a->users->user_detail->email }}</td>
            <td class=" ">{{ $a->users->user_detail->address }}</td>
            <td class=" last">
                <a href="user/{{ $a->users->user_detail->user_id }}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Detail </a>
                <a href="user/edit/{{ $a->users->user_detail->user_id }}" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                <a href="delete/{{ $a->users->user_detail->user_id }}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete </a>
            </td>
        </tr>
        @endforeach
    </tbody>
    @endif
</table>
@endsection
