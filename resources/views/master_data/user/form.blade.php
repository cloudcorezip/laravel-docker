@extends('layouts.form')

@section('title')
@if ($users->id)
<h3 class="text-center">Edit User</h3>
@else
<h3 class="text-center">Tambah User Baru</h3>
@endif
@endsection

@section('form-open')
    @if ($users->id)
    {!! Form::open([
            'url' => 'user/update/' . $users->id,
            'enctype' =>'multipart/form-data',
            'method' => 'PUT','class' => 'form-horizontal form-label-left'
        ]) 
    !!}
    @else
    {!! 
        Form::open([
            'url' => '/user/store',
            'enctype' =>'multipart/form-data',
            'class' => 'form-horizontal form-label-left'
        ])
    !!}
    @endif
@endsection
@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">NIK</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="number" id="last-name" value="{{ old('nik',  isset($users->nik) ? $users->nik : null) }}" name="nik"   class="form-control col-md-7 col-xs-12">
                @if($errors->has('nik'))
                <div class="text-danger">
                    {{ $errors->first('nik')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Lengkap</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" id="last-name" name="fullname" value="{{ old('fullname',  isset($users->user_detail->fullname) ? $users->user_detail->fullname : null) }}" class="form-control col-md-7 col-xs-12">
                @if($errors->has('fullname'))
                <div class="text-danger">
                    {{ $errors->first('fullname')}}
                </div>
                @endif
            </div>
        </div>   
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-3" for="last-name">Tempat Lahir</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
                <input type="text" id="last-name" value="{{ old('birthplace',  isset($users->user_detail->birthplace) ? $users->user_detail->birthplace : null) }}" name="birthplace"   class="form-control col-md-7 col-xs-12">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Lahir</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
                <input type="text" name="birthdate" value="{{ old('birthdate',  isset($users->user_detail->birthdate) ? $users->user_detail->birthdate : null) }}" class="form-control has-feedback-left" id="single_cal2" placeholder="Tanggal" aria-describedby="inputSuccess2Status">
                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                @if($errors->has('birthdate'))
                <div class="text-danger">
                    {{ $errors->first('birthdate')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Provinsi</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="select2_single form-control" name="provinces" id="provinces">
                    <option value="" disabled="true" selected="true">Pilih Provinsi</option>
                    @foreach ($province as $key => $value)
                    <option value="{{$value->id}}">{{$value->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kota / Kabupaten</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="select2_single form-control" name="regencies" id="regencies">
                  <option value="" disable="true" selected="true">Pilih Kota/Kab</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kecamatan</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="select2_single form-control" name="districts" id="districts">
                  <option value="" disable="true" selected="true">Pilih Kecamatan</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kelurahan</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="select2_single form-control" name="postcode_id" id="postcode_id">
                  <option value="1" {{old('postcode_id',isset($users->user_detail->postcode_id) ? $users->user_detail->postcode_id : null) == 1 ? 'selected' : ''}}>Jawa</option>
                </select>
                <!-- <select id="heard"  name="postcode_id" class="select2_single form-control" >
                    <option value="">Pilih..</option>
                    @foreach ($postcode as $p)
                    <option value="{{$p->id}}" {{old('postcode_id',isset($users->user_detail->postcode_id) ? $users->user_detail->postcode_id : null) == $p->id ? 'selected' : ''}}>{{$p->code}}</option>
                    @endforeach 
                    
                </select> -->
                @if($errors->has('postcode_id'))
                <div class="text-danger">
                    {{ $errors->first('postcode_id')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat Lengkap</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <textarea id="message"  class="form-control" name="address" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10">{{ old('address',  isset($users->user_detail->address) ? $users->user_detail->address : null) }}</textarea>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">No HP</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="number" id="last-name" value="{{ old('phone_no',  isset($users->phone_no) ? $users->phone_no : null) }}" name="phone_no"   class="form-control col-md-7 col-xs-12">
                @if($errors->has('phone_no'))
                <div class="text-danger">
                    {{ $errors->first('phone_no')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Email</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="email" id="last-name" value="{{ old('email',  isset($users->user_detail->email) ? $users->user_detail->email : null) }}" name="email"  class="form-control col-md-7 col-xs-12">
                @if($errors->has('email'))
                <div class="text-danger">
                    {{ $errors->first('email')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Pekerjaan</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" id="last-name" value="{{ old('occupation',  isset($users->user_detail->occupation) ? $users->user_detail->occupation : null) }}" name="occupation"  class="form-control col-md-7 col-xs-12">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Salary</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="number" id="last-name" value="{{ old('salary',  isset($users->user_detail->salary) ? $users->user_detail->salary : null) }}" name="salary"  class="form-control col-md-7 col-xs-12">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">KTP</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                @if(isset($users->user_detail->id_card_image))
                <img src="{{ asset('images/'.$users->user_detail->id_card_image) }}" style="width:  173px; height:100px;"/>
                @endif
                <input type="file" id="last-name" name="id_card_image"  class="form-control col-md-7 col-xs-12">
                @if($errors->has('id_card_image'))
                <div class="text-danger">
                    {{ $errors->first('id_card_image')}}
                </div>
                @endif
            </div>
        </div>
            <div class="form-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Slip Gaji</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    @foreach($salary as $a)
                    @if(isset($users->id))
                    <div class="col-md-7 col-sm-7 col-xs-12"> 
                        <img src="{{ asset('images/'.$a->path) }}" style="width:  173px; height:100px;"/>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <a href="/user/delete_salary/{{ $a->id }}" onclick="var c = confirm('Apakah Anda yakin ingin menghapus slip gaji ini?'); if (c) {$('#delete_salary-{{ $a->id }}').submit()}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
       
        <div class="form-group increment" >
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"></label>
            <div class="col-md-7 col-sm-7 col-xs-12">
                <input type="file" name="path[]" class="form-control ">
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12"> 
                <button class="btn btn-success slip" type="button">+ Tambah</button>
            </div>
        </div>
        <div class="clone hide">
            <div class="form-group control-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"></label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                    <input type="file" name="path[]" class="form-control">
                </div>

                <div class="col-md-2 col-sm-2 col-xs-12"> 
                    <button class="btn btn-danger" type="button"><i class="fa fa-trash"></i> Hapus</button>
                </div>
            </div>
        </div>
        <div class="form-group" >
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Hak Akses</label>
            <div class="col-md-4 col-sm-4 col-xs-12">
                
            </div>
        </div>
        
        @if (Auth::user()->hasRole('Admin Cluever'))
        <div class="form-group" >
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"></label>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <label for="chkPassport">
                    <input type="checkbox" value="1" {{ $users->hasRole('Admin Cluever') ? 'checked': ""}} name="role_id1[]"/>
                    Admin Cluever
                </label>
                <div style="display: none">
                    <select class="select2_single form-control"  name="institution_id[1]" >
                        <option value="">Pilih Institusi</option>
                        @foreach($institution as $a)
                        <option value="{{$a->id}}">{{$a->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group" >
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"></label>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <label for="chkPassport">
                    <input type="checkbox"  value="3" {{ $users->hasRole('Verifikator') ? 'checked': ""}} name="role_id1[]" />
                    Verifikator
                </label>
                <div style="display: none">
                    <select class="select2_single form-control" name="institution_id[3]" >
                        <option value="">Pilih Institusi</option>
                        @foreach($institution as $a)
                        <option value="{{$a->id}}">{{$a->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        @if($errors->has('role_id1'))
        <div class="text-danger">
            {{ $errors->first('role_id1')}}
        </div>
        @endif
        <div class="form-group" id="dvSekolah" style="display: none">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"></label>
            <div class="col-md-4 col-sm-4 col-xs-12"></div>
            <div class="col-md-5 col-sm-5 col-xs-12">
                <select  name="school_id" class="select2_single form-control col-md-7 col-xs-12"  >
                    <option value=""  selected="true">Pilih Sekolah</option>
                    @foreach ($Schools as $key => $value)
                    <option value="{{$value->id}}">{{$value->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        @endif
        @if(Auth::user()->hasRole('Admin Institusi') || Auth::user()->hasRole('Admin Cluever'))
        <div class="form-group" >
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"></label>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <label for="chkPassport">
                    <input type="checkbox" value="2" name="role_id1[]" {{ $users->hasRole('Admin Institusi') ? 'checked': ""}} />
                    Admin Institusi
                </label>
            </div>
        </div>           
        <div class="form-group" >
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"></label>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <label for="chkPassport">
                    <input type="checkbox" value="4" {{ $users->hasRole('Educator') ? 'checked': ""}} name="role_id1[]" />
                    Educator
                </label>
            </div>
        </div>
        <div class="form-group" >
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"></label>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <label for="chkPassport">
                    <input type="checkbox" value="5" {{ $users->hasRole('Wali Kelas') ? 'checked': ""}} name="role_id1[]" />
                    Wali Kelas
                </label>
            </div>
        </div>
        <div class="form-group" >
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"></label>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <label for="chkPassport">
                    <input type="checkbox" {{ $users->hasRole('Orang Tua') ? 'checked': ""}} value="6" name="role_id1[]" />
                    Orang Tua
                </label>
            </div>
        </div>
            @if($errors->has('role_id1'))
            <div class="text-danger">
                {{ $errors->first('role_id1')}}
            </div>
            @endif
        <div class="form-group" >
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"></label>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <label for="chkPassport">
                    <input type="checkbox" value="7" {{ $users->hasRole('Siswa') ? 'checked': ""}} name="role_id1[]" id="chkSiswa" />
                    Siswa
                </label>
            </div>
            <div class="form-group" id="dvSekolah" style="display: none">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"></label>
                <div class="col-md-4 col-sm-4 col-xs-12"></div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                    <select  name="school_id" class="select2_single form-control col-md-7 col-xs-12"  >
                        <option value="0"  selected="true">Pilih Sekolah</option>
                        @foreach ($Schools as $key => $value)
                        <option value="{{$value->id}}">{{$value->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        @if($errors->has('role_id1'))
        <div class="text-danger">
            {{ $errors->first('role_id1')}}
        </div>
        @endif
        
        @endif
    </div>
</div>
<hr>
<input type="submit" value="Simpan" class="btn btn-primary">
@endsection
@section('add-script')

<script type="text/javascript">
    $(function () {
        $("#chkInstitusi").click(function () {
            if ($(this).is(":checked")) {
                $("#dvInstitusi").show();
                $("#AddPassport").hide();
            } else {
                $("#dvInstitusi").hide();
                $("#AddPassport").show();
            }
        });
    });
    $(function () {
        $("#chkEducator").click(function () {
            if ($(this).is(":checked")) {
                $("#dvEducator").show();
                $("#AddPassport").hide();
            } else {
                $("#dvEducator").hide();
                $("#AddPassport").show();
            }
        });
    });
    $(function () {
        $("#chkWali").click(function () {
            if ($(this).is(":checked")) {
                $("#dvWali").show();
                $("#AddPassport").hide();
            } else {
                $("#dvWali").hide();
                $("#AddPassport").show();
            }
        });
    });
    $(function () {
        $("#chkSiswa").click(function () {
            if ($(this).is(":checked")) {
                $("#dvSiswa").show();
                $("#dvSekolah").show();
                $("#AddPassport").hide();
            } else {
                $("#dvSiswa").hide();
                $("#dvSekolah").hide();
                $("#AddPassport").show();
            }
        });
    });
    $(function () {
        $("#chkSiswa1").click(function () {
            if ($(this).is(":checked")) {
                $("#dvSiswa1").show();
                $("#dvSekolah1").show();
                $("#AddPassport").hide();
            } else {
                $("#dvSiswa1").hide();
                $("#dvSekolah1").hide();
                $("#AddPassport").show();
            }
        });
    });
    $(function () {
        $("#chkOrangTua").click(function () {
            if ($(this).is(":checked")) {
                $("#dvOrangTua").show();
                $("#AddPassport").hide();
            } else {
                $("#dvOrangTua").hide();
                $("#AddPassport").show();
            }
        });
    });
    $(function () {
        $("#chkPassport1").click(function () {
            if ($(this).is(":checked")) {
                $("#dvPassport1").show();
                $("#AddPassport").hide();
            } else {
                $("#dvPassport1").hide();
                $("#AddPassport").show();
            }
        });
    });
    $(document).on('click', '.remove-tr', function(){ 
     $(this).parents('.f').remove();
 });
    $(document).ready(function() {
        function initiateSelect() {
            $(".select2_single").select2({
                placeholder: "Pilih",
                allowClear: false
            });
            $(".select2_group").select2({});
            $(".select2_multiple").select2({
                maximumSelectionLength: 4,
                placeholder: "With Max Selection limit 4",
                allowClear: true
            });
        }
        $(".gaji").click(function(){ 
            var html = $(".copy").html();
            $(".after-gaji").after(html);
        });
        $("body").on("click",".remove_gaji",function(){ 
            $(this).parents(".control-group").remove();
        });
        $(".role").click(function(){ 
            var html = $(".copy_role").html();
            $(".after-role").after(html);
            initiateSelect()
        });
        $("body").on("click",".remove_role",function(){ 
            $(this).parents(".control-group").remove();
        });
        const roleOptions = `
        <div class="col-md-5 col-sm-5 col-xs-12">
        <select class="select2_single form-control role_options" name="role_id[]" >
        <option value="0">Pilih..</option>
        <option value="1">Admin Cluever</option>
        <option value="2">Admin Institusi</option>
        <option value="3">Verifikator</option>
        <option value="4">Educator</option>
        <option value="5">Wali Kelas</option>
        <option value="6">Orang Tua</option>
        <option value="7">Siswa</option>
        </select>
        </div>`
        const roleAddButton = `<div class="col-md-2 col-sm-2 col-xs-12 button_container">
        <button type="button" name="add" id="add" class="btn btn-success">+ Add</button>
        </div>`
        const roleContainer = `<div class="row" style="margin-top: 4px;">${ roleOptions }</div>`
        $('#dynamicOptions').append(roleContainer)
        initiateSelect()
        var i = 0;
        $(document).on('click', '#add', function() {
            ++i;
            $("#dynamicOptions").append(roleContainer);
            $(".select2_single").select2({
                placeholder: "Pilih",
                allowClear: false
            });
            $(".select2_group").select2({});
            $(".select2_multiple").select2({
                maximumSelectionLength: 4,
                placeholder: "With Max Selection limit 4",
                allowClear: true
            });
            $(this).remove()
        });
        $(function(){
            const institutionOptions = `
            <div name="institusi" class="col-md-5 col-sm-5 col-xs-12">
            <select  name="institution_id" class="select2_single form-control col-md-7 col-xs-12"  >
            <option value="">Pilih Institusi</option>
            <option value="1">Edulab</option>
            <option value="2">SSC</option>
            <option value="3">Tridaya</option>
            </select>
            </div>`
            const schoolOptions = `
            <div name="siswa" class="col-md-3 col-sm-3 col-xs-12">
            <select  name="school_id" class="select2_single form-control col-md-7 col-xs-12"  >
            <option value="">Pilih Sekolah</option>
            <option value="1">SMA 2 Bandung</option>
            <option value="2">SMA 3 Bandung</option>
            <option value="3">SMA 4 Bandung</option>
            </select>
            </div>`
            const removeButton = `
            <div class="col-md-1 remove_button">
            <button class="btn btn-danger">X</button>
            </div>`
            $(document).on('change', '.role_options', function() {
                // $(this).parent().siblings('.button_container')[0].remove()
                if ($(this).val()) {
                    var container = $(this).parent().parent()
                    switch ($(this).val()) {
                        case '2':
                        container.append(institutionOptions)
                        break
                    }
                    container.append(roleAddButton)
                    initiateSelect()
                } else {
                    // hilangkan elemen
                }
            });
        });
    });
</script>
@endsection
