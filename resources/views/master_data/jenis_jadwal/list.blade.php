@extends('layouts.list')
@if(Auth::user()->hasRole('Admin Cluever'))
    @section('title')
    <h3 class="back-btn">
        <a href="javascript:history.go(-1)">
            <i class="fa fa-arrow-left"></i>
        </a>
    </h3>
    <h3 class="text-center">Jenis Jadwal {{$Institusi->name}}</h3>
    @endsection
    @section('add-button')
    <a href="{{ URL('jenis_jadwal/create/'.$Institusi->id) }}" class="btn btn-success new-data-btn"> Tambah Jenis Jadwal Baru </a>
    @endsection
@else
    @section('title')
    <h3 class="text-center">Jenis Jadwal</h3>
    @endsection
    @section('add-button')
    <a href="{{ URL('jenis_jadwal/create/'. Auth::user()->institution_user->institution_id) }}" class="btn btn-success new-data-btn"> Tambah Jenis Jadwal Baru </a>
    @endsection
@endif

@section('content')
<table id="example" class="table table-striped responsive-utilities jambo_table">
    <thead class="headings">
        <tr class="headings">
            <th style="width: 5%">No</th>
            <th style="width: 20%">Jenis Jadwal</th>
            <th style="width: 25%">Deskripsi </th>
            <th style="width: 20%">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        @foreach($JenisJadwal as $a)
        <tr class="odd pointer">
            <td>{{$no++}}</td>
            <td class=" ">{{ $a->name }}</td>
            <td class=" ">{{ $a->description }}</td>
            <td class=" last">
                <a href="/jenis_jadwal/{{ $a->id }}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Detail </a>
                <a href="/jenis_jadwal/edit/{{ $a->id }}" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                <a href="#" onclick="var c = confirm('Apakah Anda yakin ingin menghapus jenis jadwal ini?'); if (c) {$('#delete-jenis_jadwal-{{ $a->id }}').submit()}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete </a>
                <form id="delete-jenis_jadwal-{{ $a->id }}" method="POST" action="/jenis_jadwal/delete/{{ $a->id }}">
                    {{ csrf_field() }}
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection