@extends('layouts.form')
@section('title')
@if ($JenisJadwal->id)
<h3 class="text-center">Edit Jenis Jadwal</h3>
@else
<h3 class="text-center">Tambah Jenis Jadwal Baru</h3>
@endif
@endsection

@section('form-open')
    @if ($JenisJadwal->id)
    {!! Form::open([

            'url' => '/jenis_jadwal/update/' . $JenisJadwal->id,
            'enctype' =>'multipart/form-data',
            'method' => 'PUT','class' => 'form-horizontal form-label-left'
        ]) 
    !!}
    @else
    {!! Form::open([
        'url' => '/jenis_jadwal/store',
        'enctype' =>'multipart/form-data',
        'class' => 'form-horizontal form-label-left'
        ]) 
    !!}
    <input type="text" id="last-name" name="institution_id" style="display: none" value="{{$id}}" class="form-control col-md-7 col-xs-12">
    @endif
@endsection
@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            <label class="control-label col-md-3" for="first-name">Jenis Jadwal</label>
            <div class="col-md-9">
                <input type="text" id="last-name" name="name" value="{{ old('name',  isset($JenisJadwal->name) ? $JenisJadwal->name : null) }}" class="form-control col-md-7 col-xs-12">
                @if($errors->has('name'))
                <div class="text-danger">
                    {{ $errors->first('name')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3" for="first-name">Deskripsi</label>
            <div class="col-md-9">
                <textarea id="message"  class="form-control" name="description" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10">{{ old('description',  isset($JenisJadwal->description) ? $JenisJadwal->description : null) }} </textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Presensi Oleh Admin</label>
	    <div class="col-md-9 col-sm-9 col-xs-12">
		<input type="radio" class="flat" name="admin_input" value="1" /> Ya
                <input type="radio" class="flat" name="admin_input" value="0" /> Tidak

                <!-- <select id="heard"  name="admin_input" class="select2_single form-control" >
                    <option value="">Pilih..</option>
                    <option value="1" {{old('admin_input',isset($JenisJadwal->admin_input) ? $JenisJadwal->admin_input : null) == "1" ? 'selected' : ''}}>Ya</option>
                    <option value="0" {{old('admin_input',isset($JenisJadwal->admin_input) ? $JenisJadwal->admin_input : null) == "0" ? 'selected' : ''}}>Tidak</option>
                </select> -->
                @if($errors->has('admin_input'))
                <div class="text-danger">
                    {{ $errors->first('admin_input')}}
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<hr>
<input type="submit" value="Simpan" class="btn btn-primary">
@endsection
