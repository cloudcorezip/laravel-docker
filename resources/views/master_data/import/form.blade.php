@extends('layouts.form')
@section('title')
<h3 class="text-center">Import Data User</h3>
@endsection
@section('content')
@if (\Session::has('success'))
<div class="alert alert-success alert-dismissable fade in">
{!! \Session::get('success') !!}</div>
@endif
<div class="row">
    <div class="col-md-8">
        {!! Form::open([
            'url' => '/import/importuser',
            'enctype' =>'multipart/form-data',
            'class' => 'form-horizontal form-label-left',
            'method' => 'POST'
            ]) 
        !!}
            @csrf

            @if (session('error'))
                <div class="alert alert-success">
                    {{ session('error') }}
                </div>
            @endif
        <div class="form-group">
            <label class="control-label col-md-3" for="first-name">Pilih Institusi</label>
            <div class="col-md-9">
                <select id="heard"  name="institution_id" class="select2_single form-control" >
                    <option value="">Pilih..</option>
                    @foreach($institutions as $p)
                    <option value="{{$p->id}}">{{$p->name}}</option>
                    @endforeach 
                </select>
                @if($errors->has('institution_id'))
                <div class="text-danger">
                    {{ $errors->first('institution_id')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3" for="first-name">File (.xls, .xlsx)</label>
            <div class="col-md-9">
                <input type="file" class="form-control" name="file">
                <p class="text-danger">{{ $errors->first('file') }}</p>
            </div>
        </div>
    </div>
</div>
<hr>
<input type="submit" value="Upload" class="btn btn-primary">
@endsection
