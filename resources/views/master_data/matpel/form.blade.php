@extends('layouts.form')
@section('title')
@if ($matpel->id)
<h3 class="text-center">Edit Mata Pelajaran</h3>
@else
<h3 class="text-center">Tambah Mata Pelajaran Baru</h3>
@endif
@endsection

@section('form-open')
    @if ($matpel->id)
    {!! Form::open([

            'url' => '/matpel/update/' . $matpel->id,
            'enctype' =>'multipart/form-data',
            'method' => 'PUT','class' => 'form-horizontal form-label-left'
        ]) 
    !!}
    @else
    {!! Form::open([
        'url' => '/matpel/store',
        'enctype' =>'multipart/form-data',
        'class' => 'form-horizontal form-label-left'
        ]) 
    !!}
    <input type="text" id="last-name" name="institution_id" style="display: none" value="{{$id}}" class="form-control col-md-7 col-xs-12">
    @endif
@endsection
@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            <label class="control-label col-md-3" for="first-name">Mata Pelajaran</label>
            <div class="col-md-9">
                <input type="text" id="last-name" name="name" value="{{ old('name',  isset($matpel->name) ? $matpel->name : null) }}" class="form-control col-md-7 col-xs-12">
                @if($errors->has('name'))
                <div class="text-danger">
                    {{ $errors->first('name')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3" for="first-name">Deskripsi</label>
            <div class="col-md-9">
                <textarea id="message"  class="form-control" name="description" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10">{{ old('description',  isset($matpel->description) ? $matpel->description : null) }}</textarea>
            </div>
        </div>
    </div>
</div>
<hr>
<input type="submit" value="Simpan" class="btn btn-primary">
@endsection
