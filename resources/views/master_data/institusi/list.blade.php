@extends('layouts.list')

@section('title')
<h3 class="text-center">Institusi</h3>
@endsection

@section('add-button')
<a href="{{ URL('institusi/create') }}" class="btn btn-success new-data-btn"> Tambah Institusi Baru </a>
@endsection

@section('content')
<table id="example" class="table table-striped responsive-utilities jambo_table">
    <thead>
        <tr class="headings">
            <th style="width: 1%">No</th>
            <th style="width: 10%">Nama Institutsi</th>
            <th style="width: 15%">Alamat </th>
            <th style="width: 25%">Deskripsi</th>
            <th style="width: 15%">Action</th>
        </tr>
    </thead>
    <tbody>
         <?php $no = 1; ?>
        @foreach($institusi as $a)
        <tr class="odd pointer">
            <td>{{$no++}}</td>
            <td class=" ">{{ $a->name }}</td>
            <td class=" ">{{ $a->address }}</td>
            <td class=" ">{{ str_limit($a->description, 100, '...') }}</td>
            <td class=" last">
                <a href="/institusi/{{ $a->id }}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Detail </a>
                <a href="/institusi/edit/{{ $a->id }}" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                <a href="#" onclick="var c = confirm('Apakah Anda yakin ingin menghapus institusi ini?'); if (c) {$('#delete-institusi-{{ $a->id }}').submit()}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete </a>
                <form id="delete-institusi-{{ $a->id }}" method="POST" action="/institusi/delete/{{ $a->id }}">
                    {{ csrf_field() }}
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
