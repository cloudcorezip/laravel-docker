@extends('layouts.blank')

@push('stylesheets')
<link href="{{ asset("css/animate.min.css") }}" rel="stylesheet">
<link href="{{ asset("css/custom.css") }}" rel="stylesheet">
<link href="{{ asset("css/icheck/flat/green.css") }}" rel="stylesheet" />
<link href="{{ asset("css/datatables/tools/css/dataTables.tableTools.css") }} rel="stylesheet">
@endpush

@section('main_container')
<div class="right_col" role="main">
    <div class="">               
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h3 class="back-btn">
                            <a href="{{ URL::previous() }}">
                                <i class="fa fa-arrow-left"></i>
                            </a>
                        </h3>
                        <h3 class="text-center">{{$institusi->name}}<small></small></h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                            <div class="profile_img">
                                <div id="crop-avatar">
                                    <div class="avatar-view" title="Change the avatar">
                                        <img src="{{ asset("images/$institusi->logo_path") }}" alt="img" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 profile_left">
                            <ul class="list-unstyled user_data">
                                <li><i class="fa fa-map-marker user-profile-icon"></i> {{$institusi->address}}
                                </li>
                                <li class="m-top-xs">
                                    <i class="fa fa-external-link user-profile-icon"></i>
                                    <a href="{{ URL("$institusi->website") }}" target="_blank">{{$institusi->website}}</a>
                                </li>
                                <li>
                                    <i class="fa fa-briefcase user-profile-icon"></i> {{$institusi->description}}
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <br>
                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Program Institusi</a>
                                    </li>
                                    <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Educator</a>
                                    </li>
                                </ul>
                                <div id="myTabContent" class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                        <a href="/program/create_program/{{$institusi->id}}" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a>
                                        <table class=" example table table-striped responsive-utilities jambo_table">
                                            <thead>
                                                <tr class="headings">
                                                    <th>No</th>
                                                    <th>Nama Program</th>
                                                    <th>Deskripsi </th>
                                                    <th>Biaya</th>
                                                    <th>Diskon</th>
                                                    <th class=" no-link last"><span class="nobr">Action</span>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $no = 1; ?>
                                                @foreach($program as $a)
                                                <tr class="odd pointer">
                                                    <td>{{$no++}}</td>
                                                    <td class=" ">{{ $a->name }}</td>
                                                    <td class=" ">{{ $a->description }}</td>
                                                    <td class=" ">{{ $a->price }}</td>
                                                    <td class=" ">{{ $a->discount }}</td>
                                                    <td class=" last">
                                                        <a href="/program/edit/{{ $a->id }}" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                                        <a href="#" onclick="var c = confirm('Apakah Anda yakin ingin menghapus pengajuan ini?'); if (c) {$('#delete-program-{{ $a->id }}').submit()}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete </a>
                                                        <form id="delete-program-{{ $a->id }}" method="POST" action="/program/delete/{{ $a->id }}">
                                                            {{ csrf_field() }}
                                                        </form>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                        <a href="/tambah_program/{{$institusi->id}}" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a>
                                        <table class="example table table-striped responsive-utilities jambo_table">
                                            <thead>
                                                <tr class="headings">
                                                    <th>No</th>
                                                    <th>NIK</th>
                                                    <th>Nama</th>
                                                    <th>No HP</th>
                                                    <th>Email </th>
                                                    <th>Alamat</th>
                                                    <th class=" no-link last"><span class="nobr">Action</span>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset("js/nicescroll/jquery.nicescroll.min.js") }}"></script>
<script src="{{ asset("js/icheck/icheck.min.js") }}"></script>
<script src="{{ asset("js/custom.js") }}"></script>
<script src="{{ asset("js/datatables/js/jquery.dataTables.js") }}"></script>
<script src="{{ asset("js/datatables/tools/js/dataTables.tableTools.js") }}"></script>
<script>
    $(document).ready(function () {
        $('input.tableflat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });

    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('.example').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            },
            "aoColumnDefs": [
            {
                'bSortable': false,
                'aTargets': [0]
                        } //disables sorting for column one
                        ],
                        'iDisplayLength': 10,
                        "sPaginationType": "full_numbers",

                    });
        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });
</script>

@endpush
