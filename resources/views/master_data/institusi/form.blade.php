@extends('layouts.form')

@section('title')
@if ($institusi->id)
<h3 class="text-center">Edit Institusi</h3>
@else
<h3 class="text-center">Tambah Institusi Baru</h3>
@endif
@endsection

@section('form-open')
    @if ($institusi->id)
    {!! Form::open([
            'url' => '/institusi/update/' . $institusi->id,
            'enctype' =>'multipart/form-data',
            'method' => 'PUT','class' => 'form-horizontal form-label-left'
        ]) 
    !!}
    @else
    {!! Form::open([
        'url' => '/institusi/store',
        'enctype' =>'multipart/form-data',
        'class' => 'form-horizontal form-label-left'
        ]) 
    !!}
    @endif
@endsection
@section('content')
<div class="row">
    <div class="col-md-8 col-sm-12 col-xs-12">
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Institusi</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" id="last-name" name="name" value="{{ old('name',  isset($institusi->name) ? $institusi->name : null) }}"  class="form-control col-md-7 col-xs-12">
                @if($errors->has('name'))
                <div class="text-danger">
                    {{ $errors->first('name')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Provinsi</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="select2_single form-control" name="provinces" id="provinces">
                    <option value="0" disabled="true" selected="true">Pilih Provinsi</option>
                    @foreach ($province as $key => $value)
                    <option value="{{$value->id}}">{{$value->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kota / Kabupaten</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="select2_single form-control" name="regencies" id="regencies">
                  <option value="0" disable="true" selected="true">Pilih Kota/Kab</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kecamatan</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="select2_single form-control" name="districts" id="districts">
                  <option value="0" disable="true" selected="true">Pilih Kecamatan</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kelurahan</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="select2_single form-control" name="postcode_id" id="postcode_id">
                  <option value="" disable="true" selected="true">Pilih Kelurahan</option>
                </select>
                <!-- <select id="heard"  name="postcode_id" class="select2_single form-control" >
                    <option value="">Pilih..</option>
                    @foreach ($postcode as $p)
                    <option value="{{$p->id}}" {{old('postcode_id',isset($users->user_detail->postcode_id) ? $users->user_detail->postcode_id : null) == $p->id ? 'selected' : ''}}>{{$p->code}}</option>
                    @endforeach 
                    
                </select> -->
                @if($errors->has('postcode_id'))
                <div class="text-danger">
                    {{ $errors->first('postcode_id')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Alamat Lengkap</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" id="last-name" value="{{ old('address',  isset($institusi->address) ? $institusi->address : null) }}" name="address"  class="form-control col-md-7 col-xs-12">
                @if($errors->has('address'))
                <div class="text-danger">
                    {{ $errors->first('address')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Deskripsi</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <textarea id="message"  class="form-control" name="description" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10">{{ old('description',  isset($institusi->description) ? $institusi->description : null) }}</textarea>
                @if($errors->has('description'))
                <div class="text-danger">
                    {{ $errors->first('description')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Website</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" name="website" id="last-name" value="{{ old('website',  isset($institusi->website) ? $institusi->website : null) }}"   class="form-control col-md-7 col-xs-12">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Logo</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                @if(isset($institusi->logo_path))
                <img src="{{ asset('images/'.$institusi->logo_path) }}" style="width:  173px; height:100px;"/>
                @endif
                <input type="file" id="last-name" name="logo_path"  class="form-control col-md-7 col-xs-12">
                @if($errors->has('logo_path'))
                <div class="text-danger">
                    {{ $errors->first('logo_path')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group increment" >
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Slider</label>
            <div class="col-md-7 col-sm-7 col-xs-12">
                <input type="file" name="path[]" class="form-control ">
                @if($errors->has('path'))
                <div class="text-danger">
                    {{ $errors->first('path')}}
                </div>
                @endif
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12"> 
                <button class="btn btn-success slip" type="button"><i class="glyphicon glyphicon-plus"></i> Tambah</button>
            </div>
        </div>
        <div class="clone hide">
            <div class="form-group control-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"></label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                    <input type="file" name="path[]" class="form-control">
                </div>

                <div class="col-md-2 col-sm-2 col-xs-12"> 
                    <button class="btn btn-danger" type="button"><i class="fa fa-trash"></i> Hapus</button>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<input type="submit" value="Simpan" class="btn btn-primary">
@endsection