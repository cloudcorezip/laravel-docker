@extends('layouts.list')

@section('title')
<h3 class="text-center">Program Belajar</h3>
@endsection

@section('add-button')
<a href="{{ URL('program/create') }}" class="btn btn-success new-data-btn"> Tambah Program Baru </a>
@endsection

@section('content')
<table id="example" class="table table-striped responsive-utilities jambo_table">
    <thead class="headings">
        <tr class="headings">
            <th style="width: 5%">No</th>
            <th style="width: 20%">Nama Program</th>
            <th style="width: 25%">Deskripsi </th>
            <th style="width: 15%">Biaya</th>
            <th style="width: 10%">Diskon</th>
            <th style="width: 20%">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        @foreach($program as $a)
        <tr class="odd pointer">
            <td>{{$no++}}</td>
            <td class=" ">{{ $a->name }}</td>
            <td class=" ">{{ $a->description }}</td>
            <td class=" ">{{ $a->price }}</td>
            <td class=" ">{{ $a->discount }}</td>
            <td class=" last">
                <a href="/program/{{ $a->id }}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Detail </a>
                <a href="/program/edit/{{ $a->id }}" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                <a href="#" onclick="var c = confirm('Apakah Anda yakin ingin menghapus program ini?'); if (c) {$('#delete-program-{{ $a->id }}').submit()}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete </a>
                <form id="delete-program-{{ $a->id }}" method="POST" action="/program/delete/{{ $a->id }}">
                    {{ csrf_field() }}
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection