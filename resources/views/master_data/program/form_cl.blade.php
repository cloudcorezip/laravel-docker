@extends('layouts.form')
@section('title')
@if ($program->id)
<h3 class="text-center">Edit Program Belajar</h3>
@else
<h3 class="text-center">Tambah Program Belajar Baru</h3>
@endif
@endsection

@section('form-open')
    @if ($program->id)
    {!! Form::open([

            'url' => '/program/update/' . $program->id,
            'enctype' =>'multipart/form-data',
            'method' => 'PUT','class' => 'form-horizontal form-label-left'
        ]) 
    !!}
    @else
    {!! Form::open([
        'url' => '/program/store',
        'enctype' =>'multipart/form-data',
        'class' => 'form-horizontal form-label-left'
        ]) 
    !!}
    <input type="text" id="last-name" name="institution_id" style="display: none" value="{{ $id }}" class="form-control col-md-7 col-xs-12">
    @endif
@endsection
@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            <label class="control-label col-md-3" for="first-name">Nama Program Belajar</label>
            <div class="col-md-9">
                <input type="text" id="last-name" name="name" value="{{ old('name',  isset($program->name) ? $program->name : null) }}" class="form-control col-md-7 col-xs-12">
                @if($errors->has('name'))
                <div class="text-danger">
                    {{ $errors->first('name')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3" for="first-name">Deskripsi</label>
            <div class="col-md-9">
                <textarea id="message"  class="form-control" name="description" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10">{{ old('description',  isset($program->description) ? $program->description : null) }} </textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3" for="first-name">Biaya</label>
            <div class="col-md-9">
                <input type="number" id="last-name" name="price" value="{{ old('price',  isset($program->price) ? $program->price : null) }}" class="form-control col-md-7 col-xs-12">
                @if($errors->has('price'))
                <div class="text-danger">
                    {{ $errors->first('price')}}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3" for="first-name">Diskon</label>
            <div class="col-md-9">
                <input type="number" id="last-name" name="discount" value="{{ old('discount',  isset($program->discount) ? $program->discount : null) }}" class="form-control col-md-7 col-xs-12">
                @if($errors->has('discount'))
                <div class="text-danger">
                    {{ $errors->first('discount')}}
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<hr>
<input type="submit" value="Simpan" class="btn btn-primary">
@endsection
