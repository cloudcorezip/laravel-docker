@extends('layouts.detail')

@section('title')
<h3 class="text-center">{{$kelas->class_code}}</h3>
@endsection
@section('content')
<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-6">
        <div class="dashboard_graph x_panel">
            <div class="x_title">
                <h2>Pilih Siswa</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped responsive-utilities jambo_table example">
                    <thead>
                        <tr class="headings">
                            <th>No </th>
                            <th>NIK </th>
                            <th>Nama</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        @foreach($user_roles as $a)

                        <tr class="odd pointer">
                            <td>{{$no++}}</td>
                            <td class=" ">{{ $a->users->nik}}</td>
                            <td class=" ">{{ $a->users->user_detail->fullname }}</td>
                            <td><a href="/kelas/add_siswakelas/{{$a->users->id}}/{{$kelas->id}}" class="btn btn-success btn-xs"><i class="fa fa-plus"></i></a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6">
        <div class="dashboard_graph x_panel">
            <div class="x_title">
                <h2>Siswa</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table  class="table table-striped responsive-utilities jambo_table example">
                    <thead>
                        <tr class="headings">
                            <th>No </th>
                            <th>NIK </th>
                            <th>Nama</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $no = 1; ?>
                        @foreach($student_class as $a)
                        <tr class="odd pointer">
                            <td class=" ">{{$no++}}</td>
                            <td>{{$a->users->nik}}</td>
                            <td>{{$a->users->user_detail->fullname}} </td>
                            <td>
                                <a href="#" onclick="var c = confirm('Apakah Anda yakin ingin menghapus siswa ini?'); if (c) {$('#delete_siswakelas-{{ $a->id }}').submit()}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
                                <form id="delete_siswakelas-{{ $a->id }}" method="POST" action="/kelas/delete_siswakelas/{{ $a->id }}">
                                    {{ csrf_field() }}
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('add-script')
<script>
    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('.example').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            },
            "aoColumnDefs": [
            {
                'bSortable': false,
                'aTargets': [0]
                        } //disables sorting for column one
                        ],
                        'iDisplayLength': 10,
                        "sPaginationType": "full_numbers",

                    });
        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });
</script>
@endsection