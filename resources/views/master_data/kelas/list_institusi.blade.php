@extends('layouts.list')

@section('title')
<h3 class="text-center">Manajemen Kelas</h3>
@endsection

@section('content')
<table id="example" class="table table-striped responsive-utilities jambo_table">
    <thead class="headings">
        <tr class="headings">
            <th >No</th>
            <th >Nama Institusi</th>
            <th >Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        @foreach($institutions as $a)
        <tr class="odd pointer">
            <td>{{$no++}}</td>
            <td class=" ">{{ $a->name }}</td>
            <td class=" last">
                <a href="/kelas/detail_institusi/{{ $a->id }}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Detail </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection