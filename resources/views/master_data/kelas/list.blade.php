@extends('layouts.list')
@if(Auth::user()->hasRole('Admin Cluever'))
    @section('title')
    <h3 class="back-btn">
        <a href="javascript:history.go(-1)">
            <i class="fa fa-arrow-left"></i>
        </a>
    </h3>
    <h3 class="text-center">Manajemen Kelas {{$Institusi->name}}</h3>
    @endsection
    @section('add-button')
    <a href="{{ URL('kelas/create/'.$Institusi->id) }}" class="btn btn-success new-data-btn"> Tambah Kelas Baru </a>
    @endsection
@else
    @section('title')
    <h3 class="text-center">Manajemen Kelas</h3>
    @endsection
    @section('add-button')
    <a href="{{ URL('kelas/create/'. Auth::user()->institution_user->institution_id) }}" class="btn btn-success new-data-btn"> Tambah Kelas Baru </a>
    @endsection
@endif

@section('content')
<table id="example" class="table table-striped responsive-utilities jambo_table">
    <thead>
        <tr class="headings">
            <th style="width: 1%">No</th>
            <th style="width: 10%">Tahun Ajaran</th>
            <th style="width: 10%">Kode Kelas </th>
            <th style="width: 10%">Kelas </th>
            <th style="width: 10%">Program Belajar </th>
            <!-- <th style="width: 20%">Hari</th> -->
            <th style="width: 10%">Wali Kelas</th>
            <th style="width: 10%">Kapasitas</th>
            <th style="width: 15%">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($kelas as $no => $a)
        <tr class="odd pointer">
            <td>{{$no + 1}}</td>
            <td class=" ">{{$a->school_year->name}}</td>
            <td class=" ">{{$a->class_code}}</td>
            <td class=" ">{{$a->class_name}}</td>
            <td class="a-right a-right ">{{$a->programs->name}}</td>
            <!-- <td class=" ">
                @foreach($a->schedule as $idx => $t)
                {{ $t ? $t->day : '-' }}{{$idx === sizeOf($a->schedule) - 1 ? '' : ','}}
                @endforeach
                {{sizeOf($a->schedule) === 0 ? '-' : ''}}
            </td> -->
            <td>
                {{$a->homeroom_teacher->nik}} 
                <br />
                <small>{{$a->homeroom_teacher->user_detail->fullname}}</small>
            </td>
            <td class=" ">{{$a->capacity}} orang</td>
            <td class=" last">
                <a href="/kelas/{{ $a->id }}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Detail </a>
                <a href="/kelas/edit/{{ $a->id }}" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                <a href="#" onclick="var c = confirm('Apakah Anda yakin ingin menghapus Kelas ini?'); if (c) {$('#delete-pengajuan-{{ $a->id }}').submit()}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete </a>
                <form id="delete-kelas-{{ $a->id }}" method="POST" action="/kelas/delete/{{ $a->id }}">
                    {{ csrf_field() }}
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
