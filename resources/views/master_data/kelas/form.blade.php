@extends('layouts.form')
@section('title')
@if ($classes->id)
<h3 class="text-center">Edit Kelas</h3>
@else
<h3 class="text-center">Tambah Kelas Baru</h3>
@endif
@endsection

@section('form-open')
    @if ($classes->id)
    {!! Form::open([
            'url' => '/kelas/update/'.$classes->id,
            'enctype' =>'multipart/form-data',
            'method' => 'PUT','class' => 'form-horizontal form-label-left'
        ]) 
    !!}
    @else
    {!! Form::open([
        'url' => '/kelas/store',
        'enctype' =>'multipart/form-data',
        'class' => 'form-horizontal form-label-left'
        ]) 
    !!}
    <input type="text" id="last-name" name="institution_id" style="display: none" value="{{$id}}" class="form-control col-md-7 col-xs-12">
    @endif
@endsection
@section('content')
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kode Kelas</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="last-name" value="{{ old('class_code',  isset($classes->class_code) ? $classes->class_code : null) }}" name="class_code"  class="form-control ">
        @if($errors->has('class_code'))
        <div class="text-danger">
            {{ $errors->first('class_code')}}
        </div>
        @endif
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kelas
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <select id="heard" class="select2_single form-control" name="class_name" >
            <option value="">Pilih..</option>
            <option value="7 SMP">7 SMP</option>
            <option value="8 SMP">8 SMP</option>
            <option value="9 SMP">9 SMP</option>
            <option value="10 SMA IPA">10 SMA IPA</option>
            <option value="11 SMA IPA">11 SMA IPA</option>
            <option value="12 SMA IPA">12 SMA IPA</option>
            <option value="10 SMA IPS">10 SMA IPS</option>
            <option value="11 SMA IPS">11 SMA IPS</option>
            <option value="12 SMA IPS">12 SMA IPS</option>
        </select>
        @if($errors->has('class_name'))
        <div class="text-danger">
            {{ $errors->first('class_name')}}
        </div>
        @endif
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tahun Ajaran
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <select id="heard"  name="school_year_id" class="select2_single form-control" >
            <option value="">Pilih..</option>
            @foreach ($SchoolYear as $p)
            <option value="{{$p->id}}" {{old('school_year_id',isset($classes->school_year_id) ? $classes->school_year_id : null) == $p->id ? 'selected' : ''}}>{{$p->name}}</option>
            @endforeach 
        </select>
        @if($errors->has('school_year_id'))
        <div class="text-danger">
            {{ $errors->first('school_year_id')}}
        </div>
        @endif
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Program Belajar 
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <select id="heard"  name="program_id" class="select2_single form-control" >
            <option value="">Pilih..</option>
            @foreach ($program as $p)
            <option value="{{$p->id}}" {{old('program_id',isset($classes->program_id) ? $classes->program_id : null) == $p->id ? 'selected' : ''}}>{{$p->name}}</option>
            @endforeach 
        </select>
        @if($errors->has('program_id'))
        <div class="text-danger">
            {{ $errors->first('program_id')}}
        </div>
        @endif
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Wali Kelas
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        
        <select id="heard"  name="homeroom_teacher_id" class="select2_single form-control" >
            <option value="">Pilih..</option>
            @foreach ($user_roles as $p)
            <option value="{{$p->users->id}}" {{old('homeroom_teacher_id',isset($classes->homeroom_teacher_id) ? $classes->homeroom_teacher_id : null) == $p->users->id ? 'selected' : ''}}>{{$p->users->user_detail->fullname}} ({{$p->users->nik}})</option>
            @endforeach 
            
        </select>
        @if($errors->has('homeroom_teacher_id'))
        <div class="text-danger">
            {{ $errors->first('homeroom_teacher_id')}}
        </div>
        @endif
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kapasitas</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="number" id="last-name" value="{{ old('capacity',  isset($classes->capacity) ? $classes->capacity : null) }}" name="capacity"  class="form-control ">
        @if($errors->has('capacity'))
        <div class="text-danger">
            {{ $errors->first('capacity')}}
        </div>
        @endif
    </div>
</div>
<hr>
<input type="submit" value="Simpan" class="btn btn-primary">
@endsection