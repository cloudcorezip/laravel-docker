<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Auth::routes();
Route::post('/logout', 'Auth\LoginController@logout');
Route::get('/', 'HomeController@index');

Route::group(['prefix' => 'payment', 'middleware' => ['auth','roles'], 'roles' => ['Admin Cluever','Admin Institusi']], function()
{
	Route::get('/', 'PaymentController@index');
	Route::post('delete/{id}', 'PaymentController@delete');
	Route::get('create', 'PaymentController@create')->name('payment.create');
	Route::get('edit/{id}', 'PaymentController@edit');
	Route::put('update/{id}', 'PaymentController@update');
	Route::post('store', 'PaymentController@store');
	Route::get('{id}', 'PaymentController@show');
});
Route::group(['prefix' => 'proposal', 'middleware' => ['auth','roles'], 'roles' => ['Admin Cluever','Admin Institusi']], function()
{
	Route::get('/', 'RegistrationProposalController@index');
	Route::post('delete/{id}', 'RegistrationProposalController@delete');
	Route::get('create', 'RegistrationProposalController@create');
	Route::get('edit/{id}', 'RegistrationProposalController@edit');
	Route::put('update/{id}', 'RegistrationProposalController@update');
	Route::post('store', 'RegistrationProposalController@store');
	Route::get('{id}', 'RegistrationProposalController@show');
	Route::post('acceptance/{id}', 'RegistrationProposalController@acceptance');
});
Route::group(['prefix' => 'presence', 'middleware' => ['auth','roles'], 'roles' => ['Admin Cluever','Admin Institusi','Educator']], function()
{
	Route::get('/', 'PresenceController@index');
	Route::post('undo_presence/{scheduleId}/{userId}', 'PresenceController@undoPresence');
	Route::post('{status}/{scheduleId}/{userId}', 'PresenceController@presence');
	Route::post('presence/{id}', 'PresenceController@delete');
	Route::post('delete/{id}', 'PresenceController@delete');
	Route::get('create/{id}/{class_id}', 'PresenceController@create');
	Route::get('edit/{id}', 'PresenceController@edit');
	Route::put('update/{id}', 'PresenceController@update');
	Route::post('store', 'PresenceController@store');
	Route::get('{id}', 'PresenceController@show');
});
Route::group(['prefix' => 'pengajuan', 'middleware' => ['auth','roles'], 'roles' => ['Admin Cluever','Verifikator']], function()
{
	Route::get('/', 'ApplicationController@index');
	Route::post('delete/{id}', 'ApplicationController@delete');
	Route::get('create', 'ApplicationController@create');
	Route::get('edit/{id}', 'ApplicationController@edit');
	Route::post('store', 'ApplicationController@store');
	Route::put('update', 'ApplicationController@update');
	Route::post('acceptance/{id}', 'ApplicationController@acceptance');
	Route::get('{id}', 'ApplicationController@detail');
});
Route::group(['prefix' => 'import', 'middleware' => ['auth','roles'], 'roles' => ['Admin Cluever','Admin Institusi']], function()
{
	Route::get('/', 'ImportController@index');
	Route::get('user', 'ImportController@user');
	Route::get('presensi', 'ImportController@presensi');
	Route::post('importuser', 'ImportController@importuser');
	Route::post('importpresensi', 'ImportController@importpresensi');

});

Route::group(['prefix' => 'user', 'middleware' => ['auth','roles'], 'roles' => ['Admin Cluever','Admin Institusi','Siswa','Verifikator','Educator','Orang Tua']], function() 
{
    Route::put('update_password', 'UserController@updatePassword');
    Route::get('change_password', 'UserController@changePassword');
    Route::get('edit/{id}', 'UserController@edit');
});

Route::group(['prefix' => 'user', 'middleware' => ['auth','roles'], 'roles' => ['Admin Cluever','Admin Institusi']], function()
{
	Route::get('/', 'UserController@index');
	Route::post('delete/{id}', 'ApplicationController@delete');
	Route::get('create', 'UserController@create');
	Route::post('store', 'UserController@store');
	Route::put('update/{id}', 'UserController@update');
	Route::post('acceptance/{id}', 'ApplicationController@acceptance');
	Route::get('{id}', 'UserController@detail');
	Route::get('create_student/{id}', 'UserController@create_student');
	Route::get('edit_student/{id}', 'UserController@edit_student');
	Route::post('store_student', 'UserController@store_student');
	Route::put('update_student/{id}', 'UserController@update_student');
	Route::get('delete_salary/{id}', 'UserController@delete_salary');
});
Route::group(['prefix' => 'institusi', 'middleware' => ['auth','roles'], 'roles' => ['Admin Cluever']], function()
{
	Route::get('/', 'InstitutionController@index');
	Route::post('delete/{id}', 'InstitutionController@delete');
	Route::get('create', 'InstitutionController@create');
	Route::get('edit/{id}', 'InstitutionController@edit');
	Route::post('store', 'InstitutionController@store');
	Route::put('update/{id}', 'InstitutionController@update');
	Route::get('{id}', 'InstitutionController@detail');
});
Route::group(['prefix' => 'tahun_ajaran', 'middleware' => ['auth','roles'], 'roles' => ['Admin Cluever','Admin Institusi']], function()
{
	Route::get('/', 'SchoolYearController@index');
	Route::get('institusi', 'SchoolYearController@institusi');
	Route::post('delete/{id}', 'SchoolYearController@delete');
	Route::get('create/{id}', 'SchoolYearController@create');
	Route::get('edit/{id}', 'SchoolYearController@edit');
	Route::post('store', 'SchoolYearController@store');
	Route::put('update/{id}', 'SchoolYearController@update');
	Route::get('{id}', 'SchoolYearController@detail');
	Route::get('detail_institusi/{id}', 'SchoolYearController@detail_institusi');
});
Route::group(['prefix' => 'jenis_jadwal', 'middleware' => ['auth','roles'], 'roles' => ['Admin Cluever','Admin Institusi']], function()
{
	Route::get('/', 'ScheduleTypeController@index');
	Route::get('institusi', 'ScheduleTypeController@institusi');
	Route::get('detail_institusi/{id}', 'ScheduleTypeController@detail_institusi');
	Route::post('delete/{id}', 'ScheduleTypeController@delete');
	Route::get('create/{id}', 'ScheduleTypeController@create');
	Route::get('edit/{id}', 'ScheduleTypeController@edit');
	Route::post('store', 'ScheduleTypeController@store');
	Route::put('update/{id}', 'ScheduleTypeController@update');
	Route::get('{id}', 'ScheduleTypeController@detail');
});
Route::group(['prefix' => 'matpel', 'middleware' => ['auth','roles'], 'roles' => ['Admin Cluever','Admin Institusi']], function()
{
	Route::get('/', 'SubjectController@index');
	Route::get('institusi', 'SubjectController@institusi');
	Route::get('detail_institusi/{id}', 'SubjectController@detail_institusi');
	Route::post('delete/{id}', 'SubjectController@delete');
	Route::get('create/{id}', 'SubjectController@create');
	Route::get('edit/{id}', 'SubjectController@edit');
	Route::post('store', 'SubjectController@store');
	Route::put('update/{id}', 'SubjectController@update');
	Route::get('{id}', 'SubjectController@detail');
});
Route::group(['prefix' => 'kelas', 'middleware' => ['auth','roles'], 'roles' => ['Admin Cluever','Admin Institusi']], function()
{
	Route::get('/', 'ClassController@index');
	Route::get('institusi', 'ClassController@institusi');
	Route::get('detail_institusi/{id}', 'ClassController@detail_institusi');
	Route::post('delete/{id}', 'ClassController@delete');
	Route::get('create/{id}', 'ClassController@create');
	Route::get('edit/{id}', 'ClassController@edit');
	Route::get('add_siswakelas/{id}/{classid}', 'ClassController@add_siswakelas');
	Route::post('delete_siswakelas/{id}', 'ClassController@delete_siswakelas');
	Route::post('store', 'ClassController@store');
	Route::put('update/{id}', 'ClassController@update');
	Route::get('{id}', 'ClassController@detail');
});
Route::group(['prefix' => 'penjadwalan', 'middleware' => ['auth','roles'], 'roles' => ['Admin Cluever','Admin Institusi']], function()
{
	Route::get('/', 'ScheduleController@index');
	Route::get('institusi', 'ScheduleController@institusi');
	Route::get('detail_institusi/{id}', 'ScheduleController@detail_institusi');
	Route::post('delete/{id}', 'ScheduleController@delete');
	Route::get('create/{id}', 'ScheduleController@create');
	Route::get('edit/{id}', 'ScheduleController@edit');
	Route::post('store', 'ScheduleController@store');
	Route::put('update/{id}', 'ScheduleController@update');
	Route::get('{id}', 'ScheduleController@detail');
});
Route::group(['prefix' => 'program', 'middleware' => ['auth','roles'], 'roles' => ['Admin Cluever','Admin Institusi']], function()
{
	Route::get('/', 'ProgramController@index');
	Route::post('delete/{id}', 'ProgramController@delete');
	Route::get('create', 'ProgramController@create');
	Route::get('edit/{id}', 'ProgramController@edit');
	Route::post('store', 'ProgramController@store');
	Route::put('update/{id}', 'ProgramController@update');
	Route::get('{id}', 'ProgramController@detail');
	Route::get('create_program/{id}', 'ProgramController@create_program');
});
Route::group(['prefix' => 'flexi', 'middleware' => ['auth','roles'], 'roles' => ['Siswa']], function()
{
    Route::get('/', 'FlexiController@index');
    Route::get('/schedule', 'FlexiController@schedule_list');
    Route::get('/rapor', 'FlexiController@getAllRapor');
    Route::post('{id}/{student_id}', 'FlexiController@store');
    Route::post('unbook/{scheduleId}/{userId}', 'FlexiController@unbook');
//    Route::get('edit/{id}', 'PaymentController@edit');
//    Route::put('update/{id}', 'PaymentController@update');
//    Route::post('store', 'PaymentController@store');
//    Route::get('{id}', 'PaymentController@show');
});
Route::get('/json-regencies', 'HomeController@regencies');
Route::get('/json-districts', 'HomeController@districts');
Route::get('/json-postcodes', 'HomeController@postcodes');
