<?php

Route::group([
    'namespace' => 'API\Auth',
], function ($router) {
    $router->post('authenticate', 'LoginController@login');
    $router->post('register', 'RegisterController@create');
    $router->post('check_token', '\App\Http\Controllers\API\UserController@checkToken');
});

Route::group([
	'namespace' => 'API',
	'prefix' => 'api/v1a',
	'middleware' => ['api', 'jwt.auth'],
], function ($router) {
	$router->post('helper/get_all_pending_payment', 'PaymentController@pending');
	$router->post('payment/pending_payments', 'PaymentController@pending');
	$router->post('payment/charge', 'PaymentController@payToMidtrans');
	$router->get('institution/search', 'InstitutionController@search');
	$router->post('institution/get_institution_detail', 'InstitutionController@show');
	$router->post('registration_proposal', 'InstitutionController@createProposal');
	$router->post('installment/check_for_update', 'PaymentController@checkInstallmentUpdate');
	$router->post('installment/initialize_list', 'PaymentController@installmentList');
	$router->post('tx_history/list_tx_histories', 'PaymentController@transactionHistories');
	$router->get('user_detail/{id}', 'UserController@show');
	$router->post('user_configuration/get_user_configurations', 'UserController@getUserConfig');
	$router->post('user_configuration/update_user_configurations', 'UserController@updateUserConfig');
	$router->post('helper/profile', 'UserController@profile');
	$router->post('helper/provinces', 'RegionController@getProvinces');
	$router->post('helper/create_secondary', 'UserController@createSecondaryUser');
	$router->post('application', 'PaymentController@createApplication');
});
