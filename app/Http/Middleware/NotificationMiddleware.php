<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Notification;
use Auth;

class NotificationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user()->id;
        $notifs = Notification::where('user_id',$user)->where('is_read',0)->get();
        $request->merge(compact('notifs'));
        return $next($request);
    }
}
