<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Presence;
use App\Models\PresenceStatus;
use App\Models\Schedule;
use App\Models\StudentFlexi;
use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FlexiController extends Controller
{
    public function index()
    {
        $dataBeforeDate= [];
        $dataTanggal = [];
        $dataAfterDate = [];
        try {
            $period = new DatePeriod(
                new DateTime(date('Y-m-d', strtotime('monday this week'))), // Monday-nya kebawa
                new DateInterval('P1D'),
                new DateTime(date('Y-m-d', strtotime('sunday this week'))) // Sunday-nya ga kebawa, jadi sampai sabtu
            );
            $after = new DatePeriod(
                new DateTime(date('Y-m-d', strtotime('today'))),
                new DateInterval('P1D'),
                new DateTime(date('Y-m-d', strtotime('+2 days')))
            );
            $before = new DatePeriod(
                new DateTime(date('Y-m-d', strtotime('sunday last week'))),
                new DateInterval('P1D'),
                new DateTime(date('Y-m-d', strtotime('today')))
            );
        } catch (\Exception $e) {
            report($e);
        }
        foreach ($period as $date) {
            $dataTanggal[] = $date->format('Y-m-d');
        }
        foreach ($after as $date) {
            $dataAfterDate[] = $date->format('Y-m-d');
        }
        foreach ($before as $date) {
            $dataBeforeDate[] = $date->format('Y-m-d');
        }

//        dd($dataBeforeDate);

        foreach (Auth::user()->clasess as $class) {
            $data = $class->programs->where('name', 'ILIKE', '%Flexi%')->count();
            if ($data > 0) {
                $flexi = Schedule::select('schedules.id','schedules.subject_id','schedules.note','schedules.educator_id','schedules.class_id',
                    'schedules.start_hour','schedules.finish_hour','schedules.day',
                    DB::raw("(SELECT schedules.capacity - (SELECT COUNT(student_flexi.schedule_id) FROM student_flexi WHERE student_flexi.schedule_id = schedules.id)) AS capacity"))
                    ->join('classes as a', 'schedules.class_id', '=', 'a.id')
                    ->join('programs as b', 'a.program_id', '=', 'b.id')
                    ->join('class_students as c', 'a.id', '=', 'c.class_id')
                    ->where('c.student_id', '=', Auth::user()->id)
                    ->where('b.name', 'ILIKE', '%Flexi%')
                    ->get();

//                dd($flexi);
                $dataFlexi = StudentFlexi::where('student_id', Auth::user()->id)->get(['schedule_id']);
                $dataFlexiArr = [];
                foreach ($dataFlexi as $data) {
                    $dataFlexiArr[] = $data->schedule_id;
                }
            }
        }
        if (!isset($flexi)) {
            $flexi = [];
        }

        return view('flexi.list', compact('flexi', 'dataTanggal', 'dataFlexiArr', 'dataAfterDate', 'dataBeforeDate'));
    }

    public function store($id, $student_id)
    {
        $getBook = StudentFlexi::where('student_id', Auth::user()->id)
            ->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();

        if ($getBook >= 8) {
            return redirect()->back()->with('danger', 'Jumlah Booking Tidak Bisa Melebihi 8 Sesi');
        } else {
            StudentFlexi::create([
                'schedule_id' => $id,
                'student_id' => $student_id
            ]);
            return redirect()->back()->with('success', 'Booking Berhasil');
        }
    }

    public function schedule_list()
    {
        $schedule = StudentFlexi::where('student_id', Auth::user()->id)->get();
        try {
            $period = new DatePeriod(
                new DateTime(date('Y-m-d', strtotime('monday this week'))), // Monday-nya kebawa
                new DateInterval('P1D'),
                new DateTime(date('Y-m-d', strtotime('sunday this week'))) // Sunday-nya ga kebawa, jadi sampai sabtu
            );
        } catch (\Exception $e) {
            report($e);
        }
        foreach ($period as $date) {
            $dataTanggal[] = $date->format('Y-m-d');
        }

        return view('flexi.schedule_list', compact('schedule', 'dataTanggal'));
    }

    public function unbook($scheduleId, $studentId)
    {
        StudentFlexi::where('schedule_id', $scheduleId)
            ->where('student_id', $studentId)
            ->delete();
        return redirect()->back()->with('success', 'Pembatalan Booking Berhasil');
    }

    public function getAllRapor(Request $request)
    {
        $countAll = StudentFlexi::join('schedules', 'student_flexi.schedule_id', '=', 'schedules.id')
            ->leftjoin('presences', 'presences.schedule_id', '=', 'schedules.id')
            ->where('student_flexi.student_id', '=', Auth::user()->id)
            ->count();
        $countHadir = Presence::join('presence_statuses as p','p.id','=','presences.status_id')
            ->where('p.name','ILIKE','%Hadir%')
            ->where('presences.student_id','=', Auth::user()->id)->count();
        $countIzin = Presence::join('presence_statuses as p','p.id','=','presences.status_id')
            ->where('p.name','ILIKE','%Izin%')
            ->where('presences.student_id','=', Auth::user()->id)->count();
        $countSakit = Presence::join('presence_statuses as p','p.id','=','presences.status_id')
            ->where('p.name','ILIKE','%Sakit%')
            ->where('presences.student_id','=', Auth::user()->id)->count();
        $countAlpa = Presence::join('presence_statuses as p','p.id','=','presences.status_id')
            ->where('p.name','ILIKE','%Alpa%')
            ->where('presences.student_id','=', Auth::user()->id)->count();
        $countBelum = StudentFlexi::join('schedules', 'student_flexi.schedule_id', '=', 'schedules.id')
            ->leftjoin('presences', 'presences.schedule_id', '=', 'schedules.id')
            ->where('student_flexi.student_id','=', Auth::user()->id)
            ->whereNull('presences.status_id')
            ->count();
//        echo json_encode($countBelum); exit;
//        $data = Presence::where('student_id', Auth::user()->id)->orderBy('created_at','desc')->get();
        $data = StudentFlexi::select('student_flexi.id', 'subjects.name', 'schedules.note', 'user_details.fullname',
            'classes.class_code', 'schedules.day', 'schedules.start_hour', 'schedules.finish_hour', 'presence_statuses.name as status')
            ->join('schedules', 'student_flexi.schedule_id', '=', 'schedules.id')
            ->join('subjects', 'schedules.subject_id', '=', 'subjects.id')
            ->join('classes', 'schedules.class_id', '=', 'classes.id')
            ->join('users', 'schedules.educator_id', '=', 'users.id')
            ->join('user_details', 'user_details.user_id', '=', 'users.id')
            ->leftjoin('presences', 'presences.schedule_id', '=', 'schedules.id')
            ->leftjoin('presence_statuses', 'presences.status_id', '=', 'presence_statuses.id')
            ->where('student_flexi.student_id', '=', Auth::user()->id)
            ->orderBy('schedules.day', 'asc')
            ->get();

        $filter = $request->input('filter');
        $start = $request->input('start');
        $end = $request->input('end');
        if ($filter == 'all') {
            if (isset($start, $end)) {
                $data = StudentFlexi::select('student_flexi.id', 'subjects.name', 'schedules.note', 'user_details.fullname',
                    'classes.class_code', 'schedules.day', 'schedules.start_hour', 'schedules.finish_hour', 'presence_statuses.name as status')
                    ->join('schedules', 'student_flexi.schedule_id', '=', 'schedules.id')
                    ->join('subjects', 'schedules.subject_id', '=', 'subjects.id')
                    ->join('classes', 'schedules.class_id', '=', 'classes.id')
                    ->join('users', 'schedules.educator_id', '=', 'users.id')
                    ->join('user_details', 'user_details.user_id', '=', 'users.id')
                    ->leftjoin('presences', 'presences.schedule_id', '=', 'schedules.id')
                    ->leftjoin('presence_statuses', 'presences.status_id', '=', 'presence_statuses.id')
                    ->where('student_flexi.student_id', '=', Auth::user()->id)
                    ->whereBetween('schedules.day', [new Carbon($start), new Carbon($end)])
                    ->orderBy('schedules.day', 'asc')
                    ->get();
            } else {
                $data = StudentFlexi::select('student_flexi.id', 'subjects.name', 'schedules.note', 'user_details.fullname',
                    'classes.class_code', 'schedules.day', 'schedules.start_hour', 'schedules.finish_hour', 'presence_statuses.name as status')
                    ->join('schedules', 'student_flexi.schedule_id', '=', 'schedules.id')
                    ->join('subjects', 'schedules.subject_id', '=', 'subjects.id')
                    ->join('classes', 'schedules.class_id', '=', 'classes.id')
                    ->join('users', 'schedules.educator_id', '=', 'users.id')
                    ->join('user_details', 'user_details.user_id', '=', 'users.id')
                    ->leftjoin('presences', 'presences.schedule_id', '=', 'schedules.id')
                    ->leftjoin('presence_statuses', 'presences.status_id', '=', 'presence_statuses.id')
                    ->where('student_flexi.student_id', '=', Auth::user()->id)
                    ->orderBy('schedules.day', 'asc')
                    ->get();
            }
        }
        if ($filter == 'hadir') {
            if (isset($start, $end)) {
                $data = StudentFlexi::select('student_flexi.id', 'subjects.name', 'schedules.note', 'user_details.fullname',
                    'classes.class_code', 'schedules.day', 'schedules.start_hour', 'schedules.finish_hour', 'presence_statuses.name as status')
                    ->join('schedules', 'student_flexi.schedule_id', '=', 'schedules.id')
                    ->join('subjects', 'schedules.subject_id', '=', 'subjects.id')
                    ->join('classes', 'schedules.class_id', '=', 'classes.id')
                    ->join('users', 'schedules.educator_id', '=', 'users.id')
                    ->join('user_details', 'user_details.user_id', '=', 'users.id')
                    ->join('presences', 'presences.schedule_id', '=', 'schedules.id')
                    ->join('presence_statuses', 'presences.status_id', '=', 'presence_statuses.id')
                    ->where('student_flexi.student_id', '=', Auth::user()->id)
                    ->where('presence_statuses.name', '=', 'Hadir')
                    ->whereBetween('schedules.day', [new Carbon($start), new Carbon($end)])
                    ->orderBy('schedules.day', 'asc')
                    ->get();
            } else {
                $data = StudentFlexi::select('student_flexi.id', 'subjects.name', 'schedules.note', 'user_details.fullname',
                    'classes.class_code', 'schedules.day', 'schedules.start_hour', 'schedules.finish_hour', 'presence_statuses.name as status')
                    ->join('schedules', 'student_flexi.schedule_id', '=', 'schedules.id')
                    ->join('subjects', 'schedules.subject_id', '=', 'subjects.id')
                    ->join('classes', 'schedules.class_id', '=', 'classes.id')
                    ->join('users', 'schedules.educator_id', '=', 'users.id')
                    ->join('user_details', 'user_details.user_id', '=', 'users.id')
                    ->join('presences', 'presences.schedule_id', '=', 'schedules.id')
                    ->join('presence_statuses', 'presences.status_id', '=', 'presence_statuses.id')
                    ->where('student_flexi.student_id', '=', Auth::user()->id)
                    ->where('presence_statuses.name', '=', 'Hadir')
                    ->orderBy('schedules.day', 'asc')
                    ->get();
            }
        }
        if ($filter == 'sakit') {
            if (isset($start, $end)) {
                $data = StudentFlexi::select('student_flexi.id', 'subjects.name', 'schedules.note', 'user_details.fullname',
                    'classes.class_code', 'schedules.day', 'schedules.start_hour', 'schedules.finish_hour', 'presence_statuses.name as status')
                    ->join('schedules', 'student_flexi.schedule_id', '=', 'schedules.id')
                    ->join('subjects', 'schedules.subject_id', '=', 'subjects.id')
                    ->join('classes', 'schedules.class_id', '=', 'classes.id')
                    ->join('users', 'schedules.educator_id', '=', 'users.id')
                    ->join('user_details', 'user_details.user_id', '=', 'users.id')
                    ->join('presences', 'presences.schedule_id', '=', 'schedules.id')
                    ->join('presence_statuses', 'presences.status_id', '=', 'presence_statuses.id')
                    ->where('student_flexi.student_id', '=', Auth::user()->id)
                    ->where('presence_statuses.name', '=', 'Sakit')
                    ->whereBetween('schedules.day', [new Carbon($start), new Carbon($end)])
                    ->orderBy('schedules.day', 'asc')
                    ->get();
            } else {
                $data = StudentFlexi::select('student_flexi.id', 'subjects.name', 'schedules.note', 'user_details.fullname',
                    'classes.class_code', 'schedules.day', 'schedules.start_hour', 'schedules.finish_hour', 'presence_statuses.name as status')
                    ->join('schedules', 'student_flexi.schedule_id', '=', 'schedules.id')
                    ->join('subjects', 'schedules.subject_id', '=', 'subjects.id')
                    ->join('classes', 'schedules.class_id', '=', 'classes.id')
                    ->join('users', 'schedules.educator_id', '=', 'users.id')
                    ->join('user_details', 'user_details.user_id', '=', 'users.id')
                    ->join('presences', 'presences.schedule_id', '=', 'schedules.id')
                    ->join('presence_statuses', 'presences.status_id', '=', 'presence_statuses.id')
                    ->where('student_flexi.student_id', '=', Auth::user()->id)
                    ->where('presence_statuses.name', '=', 'Sakit')
                    ->orderBy('schedules.day', 'asc')
                    ->get();
            }
        }
        if ($filter == 'izin') {
            if (isset($start, $end)) {
                $data = StudentFlexi::select('student_flexi.id', 'subjects.name', 'schedules.note', 'user_details.fullname',
                    'classes.class_code', 'schedules.day', 'schedules.start_hour', 'schedules.finish_hour', 'presence_statuses.name as status')
                    ->join('schedules', 'student_flexi.schedule_id', '=', 'schedules.id')
                    ->join('subjects', 'schedules.subject_id', '=', 'subjects.id')
                    ->join('classes', 'schedules.class_id', '=', 'classes.id')
                    ->join('users', 'schedules.educator_id', '=', 'users.id')
                    ->join('user_details', 'user_details.user_id', '=', 'users.id')
                    ->join('presences', 'presences.schedule_id', '=', 'schedules.id')
                    ->join('presence_statuses', 'presences.status_id', '=', 'presence_statuses.id')
                    ->where('student_flexi.student_id', '=', Auth::user()->id)
                    ->where('presence_statuses.name', '=', 'Izin')
                    ->whereBetween('schedules.day', [new Carbon($start), new Carbon($end)])
                    ->orderBy('schedules.day', 'asc')
                    ->get();
            } else {
                $data = StudentFlexi::select('student_flexi.id', 'subjects.name', 'schedules.note', 'user_details.fullname',
                    'classes.class_code', 'schedules.day', 'schedules.start_hour', 'schedules.finish_hour', 'presence_statuses.name as status')
                    ->join('schedules', 'student_flexi.schedule_id', '=', 'schedules.id')
                    ->join('subjects', 'schedules.subject_id', '=', 'subjects.id')
                    ->join('classes', 'schedules.class_id', '=', 'classes.id')
                    ->join('users', 'schedules.educator_id', '=', 'users.id')
                    ->join('user_details', 'user_details.user_id', '=', 'users.id')
                    ->join('presences', 'presences.schedule_id', '=', 'schedules.id')
                    ->join('presence_statuses', 'presences.status_id', '=', 'presence_statuses.id')
                    ->where('student_flexi.student_id', '=', Auth::user()->id)
                    ->where('presence_statuses.name', '=', 'Izin')
                    ->orderBy('schedules.day', 'asc')
                    ->get();
            }
        }
        if ($filter == 'alpa') {
            if (isset($start, $end)) {
                $data = StudentFlexi::select('student_flexi.id', 'subjects.name', 'schedules.note', 'user_details.fullname',
                    'classes.class_code', 'schedules.day', 'schedules.start_hour', 'schedules.finish_hour', 'presence_statuses.name as status')
                    ->join('schedules', 'student_flexi.schedule_id', '=', 'schedules.id')
                    ->join('subjects', 'schedules.subject_id', '=', 'subjects.id')
                    ->join('classes', 'schedules.class_id', '=', 'classes.id')
                    ->join('users', 'schedules.educator_id', '=', 'users.id')
                    ->join('user_details', 'user_details.user_id', '=', 'users.id')
                    ->join('presences', 'presences.schedule_id', '=', 'schedules.id')
                    ->join('presence_statuses', 'presences.status_id', '=', 'presence_statuses.id')
                    ->where('student_flexi.student_id', '=', Auth::user()->id)
                    ->where('presence_statuses.name', '=', 'Alpa')
                    ->whereBetween('schedules.day', [new Carbon($start), new Carbon($end)])
                    ->orderBy('schedules.day', 'asc')
                    ->get();
            } else {
                $data = StudentFlexi::select('student_flexi.id', 'subjects.name', 'schedules.note', 'user_details.fullname',
                    'classes.class_code', 'schedules.day', 'schedules.start_hour', 'schedules.finish_hour', 'presence_statuses.name as status')
                    ->join('schedules', 'student_flexi.schedule_id', '=', 'schedules.id')
                    ->join('subjects', 'schedules.subject_id', '=', 'subjects.id')
                    ->join('classes', 'schedules.class_id', '=', 'classes.id')
                    ->join('users', 'schedules.educator_id', '=', 'users.id')
                    ->join('user_details', 'user_details.user_id', '=', 'users.id')
                    ->join('presences', 'presences.schedule_id', '=', 'schedules.id')
                    ->join('presence_statuses', 'presences.status_id', '=', 'presence_statuses.id')
                    ->where('student_flexi.student_id', '=', Auth::user()->id)
                    ->where('presence_statuses.name', '=', 'Alpa')
                    ->orderBy('schedules.day', 'asc')
                    ->get();
            }
        }
        if ($filter == 'belum_hadir') {
            if (isset($start, $end)) {
                $data = StudentFlexi::select('student_flexi.id', 'subjects.name', 'schedules.note', 'user_details.fullname',
                    'classes.class_code', 'schedules.day', 'schedules.start_hour', 'schedules.finish_hour', 'presence_statuses.name as status')
                    ->join('schedules', 'student_flexi.schedule_id', '=', 'schedules.id')
                    ->join('subjects', 'schedules.subject_id', '=', 'subjects.id')
                    ->join('classes', 'schedules.class_id', '=', 'classes.id')
                    ->join('users', 'schedules.educator_id', '=', 'users.id')
                    ->join('user_details', 'user_details.user_id', '=', 'users.id')
                    ->leftjoin('presences', 'presences.schedule_id', '=', 'schedules.id')
                    ->leftjoin('presence_statuses', 'presences.status_id', '=', 'presence_statuses.id')
                    ->where('student_flexi.student_id', '=', Auth::user()->id)
                    ->whereNull('presences.status_id')
                    ->whereBetween('schedules.day', [new Carbon($start), new Carbon($end)])
                    ->orderBy('schedules.day', 'asc')
                    ->get();
            } else {
                $data = StudentFlexi::select('student_flexi.id', 'subjects.name', 'schedules.note', 'user_details.fullname',
                    'classes.class_code', 'schedules.day', 'schedules.start_hour', 'schedules.finish_hour', 'presence_statuses.name as status')
                    ->join('schedules', 'student_flexi.schedule_id', '=', 'schedules.id')
                    ->join('subjects', 'schedules.subject_id', '=', 'subjects.id')
                    ->join('classes', 'schedules.class_id', '=', 'classes.id')
                    ->join('users', 'schedules.educator_id', '=', 'users.id')
                    ->join('user_details', 'user_details.user_id', '=', 'users.id')
                    ->leftjoin('presences', 'presences.schedule_id', '=', 'schedules.id')
                    ->leftjoin('presence_statuses', 'presences.status_id', '=', 'presence_statuses.id')
                    ->where('student_flexi.student_id', '=', Auth::user()->id)
                    ->whereNull('presences.status_id')
                    ->orderBy('schedules.day', 'asc')
                    ->get();
            }
        }
//        dd($filter);
//        echo json_encode($data); exit;

        return view('flexi.rapor', compact('data', 'countAll', 'countHadir', 'countIzin', 'countSakit', 'countAlpa', 'countBelum'));
    }
}