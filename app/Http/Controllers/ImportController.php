<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Institution;
use App\Imports\ImportUsers;
use App\Imports\ImportPresences;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;



class ImportController extends Controller
{
	public function index()
	{
		$institutions = Institution::all();
		return view('master_data.import.form', compact('institutions'));
	}
	public function user()
	{
		$institutions = Institution::all();
		return view('master_data.import.form', compact('institutions'));
	}
	public function presensi()
	{
		$institutions = Institution::all();
		return view('master_data.import.form_presensi', compact('institutions'));
	}

	public function importuser(Request $request)
	{
		$this->validate($request, [
			'file' => 'required|mimes:xls,xlsx',
			'institution_id' => 'required'
		]);

		if ($request->hasFile('file')) 
		{
			$file = $request->file('file'); 
			$idinstitution = $request->institution_id;
			Excel::import(new ImportUsers($request), $file);
			return redirect()->back()->with(['success' => 'Upload success']);
		}  
		return redirect()->back()->with(['error' => 'Please choose file before']);
	}
	public function importpresensi(Request $request)
	{
		$this->validate($request, [
			'file' => 'required|mimes:xls,xlsx',
			'institution_id' => 'required'
		]);

		if ($request->hasFile('file')) 
		{
			$file = $request->file('file'); 
			$idinstitution = $request->institution_id;
			Excel::import(new ImportPresences($request), $file);
			return redirect()->back()->with(['success' => 'Upload success']);
		}  
		return redirect()->back()->with(['error' => 'Please choose file before']);
	}

}
