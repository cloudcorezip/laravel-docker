<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subject;
use App\Models\Institution;
use Auth;

class SubjectController extends Controller
{
    public function index()
    {
    	$id = Auth::user()->institution_user->institution_id;
        $matpel = Subject::where('institution_id', '=', $id)->get();
        return view('master_data.matpel.list', compact('matpel'));
    }
    public function institusi()
    {
        $institutions = Institution::all();
        return view('master_data.matpel.list_institusi', compact('institutions'));
    }
    public function detail_institusi($id)
    {
        $Institusi = Institution::where('id','=',$id)->first();
        $matpel = Subject::where('institution_id', '=', $id)->get();
        return view('master_data.matpel.list', compact('matpel','Institusi'));
    }
    public function create($id)
    {
        $matpel = new Subject();
        $institutions = Institution::all();
        return view('master_data.matpel.form', compact('matpel','institutions','id'));
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
        ]);
        $data = new Subject();
        $data->institution_id = $request->institution_id;
        $data->name = $request->name;
        $data->description = $request->description;
        $data->save();
        if(Auth::user()->hasRole('Admin Cluever')){
            return redirect('/matpel/detail_institusi/'.$request->institution_id)->with('success', 'Mata Pelajaran baru berhasil dibuat');
        }
        else{
            return redirect('/matpel')->with('success', 'Mata Pelajaran baru berhasil dibuat');
        }
    }
    public function edit($id)
    {
    	$matpel = Subject::find($id);
        $institutions = Institution::all();
        return view('master_data.matpel.form', compact('matpel','institutions'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
        ]);
        $data = Subject::find($id);
        $data->name = $request->name;
        $data->description = $request->description;
        $data->save();
        if(Auth::user()->hasRole('Admin Cluever')){
            return redirect('/matpel/detail_institusi/'.$data->institution_id)->with('success', 'Mata Pelajaran baru berhasil diupdate');
        }
        else{
            return redirect('/matpel')->with('success', 'Mata Pelajaran baru berhasil diupdate');
        }
    }
    public function detail($id)
    {
    	
    }
    public function delete(Request $request, $id)
    {
    	$data = Subject::findOrFail($id);
    	$data->delete();
        if(Auth::user()->hasRole('Admin Cluever')){
            return redirect('/matpel/detail_institusi/'.$data->institution_id)->with('success', 'Mata Pelajaran baru berhasil dihapus');
        }
        else{
            return redirect('/matpel')->with('success', 'Mata Pelajaran baru berhasil dihapus');
        }
    }
}
