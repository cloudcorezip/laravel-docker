<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Program;
use Auth;

class ProgramController extends Controller
{
    public function index()
    {
    	$id = Auth::user()->institution_user->institution_id;
        $program = Program::where('institution_id', '=', $id)->get();
        return view('master_data.program.list', compact('program'));

    }
    public function create()
    {
        $program = new Program();
        return view('master_data.program.form', compact('program'));
    }
    public function create_program($id)
    {
        $program = new Program();
        return view('master_data.program.form_cl', compact('program','id'));
    }
    public function store(Request $request)
    {
    	$this->validate($request,[
            'name' => 'required',
            'price' => 'required',
            'discount' => 'required',
        ]);
        
        $data = new Program();
        $data->institution_id = $request->institution_id;
        $data->name = $request->name;
        $data->description = $request->description;
        $data->price = $request->price;
        $data->discount = $request->discount;
        $data->save();
        if(Auth::user()->hasRole('Admin Institusi')){
            return redirect('/program')->with('success', 'Program baru berhasil dibuat');
        }
        else{
            return redirect()->action('InstitutionController@detail',['id' => $request->institution_id]);
        }
    }
    public function edit($id)
    {
    	$program = Program::find($id);
        return view('master_data.program.form', compact('program'));
    }
    public function update(Request $request, $id)
    {
    	$this->validate($request,[
            'name' => 'required',
            'price' => 'required',
            'discount' => 'required',
        ]);
        $data = Program::find($request->id);
        $data->name = $request->name;
        $data->description = $request->description;
        $data->price = $request->price;
        $data->discount = $request->discount;
        $data->save();
        if(Auth::user()->hasRole('Admin Institusi')){
            return redirect('/program')->with('success', 'Program berhasil diupdate');
        }
        else{
            return redirect()->action('InstitutionController@detail',['id' => $data->institution_id]);
        }
    }
    public function detail($id)
    {
    	
    }
    public function delete(Request $request, $id)
    {
    	$data = Program::findOrFail($id);
    	$data->delete();
    	return back()->with('success', 'Program berhasil dihapus');
    }
}
