<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Program;
use App\Models\Schedule;
use App\Models\Subject;
use App\Models\InstitutionUser;
use Auth;
use App\Models\Classes;
use App\Models\ScheduleType;
use App\Models\Institution;

class ScheduleController extends Controller
{
    public function index()
    {
        $id = Auth::user()->institution_user->institution_id;
    	$schedules = Schedule::where('institution_id','=',$id)->get();
    	return view('master_data.penjadwalan.list', compact('schedules'));
    }
    public function institusi()
    {
        $institutions = Institution::all();
        return view('master_data.penjadwalan.list_institusi', compact('institutions'));
    }
    public function detail_institusi($id)
    {
        $Institusi = Institution::where('id','=',$id)->first();
        $schedules = Schedule::where('institution_id', '=', $id)->get();
        return view('master_data.penjadwalan.list', compact('schedules','Institusi'));
    }
    public function create($id)
    {
        $schedule = new Schedule();
        $subjects = Subject::where('institution_id','=',$id)->get();
        $classes = Classes::where('institution_id','=',$id)->get();
        $scheduletype = ScheduleType::where('institution_id','=',$id)->get();
        $user_roles = InstitutionUser::distinct()->where('role_id', '=', 4)->where('institution_id', '=',$id)->get(['role_id','user_id']);
        return view('master_data.penjadwalan.form', compact('schedule','scheduletype','subjects','user_roles','classes','id'));
    }

    public function store(Request $request)
    {
        // echo json_encode($request->input());exit;
        $this->validate($request,[
            'start_hour' => 'required',
            'finish_hour' => 'required',
            'day' => 'required',
            'capacity' => 'required',
            'educator_id' => 'required',
            'schedule_type_id' => 'required',
        ]);
        
        $data = new Schedule([
        	'start_hour' =>  $request->start_hour,
        	'finish_hour' => $request->finish_hour,
        	'day' => $request->day,
        	'note' => $request->note,
        	'capacity' => $request->capacity,
        	'room' => $request->room,
        	'subject_id' => empty($request->subject_id) ? null : $request->subject_id,
        	'educator_id' => $request->educator_id,
        	'class_id' => empty($request->class_id) ? null : $request->class_id,
            'schedule_type_id' => $request->schedule_type_id,
            'institution_id' => $request->institution_id,
        ]);
        $data->save();
        if(Auth::user()->hasRole('Admin Cluever')){
            return redirect('/penjadwalan/detail_institusi/'.$request->institution_id)->with('success', 'Penjadwalan baru berhasil dibuat');
        }
        else{
            return redirect('/penjadwalan')->with('success', 'Penjadwalan baru berhasil dibuat');
        }
    }

    public function edit($id)
    {
        $schedule = Schedule::find($id);
        $institutionid = $schedule->institution_id;
        $classes = Classes::where('institution_id','=',$institutionid)->get();
        $subjects = Subject::where('institution_id','=',$institutionid)->get();
        $scheduletype = ScheduleType::where('institution_id','=',$institutionid)->get();
        $user_roles = InstitutionUser::distinct()->where('role_id', '=', 4)->where('institution_id', '=',$institutionid)->get(['role_id','user_id']);
        
        return view('master_data.penjadwalan.form', compact('schedule','subjects','scheduletype','user_roles','classes'));
    }

    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'start_hour' => 'required',
            'finish_hour' => 'required',
            'day' => 'required',
            'capacity' => 'required',
            'educator_id' => 'required',
            'schedule_type_id' => 'required',
        ]);
        $schedule = schedule::findOrFail($id);
        $schedule->update([
            'start_hour' =>  $request->start_hour,
            'finish_hour' => $request->finish_hour,
            'day' => $request->day,
            'note' => $request->note,
            'capacity' => $request->capacity,
            'subject_id' => empty($request->subject_id) ? null : $request->subject_id,
            'educator_id' => $request->educator_id,
            'class_id' => empty($request->class_id) ? null : $request->class_id,
            'schedule_type_id' => $request->schedule_type_id,
        ]);
        if(Auth::user()->hasRole('Admin Cluever')){
            return redirect('/penjadwalan/detail_institusi/'.$request->institution_id)->with('success', 'Penjadwalan berhasil diupdate');
        }
        else{
            return redirect('/penjadwalan')->with('success', 'Penjadwalan berhasil diupdate');
        }
    }

    public function detail($id)
    {
        // $applications = Application::find($id);
        // return view('pengajuan.detail', compact('applications'));
    }

    public function acceptance(Request $request, $id)
    {
    	// $application = Application::findOrFail($id);
    	// $application->update([
    	// 	'application_status_id' => $request->status === 'terima' ? 2 : 3,
    	// 	'reason' => $request->reason
    	// ]);
    	// return back()->with('success', "Pengajuan berhasil di$request->status");
    }

    public function delete(Request $request, $id)
    {
    	// $application = Application::findOrFail($id);
    	// $application->delete();
    	// return back()->with('success', 'Pengajuan berhasil dihapus');
    }
}
