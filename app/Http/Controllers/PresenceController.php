<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Presence;
use App\Models\Schedule;
use App\Models\StudentClass;
use App\Models\StudentFlexi;
use App\Models\Classes;
use App\Models\Program;
use Auth;
use App\Imports\ImportUsers;
use Excel;

class PresenceController extends Controller
{
    public function index()
    {
        $id = Auth::user()->id;
    	$data = Presence::all();
        // $availableClass = Auth::user()->schedules;
        if (Auth::user()->hasRole('Educator')){
            $availableClass = Schedule::where('educator_id','=',$id)->get();
        }
        elseif(Auth::user()->hasRole('Admin Institusi')){
            $availableClass = Schedule::where('institution_id','=',Auth::user()->institution_user->institution_id)->get();
        }
        else{
            $availableClass = Schedule::all();
        }
    	return view('presence.list', compact('data', 'availableClass'));
    }
    public function create($id,$class_id)
    {
        $class = Classes::findOrFail($class_id);
        $std_flx = Program::where('id','=',$class->programs->id)->where('name', 'ILIKE','%Flexi%')->get();
        if($std_flx->count() > 0){
            $data = StudentFlexi::where('schedule_id', '=', $id)->get();
        }
        else{
            $data = StudentClass::where('class_id', '=', $class_id)->get();
        }
    	return view('presence.form', compact('data','class','presented','id'));
    }
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'details' => 'required',
        //     'amount' => 'required|numeric',
        //     'type' => 'required',
        //     'status' => 'required',
        //     'program' => 'required',
        // ]);
        // Payment::create([
        //     'payment_type_id' => $request->type,
        //     'payment_status_id' => $request->status,
        //     'program_id' => $request->program,
        //     'amount' => $request->amount,
        //     'details' => $request->details,
        // ]);
        // return redirect('payment')->with('success', 'Transaksi pembayaran berhasil ditambahkan');
    }
    public function show($id)
    {
        // $payment = Payment::findOrFail($id);
        // return view('payment.detail', compact('payment'));
    }
    public function edit($id)
    {
        // $paymentTypes = PaymentType::all();
        // $paymentStatuses = PaymentStatus::all();
        // $institutions = Auth::user()->institutions;
        // if (sizeof($institutions) > 0) {
        //     $programs = Program::where('institution_id', $institutions[0]->id)->get();
        // } else {
        //     $programs = [];
        // }
        // $payment = Payment::findOrFail($id);
        // return view('payment.form', compact('payment', 'paymentTypes', 'paymentStatuses', 'programs'));
    }
    public function update(Request $request, $id)
    {
        // $this->validate($request, [
        //     'details' => 'required',
        //     'amount' => 'required|numeric',
        //     'type' => 'required',
        //     'status' => 'required',
        //     'program' => 'required',
        // ]);
        // $payment = Payment::findOrFail($id);
        // $payment->update([
        //     'payment_type_id' => $request->type,
        //     'payment_status_id' => $request->status,
        //     'program_id' => $request->program,
        //     'amount' => $request->amount,
        //     'details' => $request->details,
        // ]);
        // return redirect('payment')->with('success', 'Transaksi pembayaran berhasil diubah');
    }
    public function delete($id)
    {
        // $payment = Payment::findOrFail($id);
        // $payment->delete();
        // return redirect('payment')->with('success', 'Transaksi pembayaran berhasil dihapus oleh ' . Auth::user()->user_detail->fullname);
    }
    public function importexcel(Request $request)
        {
            //VALIDASI
            $this->validate($request, [
                'file' => 'required|mimes:xls,xlsx'
            ]);

            if ($request->hasFile('file')) {
                $file = $request->file('file'); //GET FILE
                Excel::import(new ImportUsers, $file); //IMPORT FILE 
                return redirect()->back()->with(['success' => 'Upload success']);
            }  
            return redirect()->back()->with(['error' => 'Please choose file before']);
        }
    public function presence($statusId, $scheduleId, $studentId)
    {
        $presenceCount = Presence::where('schedule_id', $scheduleId)
            ->where('student_id', $studentId)
            ->count();
        if ($presenceCount > 0) {
            Presence::update([
                'schedule_id' => $scheduleId,
                'student_id' => $studentId,
                'status_id' => $statusId
            ]);
        } else {
            Presence::create([
                'schedule_id' => $scheduleId,
                'student_id' => $studentId,
                'status_id' => $statusId
            ]);
        }
        return back();
    }
    public function undoPresence($scheduleId, $studentId)
    {
        $presenceCount = Presence::where('schedule_id', $scheduleId)
            ->where('student_id', $studentId)
            ->delete();
        return back();
    }
}
