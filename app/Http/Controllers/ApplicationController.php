<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Program;
use App\Models\Application;
use App\Models\Institution;
use App\Models\ApplicationStatus;
use App\Models\Installment;
use App\Models\InstallmentDetail;
use App\Models\Salary;
use App\Models\Notification;
use App\Models\InstitutionUser;
use App\Mail\ApplicationEmail;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ApplicationController extends Controller
{
    public function index()
    {
        $notifs = Notification::where('user_id',Auth::user()->id)->where('is_read',0)->get();
    	$applications = Application::all();
    	return view('pengajuan.list', compact('applications','notifs'));
    }
    public function create()
    {
        $notifs = Notification::where('user_id',Auth::user()->id)->where('is_read',0)->get();
        $programs = Program::all();
        $institution = Institution::all();
        $users = User::all();
        $pengajuan = new Application;
        return view('pengajuan.form', compact('programs', 'institution', 'users', 'pengajuan','notifs'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'student_id' => 'required',
            'pic_id' => 'required',
            'institution_id' => 'required',
            'program_id' => 'required',
            'installment_period' => 'required',
        ]);
        
        $data = new Application([
        	'application_status_id' => 1,
        	'pic_id' => $request->pic_id,
        	'student_id' => $request->student_id,
        	'program_id' => $request->program_id,
        	'installment_period' => $request->installment_period,
        	'created_by_id' => 1
        ]);
        $data->save();
        return redirect('/pengajuan')->with('success', 'Pengajuan baru berhasil dibuat');
    }

    public function edit($id)
    {
        $notifs = Notification::where('user_id',Auth::user()->id)->where('is_read',0)->get();
        $applications = Application::find($id);
        $programs = Program::all();
        $statuses = ApplicationStatus::all();
        $institution = Institution::all();
        $users = User::all();
        return view('pengajuan.edit_pengajuan', compact('applications','programs','statuses','institution','users','notifs'));
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'student_id' => 'required',
            'pic_id' => 'required',
            'institution_id' => 'required',
            'program_id' => 'required',
            'installment_period' => 'required',
        ]);
        $data = Application::find($request->id);
        $data->student_id = $request->student_id;
        $data->pic_id = $request->pic_id;
        $data->application_status_id = 1;
        $data->program_id = $request->program_id;
        $data->installment_period = $request->installment_period;
        $data->save();
        return redirect('/pengajuan')->with('success', 'Pengajuan berhasil diupdate');
    }

    public function detail($id)
    {
        $notifs = Notification::where('user_id',Auth::user()->id)->where('is_read',0)->get();
        $applications = Application::find($id);
        $picid = $applications->pic_id;
        $salary = Salary::where('user_id', '=', $picid)->get();
        return view('pengajuan.detail', compact('applications','salary','notifs'));
    }

    public function acceptance(Request $request, $id)
    {
        $this->validate($request,[
            'reason' => 'required',
            'status' => 'required',
        ]);
        DB::transaction(function () use ($request,$id) {
        	$application = Application::findOrFail($id);
        	$application->update([
        		'application_status_id' => $request->status === 'terima' ? 2 : 3,
        		'acceptance_msg' => $request->reason
        	]);
            $name = $application->student->user_detail->fullname;
            if($request->status === 'terima')
            {
                $amount = ((1-$application->programs->discount/100) * $application->programs->price);
                $installment = new Installment();
                $installment->amount_unpaid = $amount;
                $installment->application_id = $id;
                $installment->save();

                $tahun = substr($application->created_at,0,4);
                $bulan = substr($application->created_at,5,2);
                $tgl = substr($application->created_at,8,2);
                $cicilan = $application->installment_period;
                $amount =  $amount / $cicilan ;
                $items = array();
                for($x = 1; $x <= $cicilan; $x++)
                {
                    if($tgl <= 28)
                    {
                        
                        if($bulan >= 13)
                        {
                            $bulan = 1;
                            $tahun = $tahun + 1;
                        }
                        $item = array(
                            'due_date' => $tahun . "-" . $bulan . "-" . $tgl,
                            'amount' => $amount,
                            'installment_id' => $installment->id,
                            'installment_detail_status_id' => 1,
                        );
                        $items[] = $item;
                    }
                    else{
                        if($bulan >= 13)
                        {
                            $bulan = 1;
                            $tahun = $tahun + 1;
                        }
                        $item = array(
                            'due_date' => $tahun . "-" . $bulan . "-" . "28",
                            'amount' => $amount,
                            'installment_id' => $installment->id,
                            'installment_detail_status_id' => 1,
                        );
                        $items[] = $item;
                    }
                   $bulan++; 
                }
                InstallmentDetail::insert($items);
            }
            // $IdInstitution = $application->programs::findOrFail($application->program_id);
            // $UserNotif = InstitutionUser::where('institution_id',$IdInstitution->institution_id)->where('role_id',2)->get();
            // $ItemNotifs = array();
            // foreach($UserNotif as $a)
            // {
            //     $itemuser = array(
            //         'user_id' => $a->user_id,
            //         'description' => 'Pengajuan cicilan '.$name .' dinyatakan di'.$request->status ,
            //     );
            //     $ItemNotifs[] = $itemuser;
            // }
            // Notification::insert($ItemNotifs);

            // $application->fullname = $application->student->user_detail->fullname;
            // $application->phone_no = $application->student->phone_no;
            // $application->id_pengajuan = $application->id;
            // $application->status = $request->status;
            // $application->note = $application->acceptance_msg;
            // Mail::to('ilma.fistadri@gmail.com')->send(new ApplicationEmail($application));
        });
        return redirect('/pengajuan')->with('success', 'Pengajuan berhasil di'.$request->status);
    }

    public function delete(Request $request, $id)
    {
    	$application = Application::findOrFail($id);
    	$application->delete();
    	return redirect('/pengajuan')->with('success', 'Pengajuan berhasil dihapus');
    }
}
