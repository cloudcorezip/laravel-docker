<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ScheduleType;
use App\Models\Institution;
use Auth;

class ScheduleTypeController extends Controller
{
    public function index()
    {
        $id = Auth::user()->institution_user->institution_id;
        $JenisJadwal = ScheduleType::where('institution_id', '=', $id)->get();
        return view('master_data.jenis_jadwal.list', compact('JenisJadwal'));
    }
    public function institusi()
    {
        $institutions = Institution::all();
        return view('master_data.jenis_jadwal.list_institusi', compact('institutions'));
    }
    public function detail_institusi($id)
    {
        $Institusi = Institution::where('id','=',$id)->first();
        $JenisJadwal = ScheduleType::where('institution_id', '=', $id)->get();
        return view('master_data.jenis_jadwal.list', compact('JenisJadwal','Institusi'));
    }
    public function create($id)
    {
        $JenisJadwal = new ScheduleType();
        $institutions = Institution::all();
        return view('master_data.jenis_jadwal.form', compact('JenisJadwal','institutions','id'));
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'admin_input' => 'required',
        ]);
        $data = new ScheduleType();
        $data->institution_id = $request->institution_id;
        $data->name = $request->name;
        $data->description = $request->description;
        $data->admin_input = $request->admin_input;
        $data->save();
        if(Auth::user()->hasRole('Admin Cluever')){
            return redirect('/jenis_jadwal/detail_institusi/'.$request->institution_id)->with('success', 'Jenis Jadwal baru berhasil dibuat');
        }
        else{
            return redirect('/jenis_jadwal')->with('success', 'Jenis jadwal baru berhasil dibuat');
        }
        
    }
    public function edit($id)
    {
    	$JenisJadwal = ScheduleType::find($id);
        $institutions = Institution::all();
        return view('master_data.jenis_jadwal.form', compact('JenisJadwal','institutions'));
    }
    public function update(Request $request, $id)
    {
    	$this->validate($request,[
            'name' => 'required',
        ]);
        $data = ScheduleType::find($request->id);
        $data->name = $request->name;
        $data->description = $request->description;
        $data->admin_input = $request->admin_input;
        $data->save();
        if(Auth::user()->hasRole('Admin Cluever')){
            return redirect('/jenis_jadwal/detail_institusi/'.$data->institution_id)->with('success', 'Jenis Jadwal baru berhasil diupdate');
        }
        else{
            return redirect('/jenis_jadwal')->with('success', 'Jenis jadwal baru berhasil diupdate');
        }
    }
    public function detail($id)
    {
    	
    }
    public function delete(Request $request, $id)
    {
    	$data = ScheduleType::findOrFail($id);
    	$data->delete();
        if(Auth::user()->hasRole('Admin Cluever')){
            return redirect('/jenis_jadwal/detail_institusi/'.$data->institution_id)->with('success', 'Jenis Jadwal baru berhasil dihapus');
        }
        else{
            return redirect('/jenis_jadwal')->with('success', 'Jenis jadwal baru berhasil dihapus');
        }
    }
}
