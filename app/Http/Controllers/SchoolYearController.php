<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SchoolYear;
use App\Models\Institution;
use Auth;

class SchoolYearController extends Controller
{
    public function index()
    {
    	$id = Auth::user()->institution_user->institution_id;
        $SchoolYear = SchoolYear::where('institution_id', '=', $id)->get();
        return view('master_data.tahun_ajaran.list', compact('SchoolYear'));
    }
    public function institusi()
    {
        $institutions = Institution::all();
        return view('master_data.tahun_ajaran.list_institusi', compact('institutions'));
    }
    public function detail_institusi($id)
    {
        $Institusi = Institution::where('id','=',$id)->first();
        $SchoolYear = SchoolYear::where('institution_id', '=', $id)->get();
        return view('master_data.tahun_ajaran.list', compact('SchoolYear','Institusi'));
    }
    public function create($id)
    {
        $SchoolYear = new SchoolYear();
        $institutions = Institution::all();
        return view('master_data.tahun_ajaran.form', compact('SchoolYear','institutions','id'));
    }
    public function store(Request $request)
    {
    	$this->validate($request,[
            'name' => 'required',
        ]);
        $data = new SchoolYear();
        $data->institution_id = $request->institution_id;
        $data->name = $request->name;
        $data->description = $request->description;
        $data->save();
        if(Auth::user()->hasRole('Admin Cluever')){
            return redirect('/tahun_ajaran/detail_institusi/'.$request->institution_id)->with('success', 'Tahun Ajaran baru berhasil dibuat');
        }
        else{
            return redirect('/tahun_ajaran')->with('success', 'Tahun Ajaran baru berhasil dibuat');
        }
    }
    public function edit($id)
    {
    	$SchoolYear = SchoolYear::find($id);
        return view('master_data.tahun_ajaran.form', compact('SchoolYear'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
        ]);
        $data = SchoolYear::find($request->id);
        $data->name = $request->name;
        $data->description = $request->description;
        $data->save(); 
        if(Auth::user()->hasRole('Admin Cluever')){
            return redirect('/tahun_ajaran/detail_institusi/'.$data->institution_id)->with('success', 'Tahun Ajaran baru berhasil diupdate');
        }
        else{
            return redirect('/tahun_ajaran')->with('success', 'Tahun Ajaran baru berhasil diupdate');
        }
        
    }
    public function detail($id)
    {
    	
    }
    public function delete(Request $request, $id)
    {
    	$data = SchoolYear::findOrFail($id);
    	$data->delete();
    	if(Auth::user()->hasRole('Admin Cluever')){
            return redirect('/tahun_ajaran/detail_institusi/'.$data->institution_id)->with('success', 'Tahun Ajaran baru berhasil dihapus');
        }
        else{
            return redirect('/tahun_ajaran')->with('success', 'Tahun Ajaran baru berhasil dihapus');
        }
    }
}
