<?php

namespace App\Http\Controllers\API;

use App\Models\Province;
use Illuminate\Http\Request;

class RegionController extends \App\Http\Controllers\Controller
{
    public function getProvinces()
    {
        try {
            $provinces = Province::select('id', 'name')->get();
            return response()->json($provinces);
        } catch (\Exception $e) {
            return response()->json([ 'message' => $e->getMessage() ], 400);
        }
    }
}
