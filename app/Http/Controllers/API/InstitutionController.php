<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Models\Institution;
use App\Models\Program;

class InstitutionController extends \App\Http\Controllers\Controller
{
    public function search(Request $request)
    {
        $_DEFAULT_LIMIT_QUERY = 10;
        $_DEFAULT_PAGE = 0;

        $params = $request->input('q');
        $limit =  $request->input('limit') ?: $_DEFAULT_LIMIT_QUERY;
        $page =  $request->input('page') ? $request->input('page') - 1 : $_DEFAULT_PAGE;
        $sortBy =  $this->getValidSortBy($request->input('sort'));
        $order =  $this->getValidOrder($request->input('order'));
        $searchQuery = trim(strtolower($params));
        $institutions = Institution::select('id', 'name', 'address', 'description', 'logo_path');
        /*
         * It is okay not to send any queries.
         * Example use case: get all institution, sort by id with certain limit
         */
        if ($searchQuery) {
            $institutions = $institutions->where('name', 'ILIKE', "%$searchQuery%")
            ->orWhere('description', 'ILIKE', "%$searchQuery%");
        }
        $institutions = $institutions
            ->orderBy($sortBy, $order)
            ->skip($limit * $page)
            ->take($limit)
            ->get();
        return response()->json([ 'institutions' => $institutions ]);
    }

    private function getValidSortBy($param)
    {
        $_DEFAULT_SORT_COL = 'id';
        $validSort = [ 'id', 'name', 'description' ];
        
        if ($param && in_array($param, $validSort)) {
            return $param;
        }

        return $_DEFAULT_SORT_COL;
    }

    private function getValidOrder($param)
    {
        $_DEFAULT_ORDER = 'asc';
        $validOrder = [ 'asc', 'desc' ];
        $param = strtolower($param);

        if ($param && in_array($param, $validOrder)) {
            return $param;
        }

        return $_DEFAULT_ORDER;
    }

    public function show(Request $request)
    {
        $id = $request->input('institution_id');
        
        if (empty($id)) {
            return response()->json([ 'message' => 'Please specify institution ID' ], 400);
        }

        $institution = Institution::select('id', 'name', 'description', 'address')
		->find($id);
	if (!$institution) {
		return response()->json([ 'message' => 'Institution not found' ], 204);
	}
	$image = 'empty_instlogo.png';
	if (sizeof($institution->images) > 0) {
		$image = $institution->images[0];
	}
	$institution->image = $image;
	unset($institution->images);
        $programs = Program::select('id', 'name', 'description', 'price', 'discount')
            ->where('institution_id', $id)
            ->get();
        $response = [];
        $response[] = [
            'institution' => $institution,
            'programs' => $programs,
        ];
        return response()->json($response, 200);
    }
}
