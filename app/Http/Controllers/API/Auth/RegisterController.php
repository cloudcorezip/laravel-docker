<?php

namespace App\Http\Controllers\API\Auth;

use App\Models\User;
use App\Models\UserDetail;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use DB;

class RegisterController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/
	
	use RegistersUsers;
	
	/**
	 * Where to redirect users after login / registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';
	protected $registerView = 'auth.register';
	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}
	
	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	protected function create(Request $request)
	{
		$userValidator = \Validator::make($request->input('user'), [
			'nik' => 'required|unique:users',
		    'phone_no' => 'required|unique:users',
		    'password' => 'required',
		    'password_confirmation' => 'required|same:password'
		]);
		$userDetailValidator = \Validator::make($request->input('user_detail'), [
			'fullname' => 'required',
		    'email' => 'required|unique:user_details',
		]);
        if ($userValidator->fails()) {
        	return response()->json(['message' => $userValidator->errors()], 400);
        }
        if ($userDetailValidator->fails()) {
        	return response()->json(['message' => $userDetailValidator->errors()], 400);
        }
		DB::beginTransaction();
		try {
			$user = User::create([
				'nik' => $request->input('user')['nik'],
				'phone_no' => $request->input('user')['phone_no'],
				'password_digest' => bcrypt($request->input('user')['password']),
			]);
			$userNotVerifiedId = 1;
			$detail = UserDetail::create([
				'fullname' => $request->input('user_detail')['fullname'],
				'email' => $request->input('user_detail')['email'],
				'user_id' => $user->id,
				'user_status_id' => $userNotVerifiedId,
				'self_image' => 'empty_profpict.png',
				'id_card_image' => 'empty_identity.png',
			]);
			DB::commit();
			$token = JWTAuth::attempt([
				'phone_no' => $request->input('user')['phone_no'],
				'password' => $request->input('user')['password'],
			]);
			return response()->json([
				'message' => 'Create user success',
				'auth_token' => $token,
			], 200);
		} catch (\Exception $e) {
			DB::rollback();
			return response()->json(['error' => $e], 400);
		}
	}
}
