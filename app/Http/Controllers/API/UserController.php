<?php

namespace App\Http\Controllers\API;

use DB;
use Auth;
use Exception;

use App\Models\User;
use App\Models\Program;
use App\Models\Institution;
use App\Models\UserDetail;
use App\Models\UserConfiguration
;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends \App\Http\Controllers\Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function profile(Request $request)
    {
        try {
            $user = Auth::user();
            return response()->json([
                'id' => $user->id,
                'phone_no' => $user->phone_no,
                'nik' => $user->nik,
                'detail' => $user->user_detail
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function show($id)
    {
        return response()->json([ 'id' => $id ], 200);
    }

    public function getUserConfig()
    {
        try {
            $user = Auth::user();
            $configuration = $user->configuration;
            return response()->json([
                'id' => $configuration->id,
                'user_id' => $configuration->user_id,
                'conf_biaya_pendidikan' => $this->validateConfigInput($configuration->conf_biaya_pendidikan),
                'conf_program_pendidikan' => $this->validateConfigInput($configuration->conf_program_pendidikan),
                'conf_rapor_harian' => $this->validateConfigInput($configuration->conf_rapor_harian),
                'conf_marketplace' => $this->validateConfigInput($configuration->conf_marketplace),
                'conf_antar_jemput' => $this->validateConfigInput($configuration->conf_antar_jemput),
                'created_at' => $configuration->created_at,
                'updated_at' => $configuration->updated_at,
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e
            ], 500);
        }
    }

    public function updateUserConfig(Request $request)
    {
        $validator = \Validator::make($request->input(), [
            'user_id' => 'required',
            'conf_biaya_pendidikan' => 'required',
            'conf_program_pendidikan' => 'required',
            'conf_rapor_harian' => 'required',
            'conf_marketplace' => 'required',
            'conf_antar_jemput' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([ 'message' => $validator->errors() ], 400);
        }
        try {
            $configuration = UserConfiguration::where('user_id', $request->input('user_id'))->firstOrFail();
            $configuration->fill($request->all())->save();
            return response()->json([ 'message' => 'Success' ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'message' => 'record not found error, user_id: ' . $request->input('user_id'),
            ], 400);
        } catch (Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 400);
        }
    }

    public function createSecondaryUser(Request $request)
    {
        $user = Auth::user();

        $inputCollection = [
            'user' => $request->input('user'),
            'users' => $request->input('user'), // For the sake of validation
            'user_detail' => $request->input('user_detail'),
            'add' => $request->input('add'),
        ];

        $validator = \Validator::make($inputCollection, [
            'user.password' => 'required',
            'user.password_confirmation' => 'required|same:user.password',
            'users.nik' => 'required|unique:users',
            'users.phone_no' => 'required|unique:users',
            'user_detail.fullname' => 'required',
            'user_detail.birthplace' => 'required',
            'user_detail.birthdate' => 'required',
            'user_detail.address' => 'required',
            'add' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }

        /*
         * Find out whether secondary user has been already existed.
         * Identifier: phone number & NIK
         */

        $existingUser = null;

        if (!empty($request->input('user')['nik']) && !empty($request->input('user')['phone_no'])) {
            $existingUser = User::where('nik', $request->input('user')['nik'])
                ->where('phone_no', $request->input('user')['phone_no'])
                ->first();
        }

        DB::beginTransaction();

        if ($existingUser == null) { // Create new user
            try { // Do create new user
                // Table: users
                $encryptedPass = str_replace('$2y', '$2a', bcrypt($inputCollection['user']['password']));
                $inputCollection['user']['password_digest'] = $encryptedPass;
                $existingUser = User::create($inputCollection['user']);

                // Table: user_details
                $inputCollection['user_detail']['user_id'] = $existingUser->id;
                $inputCollection['user_detail']['id_card_image'] = 'empty_identity.png';
                $inputCollection['user_detail']['self_image'] = 'empty_profpict.png';
        		$inputCollection['user_detail']['user_status_id'] = 2; // STATUS 2 = NOT VERIFIED
        		$inputCollection['user_detail']['birthdate'] = \Carbon\Carbon::createFromFormat('d-m-Y', $request->input('user_detail')['birthdate']);
                UserDetail::create($inputCollection['user_detail']);
            } catch (Exception $e) {
                return response()->json([ 'message' => $e->getMessage() ], 400);
                DB::rollback();
            }
        }
        
        try {
            if ($request->input('add') == 'parent') {
                $user->user_detail->update(['parent_id' => $existingUser->id]);
            } else {
                $existingUser->user_detail->update([ 'parent_id' => $user->id ]);
            }

            DB::commit();
        } catch (Exception $e) {
            return response()->json([ 'message' => $e->getMessage() ], 400);
            DB::rollback();
        }

        return response()->json(['message' => 'Create user success'], 200);
    }

    private function validateConfigInput($config)
    {
        return $config == 1 ? true : false;
    }

    public function checkToken(Request $request)
    {
        return response()->json([
            'message' => 'Token Valid'
        ], 200);
    }
}
