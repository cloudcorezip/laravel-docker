<?php

namespace App\Http\Controllers\API;

use DB;
use Exception;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Models\Payment;
use App\Models\Installment;
use App\Models\Application;
use App\Models\InstallmentDetail;
use App\Models\TransactionHistory;

use Midtrans\Config;
use Midtrans\CoreApi;
use Tymon\JWTAuth\Facades\JWTAuth;

class PaymentController extends \App\Http\Controllers\Controller
{
  public function __construct()
  {
    $this->middleware('jwt.auth');
  }

  public function pending()
  {
    try {
      $pendingCount = 0;
      $installmentDetail = InstallmentDetail::whereRaw('extract (month FROM due_date) = extract (month FROM CURRENT_DATE)')
        ->whereHas('installment', function ($q) {
          $q->whereHas('application', function ($q2) {
            $q2->where('pic_id', Auth::user()->id);
          });
        })
        ->get();
      foreach($installmentDetail as $detail) {
        $pendingCount += $detail->amount;
      }
      return response()->json([ 'pending' => $pendingCount ]);
    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage() ], 401);
    }
  }

  public function checkInstallmentUpdate()
  {
    try {
      $updates = 0;
      foreach (Auth::user()->applications as $application) {
        $updates += $application
            ->installments
            ->where('need_update', 1)
            ->count();
      }
      return response()->json([
        'value' => $updates,
        'status' => 200,
      ], 200);
    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage() ], 401);
    }
  }

  public function transactionHistories()
  {
    try {
      $txs = TransactionHistory::join('payments', 'payment_id', '=', 'payments.id')
        ->where('user_id', Auth::user()->id)
        ->where('name', 'biaya_pendidikan') // TO DO
        ->select('tx_histories.id', 'name', 'description', 'tx_status_id', 'payments.amount', 'payments.program_id', 'tx_histories.created_at')
        ->orderBy('created_at', 'DESC')
        ->get();
      return response()->json([ 'history' => $txs ], 200);
    } catch (Exception $e) {
      return response()->json([ 'message' => 'Token invalid' ], 401);
    }
  }

  public function installmentList()
  {
    $response = [];
    $response[] = [];
    $applications = [];
    foreach(Application::where('pic_id', Auth::user()->id)->get() as $key => $application) {
      $installments = [];
      foreach ($application->installments as $installment) {
        foreach ($installment->details as $installmentDetail) {
          $installments[] = [
            'id' => $installmentDetail->id,
            'amount' => $installmentDetail->amount,
            'due_date' => $installmentDetail->due_date,
            'status' => $installmentDetail->status->status,
          ];
        }
      }
      $applications[] = [
        'fullname' => $application->student->user_detail->fullname,
        'accepted' => $application->updated_at,
        'institution' => $application->programs->institutions->name,
        'installments' => $installments
      ];
    }
    return response()->json($applications, 200);
  }

  public function createApplication(Request $request)
  {
    try {
      $input = $request->input();
      $input['installment_period'] = 10;
      $input['application_status_id'] = 1;
      $application = Application::create($input);
      return response()->json([
        'message' => 'Application Created Successfully',
        'data' => $application
      ], 200);
    } catch (Exception $e) {
      return response()->json([ 'message' => $e->getMessage() ], 400);
    }
  }

  public function payToMidtrans(Request $request)
  {
    $user = Auth::user();
    $data = $request->input('transfer');

    // Get valid installment detail
    try {
      $installmentDetail = InstallmentDetail::findOrFail($data['trf_id']);
    } catch (ModelNotFoundException $e) {
      return response()->json([ 'message' => 'Installment detail (' . $data['trf_id'] . ') not found' ], 404);
    } catch (Exception $e) {
      return response()->json([ 'message' => $e->getMessage() ], 400);
    }

    Config::$serverKey = 'SB-Mid-server-AGx64GU4_X_5Qt3QkhhjijKi';
    Config::$isProduction = false;
    Config::$isSanitized = true;
    Config::$is3ds = true;

    $midtransOrderId = "B-".time()."-".$installmentDetail->id;
    
    $transactionDetail = [
        'order_id'      => $midtransOrderId,
        'gross_amount'  => $data['amount'],
    ];

    $item = [
      [
        'id'        => $installmentDetail->id,
        'price'     => $data['amount'],
        'quantity'  => 1,
        'name'      => $data['fullname'],
      ],
    ];

    $customerDetails = [
      'first_name'   => $user->user_detail->fullname,
      'email'        => $user->user_detail->email,
      'address'      => $user->user_detail->address,
      'postal_code'  => $user->user_detail->postcode->code,
      'phone'        => $user->phone_no,
      'country_code' => 'IDN',
    ];

    $bankTransfer = [
      'bank' => $data['destination'],
    ];

    $transactionData = array(
      'payment_type'        => $data['method'],
      'transaction_details' => $transactionDetail,
      'item_details'        => $item,
      'customer_details'    => $customerDetails,
      'bank_transfer'       => $bankTransfer,
    );

    $response = CoreApi::charge($transactionData);
    $statusCode = 0;

    if (!empty($response->status_code)) {
      $statusCode = (int) $response->status_code;
    }

    if ($statusCode > 0 && $statusCode == 200 || $statusCode == 201) {
      if ($statusCode == 200 || $statusCode == 201) {
        $payment = $this->storeToPayment([
          'installment_detail' => $installmentDetail,
          'amount' => $data['amount'],
          'midtrans_order_id' => $midtransOrderId,
          'destination_va' => $response->va_numbers[0]->va_number,
        ]);

        $this->storeToTransactionHistory([
          'installment_detail' => $installmentDetail,
          'payment_id' => $payment->id,
          'midtrans_response' => $response,
          'midtrans_status' => $response->transaction_status,
        ]);

        // TO-DO: CREATE NOTIFICATION
      }
      return response()->json($response, $statusCode);
    }
    
    return response()->json($response, 500);
  }

  private function storeToPayment($params)
  {
    return Payment::create([
      'payment_type_id' => 1,
      'payment_status_id' => 1,
      'installment_detail_id' => $params['installment_detail']->id,
      'application_id' => $params['installment_detail']->installment->application->id,
      'need_update' => false,
      'program_id' => $params['installment_detail']->installment->application->program->id,
      'amount' => $params['amount'],
      'details' => '',
    ]);
  }

  private function storeToTransactionHistory($params)
  {
    $user = Auth::user();

    TransactionHistory::create([
      'name' => 'biaya_pendidikan',
      'description' => "Transaksi berhasil dibuat, dengan status " . $params['midtrans_status'],
      'user_id' => $user->id,
      'payment_id' => $params['payment_id'],
      'tx_status_id' => 1, // Pending
      'midtrans_response' => json_encode($params['midtrans_response']),
      'created_by_id' => $user->id,
    ]);
  }
}
