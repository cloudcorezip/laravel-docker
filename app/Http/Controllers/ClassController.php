<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Classes;
use App\Models\Institution;
use App\Models\Program;
use App\Models\Subject;
use App\Models\UserRole;
use App\Models\Schedule;
use App\Models\StudentClass;
use App\Models\InstitutionUser;
use App\Models\SchoolYear;
use Auth;
class ClassController extends Controller
{
    public function index()
    {
        $institutionid=Auth::user()->institution_user->institution_id;
        $kelas = Classes::where('institution_id', '=', $institutionid)->get();
        return view('master_data.kelas.list', compact('kelas'));
    }
    public function institusi()
    {
        $institutions = Institution::all();
        return view('master_data.kelas.list_institusi', compact('institutions'));
    }
    public function detail_institusi($id)
    {
        $Institusi = Institution::where('id','=',$id)->first();
        $kelas = Classes::where('institution_id', '=', $id)->get();
        return view('master_data.kelas.list', compact('kelas','Institusi'));
    }
    public function create($id)
    {
        $program = Program::where('institution_id','=',$id)->get();
        $SchoolYear = SchoolYear::where('institution_id','=',$id)->get();
        $subjects = Subject::where('institution_id','=',$id)->get();
        $user_roles = InstitutionUser::distinct()->where(function($query){
                $query->where('role_id', '=', 4)->orWhere('role_id', '=', 5);
            })->where('institution_id', '=',$id)->get(['user_id']);
    	$classes = new Classes();
        $schedules = new Schedule();
        return view('master_data.kelas.form', compact('program','classes','user_roles','subjects','schedules','SchoolYear','id'));
    }
    public function store(Request $request)
    {
    	$this->validate($request,[
            'class_code' => 'required',
            'class_name' => 'required',
            'program_id' => 'required',
            'homeroom_teacher_id' => 'required',
            'school_year_id' => 'required',
            'capacity' => 'required',
        ]);
        $data = new Classes();
        $data->class_code = $request->class_code;
        $data->class_name = $request->class_name;
        $data->school_year_id = $request->school_year_id;
        $data->program_id = $request->program_id;
        $data->institution_id = $request->institution_id;
        $data->homeroom_teacher_id = $request->homeroom_teacher_id;
        $data->capacity = $request->capacity;
        $data->save();
        if(Auth::user()->hasRole('Admin Cluever')){
            return redirect('/kelas/detail_institusi/'.$request->institution_id)->with('success', 'Kelas baru berhasil dibuat');
        }
        else{
            return redirect('/kelas')->with('success', 'Kelas baru berhasil dibuat');
        }
    }
    public function edit($id)
    {
        $classes = Classes::find($id);
        $institutionid = $classes->institution_id;
        $program = Program::where('institution_id','=',$institutionid)->get();
        $subjects = Subject::where('institution_id','=',$institutionid)->get();
        $SchoolYear = SchoolYear::where('institution_id','=',$institutionid)->get();
        $user_roles = InstitutionUser::distinct()->where('role_id', '=', 4)->where('institution_id', '=',$institutionid)->get(['role_id','user_id']);
        $schedules= Schedule::where('class_id','=',$id)->get();
        return view('master_data.kelas.form', compact('classes','program','user_roles','schedules','subjects','SchoolYear'));
    }
    public function update(Request $request,$id)
    {
    	$this->validate($request,[
            'class_code' => 'required',
            'class_name' => 'required',
            'program_id' => 'required',
            'homeroom_teacher_id' => 'required',
            'school_year_id' => 'required',
            'capacity' => 'required',
        ]);
        $data = Classes::find($id);
        $data->class_code = $request->class_code;
        $data->class_name = $request->class_name;
        $data->school_year_id = $request->school_year_id;
        $data->program_id = $request->program_id;
        $data->homeroom_teacher_id = $request->homeroom_teacher_id;
        $data->capacity = $request->capacity;
        $data->save();
        if(Auth::user()->hasRole('Admin Cluever')){
            return redirect('/kelas/detail_institusi/'.$data->institution_id)->with('success', 'Kelas berhasil diupdate');
        }
        else{
            return redirect('/kelas')->with('success', 'Kelas berhasil diupdate');
        }
    }
    public function detail($id)
    {
        $kelas = Classes::find($id);
        $institutionid = $kelas->institution_id;
        $student_class = StudentClass::where('class_id','=',$id)->get();
        $student_selected = [];
        foreach ($student_class as $key => $value) {
            $student_selected[] = $value->student_id;
        }
        $user_roles = InstitutionUser::distinct()->where('role_id', '=', 7)
            ->where('institution_id', '=',$institutionid)->whereNotIn('user_id', $student_selected)->get(['role_id','user_id']);

        return view('master_data.kelas.detail',compact('user_roles','kelas','student_class'));
    }
    public function add_siswakelas($id, $classid)
    {
        $data = new StudentClass();
        $data->class_id = $classid;
        $data->student_id = $id;
        $data->save();
        return back()->with('success', "Siswa berhasil ditambahkan");
    }
    public function delete_siswakelas(Request $request, $id)
    {
        $application = StudentClass::findOrFail($id);
        $application->delete();
        return back()->with('success', "Siswa berhasil dihapus");
    }
}
