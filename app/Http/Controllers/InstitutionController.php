<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Institution;
use App\Models\InstitutionImage;
use App\Models\Program;
use App\Models\Province;
use App\Models\City;
use App\Models\District;
use App\Models\Postcode;
use Illuminate\Support\Facades\DB;

class InstitutionController extends Controller
{
    public function index()
    {
    	$institusi = Institution::all();
        return view('master_data.institusi.list', compact('institusi'));

    }
    public function create()
    {
    	$institusi = new Institution();
        $province = Province::all();
        $city = City::all();
        $district = District::all();
        $postcode = Postcode::all();
        return view('master_data.institusi.form',compact('institusi','province','city','district','postcode'));
    }
    public function store(Request $request)
    {
    	$this->validate($request,[
            'name' => 'required',
            'address' => 'required',
            'postcode_id' => 'required',
        ]);
        DB::transaction(function () use ($request) {
            $data = new Institution();
            if ($request->hasFile('logo_path')) {
                $file = $request->file('logo_path');
                $logo = "inst-".time().".".$file->extension();
                $tujuan_upload = public_path('images');
                $file->move($tujuan_upload,$logo);
                $data->logo_path = $logo;
            }
            else{
                $data->logo_path = "empty_instlogo.png";
            }

            $data->name = $request->name;
            $data->postcode_id = $request->postcode_id;
            $data->address = $request->address;
            $data->website = $request->website;
            $data->description = $request->description;
            $data->institution_type_id = 1;
            $data->save();

            if ($request->hasFile('path')) {
                $i = 0;
                foreach($request->file('path') as $file2){
                    $photo = new InstitutionImage;
                    $imageName = time() . $i . '.' . $file2->extension();
                    $file2->move(public_path().'/images/', $imageName); 
                    $photo->path = $imageName;
                    $photo->institution_id = $data->id;
                    $photo->save();
                    $i++;
                } 
            }
            else{
                $photo = new InstitutionImage;
                $photo->path = "empty_image.png";
                $photo->institution_id = $data->id;
                $photo->save();
            }
        });
        return redirect('/institusi')->with('success', 'Institusi baru berhasil dibuat');
    }
    public function edit($id)
    {
        $institusi = Institution::find($id);
        $province = Province::all();
        $city = City::all();
        $district = District::all();
        $postcode = Postcode::all();
        return view('master_data.institusi.form',compact('institusi','province','city','district','postcode'));
    }
    public function update(Request $request, $id)
    {
    	$this->validate($request,[
            'name' => 'required',
            'address' => 'required',
            'postcode_id' => 'required',
        ]);
        DB::transaction(function () use ($request,$id) {
            $data = Institution::find($id);
            if ($request->hasFile('logo_path')) {
                $file = $request->file('logo_path');
                $logo = time().".".$file->extension();
                $tujuan_upload = public_path('images');
                $file->move($tujuan_upload,$logo);
                $data->logo_path = $logo;
                }
            $data->name = $request->name;
            $data->postcode_id = $request->postcode_id;
            $data->address = $request->address;
            $data->website = $request->website;
            $data->description = $request->description;
            $data->institution_type_id = 1;
            $data->save();

            if ($request->hasFile('path')) {
                $i = 0;
                foreach($request->file('path') as $file2){
                    $photo = new InstitutionImage;
                    $imageName = time() . $i . '.' . $file2->extension();
                    $file2->move(public_path().'/images/', $imageName); 
                    $photo->path = $imageName;
                    $photo->institution_id = $request->id;
                    $photo->save();
                    $i++;
                } 
            }
        });
        return redirect('/institusi')->with('success', "Institusi berhasil diupdate");

    }
    public function detail($id)
    {
    	$program = Program::where('institution_id', '=', $id)->get();
        $institusi = Institution::find($id);
        return view('master_data.institusi.detail', compact('institusi','program'));
    }
    public function delete(Request $request, $id)
    {
    	$data = Institution::findOrFail($id);
    	$data->delete();
    	return back()->with('success', 'Institusi berhasil dihapus');
    }
}
