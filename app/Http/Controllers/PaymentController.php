<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Program;
use App\Models\Payment;
use App\Models\PaymentType;
use App\Models\PaymentStatus;
use Auth;

class PaymentController extends Controller
{
    public function index()
    {
    	if (Auth::user()->hasRole('Admin Cluever')) {
    		$payments = Payment::all();
    	} else if (Auth::user()->hasRole('Admin Institusi')) {
    		$payments = Payment::all(); // To DO: based on institution
    	}
    	return view('payment.list', compact('payments'));
    }
    public function create(Request $request)
    {
        $payment = new Payment;
        $paymentTypes = PaymentType::all();
        $paymentStatuses = PaymentStatus::all();
        $institutions = Auth::user()->institutions;
        if (sizeof($institutions) > 0) {
            $programs = Program::where('institution_id', $institutions[0]->id)->get();
        } else {
            $programs = [];
        }
    	return view('payment.form', compact('paymentTypes', 'paymentStatuses', 'programs', 'payment'));
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'details' => 'required',
            'amount' => 'required|numeric',
            'type' => 'required',
            'status' => 'required',
            'program' => 'required',
        ]);
        Payment::create([
            'payment_type_id' => $request->type,
            'payment_status_id' => $request->status,
            'program_id' => $request->program,
            'amount' => $request->amount,
            'details' => $request->details,
        ]);
        return redirect('payment')->with('success', 'Transaksi pembayaran berhasil ditambahkan');
    }
    public function show($id)
    {
        $payment = Payment::findOrFail($id);
        return view('payment.detail', compact('payment'));
    }
    public function edit($id)
    {
        $paymentTypes = PaymentType::all();
        $paymentStatuses = PaymentStatus::all();
        $institutions = Auth::user()->institutions;
        if (sizeof($institutions) > 0) {
            $programs = Program::where('institution_id', $institutions[0]->id)->get();
        } else {
            $programs = [];
        }
        $payment = Payment::findOrFail($id);
        return view('payment.form', compact('payment', 'paymentTypes', 'paymentStatuses', 'programs'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'details' => 'required',
            'amount' => 'required|numeric',
            'type' => 'required',
            'status' => 'required',
            'program' => 'required',
        ]);
        $payment = Payment::findOrFail($id);
        $payment->update([
            'payment_type_id' => $request->type,
            'payment_status_id' => $request->status,
            'program_id' => $request->program,
            'amount' => $request->amount,
            'details' => $request->details,
        ]);
        return redirect('payment')->with('success', 'Transaksi pembayaran berhasil diubah');
    }
    public function delete($id)
    {
        $payment = Payment::findOrFail($id);
        $payment->delete();
        return redirect('payment')->with('success', 'Transaksi pembayaran berhasil dihapus oleh ' . Auth::user()->user_detail->fullname);
    }
}
