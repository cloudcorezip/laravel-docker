<?php

namespace App\Http\Controllers;
use App\Models\RegistrationProposal;
use App\Models\Salary;
use App\Models\InstitutionUser;
use App\Mail\RegistrationProposalEmail;
use Illuminate\Support\Facades\Mail;
use Auth;
use Illuminate\Http\Request;
use Twilio\Rest\Client;


class RegistrationProposalController extends Controller
{

    public function index()
    {
    	$proposals = RegistrationProposal::all();
    	return view('proposal.list', compact('proposals'));
    }
    public function show($id)
    {
        $proposal = RegistrationProposal::findOrFail($id);
        $picid = $proposal->pic_id;
        $salary = Salary::where('user_id', '=', $picid)->get();
        return view('proposal.detail', compact('proposal','salary'));
    }
    public function acceptance(Request $request, $id)
    {
        $this->validate($request,[
            'status' => 'required',
        ]);

    	$proposal = RegistrationProposal::findOrFail($id);
    	$proposal->update([
    		'application_status_id' => $request->status === 'terima' ? 2 : 3,
    	]);
    	$pic = $proposal->pic_id;
    	$student = $proposal->student->id;
    	$data = new InstitutionUser;
    	$datapic = new InstitutionUser;
    	$data->user_id = $student;
    	$data->institution_id = Auth::user()->institution_user->institution_id;
    	$data->role_id = 7;
    	$data->save();
    	$datapic->user_id = $pic;
    	$datapic->institution_id = Auth::user()->institution_user->institution_id;
    	$datapic->role_id = 6;
    	$datapic->save();

    	$proposal->fullname = $proposal->pic->user_detail->fullname;
		Mail::to('ilma.fistadri@gmail.com')->send(new RegistrationProposalEmail($proposal));

	    $account_sid=\Config::get('value.TWILIO_SID');
	    $auth_token=\Config::get('value.TWILIO_AUTH_TOKEN');
	    $twilio_number=\Config::get('value.TWILIO_NUMBER');
	    $client = new Client($account_sid, $auth_token);
	    $client->messages->create('+628176872525', 
            ['from' => $twilio_number, 'body' => 'ini pesan'] );
	    

        return redirect('/proposal')->with('success', 'Follow Up berhasil di'.$request->status);
    }
    public function delete(Request $request, $id)
    {
    	$proposal = RegistrationProposal::findOrFail($id);
    	$proposal->delete();
    	return redirect('/proposal')->with('success', 'Pengajuan berhasil dihapus');
    }
}
