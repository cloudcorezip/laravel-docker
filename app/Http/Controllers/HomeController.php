<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Users;
use App\Role;
use Auth;
use Illuminate\Support\Facades\Input;
use App\Models\Province;
use App\Models\City;
use App\Models\District;
use App\Models\Postcode;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
        
    }
    public function regencies(){
      $provinces_id = Input::get('province_id');
      $regencies = City::where('province_id', '=', $provinces_id)->get();
      return response()->json($regencies);
    }
    public function districts(){
      $regencies_id = Input::get('regencies_id');
      $districts = District::where('city_id', '=', $regencies_id)->get();
      return response()->json($districts);
    }
    public function postcodes(){
      $districts_id = Input::get('districts_id');
      $districts = Postcode::where('district_id', '=', $districts_id)->get();
      return response()->json($districts);
    }
    
}

