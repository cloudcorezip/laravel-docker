<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Institution;
use App\Models\School;
use App\Models\SchoolGrade;
use Illuminate\Support\Facades\DB;
use App\Models\UserDetail;
use App\Models\InstitutionUser;
use App\Models\UserRole;
use App\Models\Salary;
use App\Imports\ImportUsers;
use App\Models\Province;
use App\Models\City;
use App\Models\District;
use App\Models\Postcode;
use App\Models\Notification;
use Illuminate\Support\Facades\Hash;
use App\Mail\UserEmail;
use App\Mail\UserPasswordEmail;
use Auth;
use Excel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        if (Auth::user()->hasRole('Admin Cluever')){
    	   $users = User::all();
        }
        else{
            $id = Auth::user()->institution_user->institution_id;
            $users = InstitutionUser::distinct()->where('institution_id', '=', $id)->get(['user_id']);
        }

        return view('master_data.user.list', compact('users'));
    }
    public function create()
    {
    	$user_details = new UserDetail();
    	$users = new User();
        $institution = Institution::all();
        $province = Province::all();
        $city = City::all();
        $district = District::all();
        $postcode = Postcode::all();
        $salary = new Salary();
        $Schools = School::all();
        $SchoolGrades = SchoolGrade::all();
        return view('master_data.user.form', compact('institution','users','user_details','province','city','district','postcode','salary','Schools','SchoolGrade'));
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'nik' => 'required|unique:users|max:255',
            'phone_no' => 'required|unique:users|max:255',
            'email' => 'required|unique:user_details|max:255',
            'fullname' => 'required',
            'role_id1' => 'required',
            'postcode_id' => 'required',
        ]);
        DB::transaction(function () use ($request) {
            $datausers = new User();
            $datadetail = new UserDetail();
            $datarole = new UserRole();

            if ($request->hasFile('id_card_image')) {
                $file = $request->file('id_card_image');
                $nama_file = time().".".$file->extension();
                $tujuan_upload = public_path('images');
                $file->move($tujuan_upload,$nama_file);
                $datadetail->id_card_image =  $nama_file ;
            }
            else{
                $datadetail->id_card_image =  "empty_identity.png";
            }
            $datausers->nik = $request->nik;
            $datausers->password_digest = str_replace('$2y', '$2a', bcrypt('cluever'));
            $datausers->phone_no = $request->phone_no;
            $datausers->created_by_id =Auth::user()->id;
            $datausers->save();

            $datadetail->user_id =$datausers->id;
            $datadetail->fullname =$request->fullname;
            $datadetail->email = $request->email;
            $datadetail->address = $request->address;
            $datadetail->self_image = 'empty_profpict.png';
            $datadetail->birthplace = $request->birthplace;
            $datadetail->birthdate = $request->birthdate != '' ? $request->birthdate : '1970-01-01' ;
            $datadetail->occupation = $request->occupation;
            $datadetail->school_id = empty($request->school_id) ? null : $request->school_id;
            $datadetail->salary = empty($request->salary) ? null : $request->salary;
            $datadetail->postcode_id = $request->postcode_id;
            $datadetail->user_status_id = "1";
            $datadetail->created_by_id = Auth::user()->id;
            $datadetail->save();

            // $datadetail->fullname = $datadetail->fullname;
            // Mail::to('ilma.fistadri@gmail.com')->send(new UserPasswordEmail($datadetail));

            if (Auth::user()->hasRole('Admin Cluever'))
            {
                $t = 0;
                foreach($request->role_id1 as $key => $roleid){
                $idinst = empty($request->institution_id[$roleid]) ? null : $request->institution_id[$roleid];
                    DB::insert('insert into roles_users (role_id, user_id, created_by_id) values (?, ?, ?)', [$roleid, $datausers->id, Auth::user()->id]);
                	$idinstitution = DB::insert('insert into institution_users (role_id, user_id, institution_id) values (?, ?, ?)', [$roleid, $datausers->id,  $idinst ]);
                    if($roleid == 2){
                        $idrole = 'Admin Institusi';
                    }
                    elseif($roleid == 4){
                        $idrole = 'Educator';
                    }
                    elseif ($roleid == 5) {
                        $idrole = 'Wali Kelas';
                    }
                    elseif($roleid == 6){
                        $idrole = 'Orang Tua';
                    }
                    else{
                        $idrole = 'Siswa';
                    }
                    // $NameInstitution = Institution::findOrFail($request->institution_id[$roleid]);

                    // $UserNotif = InstitutionUser::where('institution_id',$request->institution_id[$roleid])->where('role_id',2)->get();
                    // $ItemNotifs = array();
                    // foreach($UserNotif as $a)
                    // {
                    //     $itemuser = array(
                    //         'user_id' => $a->user_id,
                    //         'description' => $datadetail->fullname . ' menjadi '.$idrole .' di '.$NameInstitution->name,
                    //     );
                    //     $ItemNotifs[] = $itemuser;
                    //     $datadetail->role_id = $idrole;
                    //     Mail::to('ilma.fistadri@gmail.com')->send(new UserEmail($datadetail));
                    // }
                    // Notification::insert($ItemNotifs);
                    // $t++;
                }

            }
            else{
                $t = 0;
                foreach($request->role_id1 as $key => $roleid){
                    $idinst = empty($request->institution_id[$roleid]) ? null : $request->institution_id[$roleid];
                    DB::insert('insert into roles_users (role_id, user_id, created_by_id) values (?, ?, ?)', [$roleid, $datausers->id, Auth::user()->id]);
                    DB::insert('insert into institution_users (role_id, user_id, institution_id) values (?, ?, ?)', [$roleid, $datausers->id,  Auth::user()->institution_user->institution_id]);
                    $t++;
                }
            }
            
            if ($request->hasFile('path')) {
                $i = 0;
                foreach($request->file('path') as $file2){
                    $photo = new Salary;
                    $imageName = time() . $i . '.' . $file2->extension();
                    $file2->move(public_path().'/images/', $imageName); 
                    $photo->path = $imageName;
                    $photo->user_id = $datausers->id;
                    $photo->created_by_id =Auth::user()->id;
                    $photo->save();
                    $i++;
                } 
            }
        });
        return redirect('/user')->with('success', 'User baru berhasil dibuat');
    }
    public function edit($id)
    {
        if(Auth::user()->hasAnyRole(['Admin Institusi','Admin Cluever'])) {
            $users = User::find($id);
        } else {
            $users = User::find(Auth::user()->id);
        }
        $user_details = UserDetail::all();
        $institution = Institution::all();
        $province = Province::all();
        $city = City::all();
        $district = District::all();
        $postcode = Postcode::all();
        $salary = Salary::where('user_id', '=', $id)->get();
        $Schools = School::all();
        $SchoolGrades = SchoolGrade::all();
        return view('master_data.user.form', compact('institution','users','user_details','province','city','district','postcode','salary','Schools','SchoolGrade'));
    }
    public function delete_salary($id)
    {
        $data = Salary::find($id);
        $data->delete();
        return back();
    }
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'fullname' => 'required',
            'email' => 'required|email|unique:user_details,email,'.$id,
            'phone_no' => 'required|unique:users,phone_no,'.$id,
            'postcode_id' => 'required',
        ]);
        DB::transaction(function () use ($request, $id){
            $datadetail = UserDetail::where('user_id','=',$id)->first();
            if ($request->hasFile('id_card_image')) {
                $file = $request->file('id_card_image');
                $nama_file = time().".".$file->extension();
                $tujuan_upload = public_path('images');
                $file->move($tujuan_upload,$nama_file);
                $datadetail->id_card_image =  $nama_file ;
            }
            $datausers = User::find($id);
            $datausers->phone_no = $request->phone_no;
            $datausers->updated_by_id =Auth::user()->id;
            $datausers->save();

            $datadetail->fullname =$request->fullname;
            $datadetail->email = $request->email;
            $datadetail->address = $request->address;
            $datadetail->birthplace = $request->birthplace;
            $datadetail->birthdate = $request->birthdate;
            $datadetail->occupation = $request->occupation;
            $datadetail->school_id = empty($request->school_id) ? null : $request->school_id;
            $datadetail->salary = empty($request->salary) ? null : $request->salary;
            $datadetail->postcode_id = $request->postcode_id;
            $datadetail->updated_by_id = Auth::user()->id;
            $datadetail->save();
            // $t = 0;
            // foreach($request->role_id1 as $key => $roleid){
            //     DB::insert('insert into roles_users (role_id, user_id, created_by_id) values (?, ?, ?)', [$roleid, $id, Auth::user()->id]);
            //     DB::insert('insert into institution_users (role_id, user_id, institution_id) values (?, ?, ?)', [$roleid, $id,  $request->institution_id[$roleid]]);
                
            //     $t++;
            // }
            if ($request->hasFile('path')) {
                $i = 0;
                foreach($request->file('path') as $file2){
                    $photo = new Salary;
                    $imageName = time() . $i . '.' . $file2->extension();
                    $file2->move(public_path().'/images/', $imageName); 
                    $photo->path = $imageName;
                    $photo->user_id = $id;
                    $photo->created_by_id =Auth::user()->id;
                    $photo->save();
                    $i++;
                } 
            }
            
        });
        return redirect('/user')->with('success', 'User baru berhasil dibuat');
    }
    public function detail($id)
    {
        $salary = Salary::where('user_id', '=', $id)->get();
        $users = User::find($id);
        $picid = User::find($id);
        $user_detail = UserDetail::where('parent_id', '=', $id)->get();
        return view('master_data.user.detail', compact('users','salary','picid','user_detail'));
    }
    public function create_student($id)
    {
        $user_details = new UserDetail();
        $users = new User();
        $province = Province::all();
        $city = City::all();
        $district = District::all();
        $postcode = Postcode::all();
        $Schools = School::all();
        $SchoolGrades = SchoolGrade::all();
        return view('master_data.user.form_student', compact('id','users','user_details','province','city','district','postcode','Schools','SchoolGrades'));
    }
    public function store_student(Request $request)
    {
        $this->validate($request,[
            'nik' => 'required|unique:users|max:255',
            'phone_no' => 'required|unique:users|max:255',
            'email' => 'required|unique:user_details|max:255',
            'fullname' => 'required',
            'birthdate' =>'required',
            'postcode_id' => 'required',
        ]);
        DB::transaction(function () use ($request) {
            $datausers = new User();
            $datadetail = new UserDetail();
            $datarole = new UserRole();
            if ($request->hasFile('id_card_image')) {
                $file = $request->file('id_card_image');
                $nama_file = time().".".$file->extension();
                $tujuan_upload = public_path('images');
                $file->move($tujuan_upload,$nama_file);
                $datadetail->id_card_image =  $nama_file ;
            }
            else{
                $datadetail->id_card_image =  "empty_identity.png";
            }
            $datausers->nik = $request->nik;
            $datausers->password_digest = bcrypt('cluever');
            $datausers->phone_no = $request->phone_no;
            $datausers->created_by_id =Auth::user()->id;
            $datausers->save();
            $datadetail->user_id =$datausers->id;
            $datadetail->fullname =$request->fullname;
            $datadetail->email = $request->email;
            $datadetail->address = $request->address;
            $datadetail->school_id = $request->school_id;
            $datadetail->self_image = 'user.png';
            $datadetail->birthplace = $request->birthplace;
            $datadetail->birthdate = $request->birthdate;
            $datadetail->occupation = $request->occupation;
            $datadetail->salary = $request->salary;
            $datadetail->postcode_id = $request->postcode_id;
            $datadetail->user_status_id = "1";
            $datadetail->created_by_id = Auth::user()->id;
            $datadetail->parent_id = $request->parent_id;
            $datadetail->save();
            DB::insert('insert into roles_users (role_id, user_id, created_by_id) values (?, ?, ?)', [7, $datausers->id, Auth::user()->id]);
            DB::insert('insert into institution_users (role_id, user_id, institution_id) values (?, ?, ?)', [7, $datausers->id,  Auth::user()->institution_user->institution_id]);
            
        });
        return redirect()->action('UserController@detail',['id' => $request->parent_id]);
        // return back()->with('success', "Siswa berhasil dibuat");
    }
    public function edit_student($id)
    {
        $users = User::find($id);
        $user_details = new UserDetail();
        $province = Province::all();
        $city = City::all();
        $district = District::all();
        $postcode = Postcode::all();
        return view('master_data.user.form_student', compact('id','users','user_details','province','city','district','postcode'));
    }

    public function update_student(Request $request, $id)
    {
        $this->validate($request,[
            'fullname' => 'required',
            'email' => 'required|email|unique:user_details,email,'.$id,
            'phone_no' => 'required|unique:users,phone_no,'.$id,
            'postcode_id' => 'required',
        ]);
        DB::transaction(function () use ($request, $id){
            $datadetail = UserDetail::where('user_id','=',$id)->first();
            if ($request->hasFile('id_card_image')) {
                $file = $request->file('id_card_image');
                $nama_file = time().".".$file->extension();
                $tujuan_upload = public_path('images');
                $file->move($tujuan_upload,$nama_file);
                $datadetail->id_card_image =  $nama_file ;
            }
            $datausers = User::find($id);
            $datausers->phone_no = $request->phone_no;
            $datausers->updated_by_id =Auth::user()->id;
            $datausers->save();

            $datadetail->fullname =$request->fullname;
            $datadetail->email = $request->email;
            $datadetail->address = $request->address;
            $datadetail->school_id = $request->school_id;
            $datadetail->self_image = 'user.png';
            $datadetail->birthplace = $request->birthplace;
            $datadetail->birthdate = $request->birthdate;
            $datadetail->occupation = $request->occupation;
            $datadetail->salary = $request->salary;
            $datadetail->postcode_id = $request->postcode_id;
            $datadetail->updated_by_id = Auth::user()->id;
            $datadetail->save();
            
        });
        return redirect()->action('UserController@detail',['id' => $request->parent_id]);
    }
    public function changePassword()
    {
        $user = Auth::user();
        return view('master_data.user.change-password', compact('user'));
    }
    //ini function aneh route nya tidak bisa dieksekusi
    public function importexcel(Request $request)
        {
            //VALIDASI
            $this->validate($request, [
                'file' => 'required|mimes:xls,xlsx'
            ]);

            if ($request->hasFile('file')) {
                $file = $request->file('file'); //GET FILE
                Excel::import(new ImportUsers, $file); //IMPORT FILE 
                return redirect()->back()->with(['success' => 'Upload success']);
            }  
            return redirect()->back()->with(['error' => 'Please choose file before']);
        }
    public function updatePassword(Request $request)
    {
        $this->validate($request,[
            'old_password' => 'required',
            'password' => 'required',
            'password_confirm' => 'required|same:password',
        ]);
        if ($request->password != $request->password_confirm) {
            return back()->with('danger', 'Konfirmasi password harus sama dengan password');
        }
        if (Hash::check($request->old_password, Auth::user()->password_digest)) {
            User::findOrFail(Auth::user()->id)
                ->update([ 'password_digest' => Hash::make($request->password) ]);
            return back()->with('success', 'Berhasil mengubah password');
        } else {
            return back()->with('danger', 'Password lama tidak sesuai');
        }
    }
    
}