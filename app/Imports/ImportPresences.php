<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use DB;
use App\Models\Institution;
use App\Models\Presence;
use App\Models\User;
use App\Models\Schedule;
use App\Models\Classes;
use App\Models\Subject;

class ImportPresences implements ToCollection, WithHeadingRow
{
    public function __construct($request)
    {
        $this->request = $request;

    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        DB::transaction(function () use ($rows) 
        {
            foreach ($rows as $row) 
            {
                $idinstitution = $this->request->input('institution_id');
                $day = date("Y-m-d",strtotime(str_replace("/","-",$row['tanggal'])));
                $Student = User::where('nik', 'ILIKE','%'.$row['nik'].'%')->get();
                if($Student->count() > 0){
                    $studentid = $Student->first();
                    $student_id = $studentid->id;
                }
                $Classes = Classes::where('class_code', 'ILIKE','%'.$row['kelas'].'%')->where('institution_id',$idinstitution )->get();
                if($Classes ->count() > 0){
                    $Classesid = $Classes ->first();
                    $class_id = $Classesid->id;
                }
                $Subject = Subject::where('name', 'ILIKE','%'.$row['matpel'].'%')->where('institution_id',$idinstitution )->get();
                if($Subject ->count() > 0){
                    $Subjectid = $Subject ->first();
                    $Subject_id = $Subjectid->id;
                }
                $start = substr($row['jam'],0,5);
                $finish = substr($row['jam'],8,5);

                $Schedule = Schedule::where('class_id', $class_id)->where('start_hour', $start)->where('finish_hour', $finish)->where('subject_id',$Subject_id)->where('day',$day)->get();
                if($Schedule ->count() > 0){
                    $Scheduleid = $Schedule ->first();
                    $Schedule_id = $Scheduleid->id;
                }
                $anak = Presence::create([
                        'student_id' => $student_id,
                        'schedule_id' => $Schedule_id,
                        'status_id' => 1,
                    ]);
            }
        });
    
    }
}
