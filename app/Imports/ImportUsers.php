<?php

namespace App\Imports;

use App\Models\User;
use App\Models\UserDetail;
use App\Models\School;
use App\Models\Classes;
use App\Models\Program;
use App\Models\SchoolYear;
use App\Models\StudentClass;
use App\Models\UserRole;
use App\Models\Institution;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use DB;

class ImportUsers implements ToCollection, WithHeadingRow
{
    public function __construct($request)
    {
        $this->request = $request;

    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
       
        DB::transaction(function () use ($rows) {
            foreach ($rows as $row) 
            {
                // $v = $row->each(function($coloum){ 
                //    dd($v);
                // });
                $idinstitution = $this->request->input('institution_id');
                $institusi = Institution::where('id','=',$idinstitution)->first();
                $a = substr($institusi->name,0,4);
                $date_ortu = date("Y-m-d",strtotime(str_replace("/","-",$row['birthdate'])));
                $date_anak = date("Y-m-d",strtotime(str_replace("/","-",$row['birthdate'])));
                // dd($date_anak);
                // $base_anak =mktime(0,0,0,1,$date_anak-1,1900);
                // $base_ortu =mktime(0,0,0,1,$date_ortu-1,1900); 
                
                $checknik = User::where('nik', '=',$row['nik'])->count();
                if($checknik == 0)
                {
                    $anak = User::create([
                        'nik' => $row['nik'],
                        'password_digest' => bcrypt('cluever'),
                        'phone_no' => $row['phone_no'],
                    ]);
                    $ortu = User::create([
                        'nik' => $row['nik'],
                        'password_digest' => bcrypt('cluever'),
                        'phone_no' => $row['phone_no_ortu'],
                    ]);
                    $school = School::where('name', 'ILIKE','%'.$row['school'].'%')->get();
                    if($school->count() > 0){
                        $school_id = $school->first();
                        $idschool = $school_id->id;

                    }
                    else{
                        $sc = School::create([
                            'name' =>$row['school'],
                        ]);
                        $idschool = $sc->id;
                    }
                    $kelas = Classes::where('class_code', 'ILIKE','%'.$row['class'].'%')->where('institution_id','=',$idinstitution)->get();
                    if($kelas->count() > 0){
                        $kelas_id = $kelas->first();
                        $idkelas = $kelas_id->id;
                    }
                    else{
                        echo "tidak ditemukan";
                    }
                    $anakdetail = UserDetail::create([
                        'user_id' => $anak->id,
                        'fullname' => $row['fullname'],
                        'email' => $row['email'],
                        'address' => $row['address'],
                        'birthplace' => $row['birthplace'],
                        'birthdate' => $date_anak,
                        'school_id' => $idschool,
                        'self_image' => "empty_profpict.png",
                        'user_status_id' => 1,
                    ]);
                    $ortudetail = UserDetail::create([
                        'user_id' => $ortu->id,
                        'fullname' => $row['fullname_ortu'],
                        'self_image' => "empty_profpict.png",
                        'email' => $row['email_ortu'],
                        'address' => $row['address_ortu'],
                        'birthplace' => $row['birthplace_ortu'],
                        'birthdate' => $date_ortu,
                        'user_status_id' => 1,
                    ]);
                    $anakdetail->update(['parent_id' => $ortu->id]);
                    StudentClass::create([
                        'student_id' => $anak->id,
                        'class_id' => $idkelas,
                    ]);
                    DB::insert('insert into roles_users (role_id, user_id) values (?, ?)', [7, $anak->id]);
                    DB::insert('insert into roles_users (role_id, user_id) values (?, ?)', [6, $ortu->id]);
                    DB::insert('insert into institution_users (role_id, user_id, institution_id) values (?, ?, ?)', [7, $anak->id, $idinstitution]);
                    DB::insert('insert into institution_users (role_id, user_id, institution_id) values (?, ?, ?)', [6, $ortu->id, $idinstitution]);
                }

                else{
                    // echo "nik $row['nik'] sudah ada";
                }
            
            }
        });
    }
}
