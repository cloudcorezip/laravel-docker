<?php

namespace App\Mail;
use App\Models\RegistrationProposal;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegistrationProposalEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $proposal;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($proposal)
    {
        $this->proposal = $proposal;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $proposals = RegistrationProposal::all();
        return $this->from('cluever.app@gmail.com','Cluever')->subject('TestMailSubject!')
        ->view('emails.proposal')
        // ->with(
        //     [
        //         'nama' => 'Diki Alfarabi Hadi',
        //         'website' => 'www.malasngoding.com',
        //     ])
        ;

    }
}
