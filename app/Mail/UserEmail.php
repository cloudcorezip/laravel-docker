<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $datadetail;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($datadetail)
    {
        $this->datadetail = $datadetail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('cluever.app@gmail.com','Cluever')
                    ->subject($this->datadetail->fullname.' menjadi')
                    ->view('emails.user');
    }
}
