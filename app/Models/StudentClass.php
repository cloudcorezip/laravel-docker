<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentClass extends Model
{
    protected $table = 'class_students';
    protected $fillable = [ 
        'class_id', 'student_id',
    ];
    public function users() {
        return $this->belongsTo(User::class,'student_id');
    }
    public function classes(){
    	return $this->belongsTo(Classes::class,'class_id');
    }
}
