<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentFlexi extends Model
{
    protected $table = 'student_flexi';
    protected $fillable = [
        'student_id', 'schedule_id',
    ];
    public function schedule() {
        return $this->belongsTo(Schedule::class,'schedule_id');
    }
    public function student(){
        return $this->belongsTo(User::class,'student_id');
    }
    public function users(){
	return $this->belongsTo(User::class,'student_id');
    }
}
