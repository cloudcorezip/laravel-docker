<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolYear extends Model
{
	protected $table = "school_years";
    protected $fillable = [ 
        'name', 'description', 'institution_id'];

    public function classes() {
		return $this->hasMany(Classes::class);
	}
	public function institutions() {
		return $this->belongsTo(Institution::class,'institution_id');
	}
}
