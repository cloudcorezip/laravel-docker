<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $table = 'user_details';
    protected $fillable = [ 
        'user_id', 'fullname','email', 'address','birthplace','birthdate','parent_id','salary','occupation','id_card_image','self_image','user_status_id','created_by_id','school_id'
    ];
    public function users()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function user_roles()
    {
        return $this->hasMany(UserRole::class,'user_id');
    }
    public function postcode()
    {
        return $this->belongsTo(Postcode::class,'postcode_id');
    }
}
