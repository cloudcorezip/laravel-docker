<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    protected $table = "classes";
    protected $fillable = [ 
        'class_code', 'class_name', 'program_id','homeroom_teacher_id','institution_id','school_year_id'
    ];
    public function programs() {
		return $this->belongsTo(Program::class,'program_id');
	}
	public function homeroom_teacher() {
		return $this->belongsTo(User::class,'homeroom_teacher_id');
	}
    public function schedule(){
        return $this->hasMany(Schedule::class,'class_id');
    }
    public function student_class(){
        return $this->hasMany(StudentClass::class,'class_id');
    }
	public function students(){
    	return $this->hasMany(UserDetail::class,'class_id');
    }
    public function school_year() {
        return $this->belongsTo(SchoolYear::class,'school_year_id');
    }
    public function isFlexi() {
        return $this->programs()->where('name', 'ILIKE', '%Flexi%')->count()>0;
    }
    public function institutions() {
        return $this->belongsTo(Institution::class,'institution_id');
    }
}
