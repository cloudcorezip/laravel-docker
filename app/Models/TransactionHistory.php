<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionHistory extends Model
{
    protected $table = "tx_histories";
    protected $fillable = [
    	'name', 'description', 'tx_status_id', 'user_id', 'create_by_id', 'updated_by_id', 'payment_id', 'midtrans_response'
    ];

    public function user()
    {
		return $this->belongsTo(User::class, 'user_id');
	}
}
