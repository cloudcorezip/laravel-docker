<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstallmentDetailStatus extends Model
{
    protected $table = 'installment_detail_statuses';
    protected $fillable = ['status'];

    public function installmentDetails()
    {
    	return $this->hasMany(InstallmentDetail::class, 'installment_detail_status_id');
    }
}
