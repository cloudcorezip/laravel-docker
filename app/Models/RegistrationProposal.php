<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegistrationProposal extends Model
{
	protected $table = "registration_proposals";
	protected $fillable = ['pic_id','student_id','program_id','application_status_id'];

	public function programs() {
	return $this->belongsTo(Program::class,'program_id');
	}
	public function statuses() {
		return $this->belongsTo(ApplicationStatus::class,'application_status_id');
	}
	public function pic() {
		return $this->belongsTo(User::class,'pic_id');
	}
	public function student() {
		return $this->belongsTo(User::class,'student_id');
	}
}
