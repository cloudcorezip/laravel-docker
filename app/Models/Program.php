<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $table = "programs";
    protected $fillable = ['name','description','price','discount','institution_id'];


    public function applications() {
		return $this->hasMany(Application::class);
	}
	public function proposal() {
		return $this->hasMany(RegistrationProposal::class);
	}
	public function institutions() {
		return $this->belongsTo(Institution::class,'institution_id');
	}
	public function classes() {
		return $this->hasMany(Classes::class);
	}
}
