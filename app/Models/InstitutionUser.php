<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstitutionUser extends Model
{
    protected $table = 'institution_users';
    protected $fillable = ['id','user_id','institution_id','role_id'];

    public function users() {
        return $this->belongsTo(User::class,'user_id');
    }
}
