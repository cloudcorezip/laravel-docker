<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
	use Notifiable;
	use HasRoles;
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "users";
    protected $guarded = [];
    protected $fillable = [
        'id', 'nik', 'password_digest', 'phone_no', 'created_by_id'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class,'roles_users','user_id','role_id');
    }
    public function user_detail()
    {
        return $this->hasOne(UserDetail::class,'user_id');
    }
    public function institution_user()
    {
        return $this->hasOne(InstitutionUser::class,'user_id');
    }
    public function student_class()
    {
        return $this->hasMany(StudentClass::class);
    }
    public function student_flexi()
    {
        return $this->hasMany(StudentFlexi::class);
    }
    public function presence()
    {
        return $this->hasMany(Presence::class);
    }
    public function user_roles()
    {
        return $this->hasMany(UserRole::class,'id');
    }
    public function applications()
    {
        return $this->hasMany(Application::class, 'pic_id');
    }
    public function proposal() {
        return $this->hasMany(RegistrationProposal::class);
    }
    public function clasess()
    {
        return $this->belongsToMany(Classes::class, 'class_students','student_id','class_id');
    }
    public function schedules()
    {
        return $this->hasMany(Schedule::class, 'educator_id');
    }
    public function salary()
    {
        return $this->hasMany(Salary::class);
    }
    public function hasAnyRole($roles)
    {
        if (is_array($roles)){
            foreach ($roles as $role) {
                if($this->hasRole($role)){
                    return true;
                }
            }
        } else {
            if($this->hasRole($roles)){
                return true;
            }
        }
        return false;
    }
    public function hasRole($role)
    {
        if ($this->roles()->where('name',$role)->exists()) {
            return true;
        }
        return false;
    }
    public function getAuthPassword()
    {
        return $this->password_digest;
    }
    public function institutions()
    {
        return $this->belongsToMany(Institution::class, 'institution_users', 'user_id', 'institution_id');
    }
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function configuration()
    {
        return $this->hasOne(UserConfiguration::class, 'user_id');
    }
}
