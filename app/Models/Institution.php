<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Institution extends Model
{
    protected $table = "institutions";
    protected $fillable = ['name','address','website','description','institution_type','logo_path'];

    public function programs()
    {
		return $this->hasMany(Program::class);
	}
	
	public function school_year()
	{
		return $this->hasMany(SchoolYear::class);
	}

	public function schedule_type()
	{
		return $this->hasMany(ScheduleType::class);
	}

	public function subjects()
	{
		return $this->hasMany(Subject::class);
	}

	public function classes()
	{
		return $this->hasMany(Classes::class);
	}

	public function postcode()
	{
        return $this->belongsTo(Postcode::class,'postcode_id');
    }

	public function users()
	{
		return $this->belongsToMany(User::class, 'institution_users', 'user_id', 'institution_id');
	}

	public function images()
	{
		return $this->hasMany(InstitutionImage::class, 'institution_id');
	}
}
