<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Presence extends Model
{
    protected $table = "presences";
    protected $fillable = ['id','student_id','schedule_id','status_id'];

    public function status()
    {
    	return $this->belongsTo(PresenceStatus::class, 'status_id');
    }
    public function scopeIsPresented($query, $scheduleId, $studentId)
    {
    	return $query->where('schedule_id', $scheduleId)
    		->where('student_id', $studentId);
    }
    public function students() {
        return $this->belongsTo(User::class,'student_id');
    }
    public function schedule() {
        return $this->belongsTo(Schedule::class,'schedule_id');
    }
}
