<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicationStatus extends Model
{
    protected $table = "application_statuses";
    protected $fillable = ['description'];

   	public function applications() {
		return $this->hasMany(Application::class);
	}
	public function proposal() {
		return $this->hasMany(RegistrationProposal::class);
	}
}
