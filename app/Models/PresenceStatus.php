<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PresenceStatus extends Model
{
    protected $table = "presence_statuses";
    protected $fillable = [
        'name', 'description'
    ];

    public function presence(){
        return $this->hasMany(Presence::class);
    }
}
