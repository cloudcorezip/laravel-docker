<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Postcode extends Model
{
    protected $table = "postcodes";
    protected $fillable = ['code','district_id'];
	
	public function user_detail() {
		return $this->hasMany(UserDetail::class);
	}
	public function institusi() {
		return $this->hasMany(Institution::class);
	}
}
