<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = 'subjects';
    protected $fillable = [ 
        'name', 'institution_id',
    ];
    public function schedules() {
		return $this->hasMany(Schedule::class);
	}
	public function institutions() {
		return $this->belongsTo(Institution::class,'institution_id');
	}
}
