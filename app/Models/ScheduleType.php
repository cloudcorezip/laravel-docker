<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScheduleType extends Model
{
    protected $table = 'schedule_types';
    protected $fillable = [ 
        'name', 'description', 'admin_input','institution_id'
    ];
    public function schedules() {
		return $this->hasMany(Schedule::class);
	}
	public function institutions() {
		return $this->belongsTo(Institution::class,'institution_id');
	}
}
