<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = 'schedules';
    protected $fillable = [ 
        'day', 'start_hour', 'finish_hour','subject_id','educator_id','class_id','schedule_type_id','institution_id','note','capacity','room'
    ];
    public function classes(){
    	return $this->belongsTo(Classes::class,'class_id');
    }
    public function subjects(){
    	return $this->belongsTo(Subject::class,'subject_id');
    }
    public function users(){
    	return $this->belongsTo(User::class,'educator_id');
    }
    public function types(){
        return $this->belongsTo(ScheduleType::class,'schedule_type_id');
    }
    public function student_flexi()
    {
        return $this->hasMany(StudentFlexi::class);
    }
}
