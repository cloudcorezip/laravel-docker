<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstitutionImage extends Model
{
    protected $table = 'institution_images';
    protected $fillable = ['path','institution_id'];

}
