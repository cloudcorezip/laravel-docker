<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table = 'roles_users';
    protected $fillable = ['role_id','user_id'];
    protected $primarykey =['role_id','user_id'];

    public function user_detail() {
        return $this->belongsTo(UserDetail::class,'user_id');
    }
    public function users() {
        return $this->belongsTo(User::class,'user_id');
    }
}
