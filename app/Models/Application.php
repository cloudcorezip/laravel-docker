<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $table = "applications";
    protected $fillable = [
    	'pic_id','student_id','application_status_id','program_id','installment_period','created_by_id','acceptance_msg'
    ];

    public function programs()
    {
		return $this->belongsTo(Program::class, 'program_id');
	}
    public function program()
    {
		return $this->belongsTo(Program::class, 'program_id');
	}
	public function statuses()
	{
		return $this->belongsTo(ApplicationStatus::class, 'application_status_id');
	}
	public function pic()
	{
		return $this->belongsTo(User::class, 'pic_id');
	}
	public function student()
	{
		return $this->belongsTo(User::class, 'student_id');
	}
	public function installments()
	{
		return $this->hasMany(Installment::class, 'application_id');
	}
}
