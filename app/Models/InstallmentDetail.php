<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstallmentDetail extends Model
{
    protected $table = 'installment_details';
    protected $fillable = ['due_date', 'amount','installment_id','installment_detail_status_id'];

    public function status()
    {
    	return $this->belongsTo(InstallmentDetailStatus::class, 'installment_detail_status_id');
    }

    public function installment()
    {
    	return $this->belongsTo(Installment::class, 'installment_id');
    }
}
