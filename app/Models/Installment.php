<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Installment extends Model
{
    protected $table = 'installments';
    protected $fillable = ['ammount_unpaid','application_id','need_update'];

    public function details()
    {
    	return $this->hasMany(InstallmentDetail::class, 'installment_id');
    }

    public function application()
    {
    	return $this->belongsTo(Application::class, 'application_id');
    }
}
