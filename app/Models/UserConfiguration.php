<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserConfiguration extends Model
{
    protected $table = 'user_configurations';
    protected $fillable = [ 
    	'user_id', 'conf_biaya_pendidikan', 'conf_program_pendidikan', 'conf_rapor_harian', 'conf_marketplace', 'conf_antar_jemput' 
    ];
}
