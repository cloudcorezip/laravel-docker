<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = "payments";
    protected $fillable = [
    	'payment_type_id', 'payment_status_id', 'installment_detail_id', 'application_id', 'details', 'program_id', 'amount'
    ];
	
	public function application()
	{
		return $this->belongsTo(\App\Models\Application::class);
	}
	public function program()
	{
		return $this->belongsTo(\App\Models\Program::class);
	}
	public function status()
	{
		return $this->belongsTo(\App\Models\PaymentStatus::class, 'payment_status_id');
	}

}
