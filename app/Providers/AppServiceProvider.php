<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Models\Notification;
use Auth;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    public function boot()
    {
        View::composer('*', function($view)
            {
                if(Auth::check()){
                    $notifs = Notification::where('user_id',Auth::user()->id)->where('is_read',0)->get();
                    View::share('notifs', $notifs );
                }
                else{
                    View::share('notifs', null );
                }
            }
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
