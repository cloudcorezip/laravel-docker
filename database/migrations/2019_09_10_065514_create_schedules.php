<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->time('start_hour');
            $table->time('finish_hour');
            $table->date('day')->nullable();
            $table->string('note')->nullable();
            $table->integer('capacity')->nullable();
            $table->string('class_room')->nullable();
            $table->unsignedBigInteger('subject_id')->nullable();
            $table->unsignedBigInteger('educator_id');
            $table->unsignedBigInteger('class_id')->nullable();
            $table->unsignedBigInteger('schedule_type_id');
            $table->unsignedBigInteger('institution_id');
            $table->foreign('class_id')->references('id')->on('classes');
            $table->foreign('educator_id')->references('id')->on('users');
            $table->foreign('subject_id')->references('id')->on('subjects');
            $table->foreign('schedule_type_id')->references('id')->on('schedule_types');
            $table->foreign('institution_id')->references('id')->on('institutions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
