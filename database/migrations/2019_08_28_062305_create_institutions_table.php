<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstitutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institutions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('postcode_id')->nullable();
            $table->string('address')->nullable();
            $table->string('website')->nullable();
            $table->string('virtual_account')->nullable();
            $table->text('description')->nullable();
            $table->string('logo_path')->nullable();
            $table->unsignedBigInteger('institution_type_id');
            $table->foreign('postcode_id')->references('id')->on('postcodes');
            $table->foreign('institution_type_id')->references('id')->on('institution_types');
            $table->timestamps();
        });
        Schema::table('institutions', function($table) {
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institutions');
    }
}
