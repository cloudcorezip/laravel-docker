<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTxHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tx_histories', function (Blueprint $table) {
            $table->bigIncrements('id',10);
            $table->string('name',255)->nullable();
            $table->string('description')->nullable();
            $table->unsignedBigInteger('tx_status_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('created_by_id')->nullable();
            $table->unsignedBigInteger('updated_by_id')->nullable();
            $table->string('midtrans_response')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('tx_status_id')->references('id')->on('tx_statuses');
            $table->foreign('created_by_id')->references('id')->on('users');
            $table->foreign('updated_by_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tx_histories');
    }
}
