<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('payment_type_id');
            $table->unsignedBigInteger('payment_status_id');
            $table->unsignedBigInteger('installment_detail_id')->nullable();
            $table->unsignedBigInteger('application_id')->nullable();
            $table->unsignedBigInteger('program_id'); // <- sengaja redundan
            $table->foreign('payment_type_id')->references('id')->on('payment_types');
            $table->foreign('payment_status_id')->references('id')->on('payment_statuses');
            $table->foreign('installment_detail_id')->references('id')->on('installment_details');
            $table->foreign('application_id')->references('id')->on('applications');
            $table->foreign('program_id')->references('id')->on('programs');
            $table->integer('amount');
            $table->string('details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
