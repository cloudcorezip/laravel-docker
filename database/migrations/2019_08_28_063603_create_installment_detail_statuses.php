<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstallmentDetailStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('installment_detail_statuses', function (Blueprint $table) {
            $table->BigIncrements('id');
            $table->string('status');
            $table->timestamps();
        });
         Schema::table('installment_detail_statuses', function($table) {
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('installment_detail_statuses');
    }
}
