<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationProposals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registration_proposals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('program_id');
            $table->unsignedBigInteger('pic_id');
            $table->unsignedBigInteger('student_id');
            $table->unsignedBigInteger('created_by_id')->nullable();
            $table->unsignedBigInteger('updated_by_id')->nullable();
            $table->unsignedBigInteger('application_status_id');
            $table->timestamps();
            $table->foreign('application_status_id')->references('id')->on('application_statuses');
            $table->foreign('program_id')->references('id')->on('programs');
            $table->foreign('pic_id')->references('id')->on('users');
            $table->foreign('student_id')->references('id')->on('users');
            $table->foreign('created_by_id')->references('id')->on('users');
            $table->foreign('updated_by_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registration_proposals');
    }
}
