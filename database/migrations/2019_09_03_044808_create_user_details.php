<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('fullname',255);
            $table->string('email',64)->nullable();
            $table->text('address')->nullable();
            $table->string('self_image',255)->null();
            $table->string('id_card_image',255)->nullable();
            $table->string('birthplace',92)->nullable();
            $table->date('birthdate')->nullable();
            $table->string('occupation',92)->nullable();
            $table->integer('salary')->nullable()->default(null);
            $table->unsignedBigInteger('school_id')->nullable();
            $table->unsignedBigInteger('school_grade_id')->nullable();
            $table->unsignedBigInteger('postcode_id')->nullable();
            $table->unsignedBigInteger('user_status_id');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->unsignedBigInteger('created_by_id')->nullable();
            $table->unsignedBigInteger('updated_by_id')->nullable();
            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('school_grade_id')->references('id')->on('school_grades');
            $table->foreign('parent_id')->references('id')->on('users');
            $table->foreign('user_status_id')->references('id')->on('user_statuses');
            $table->foreign('postcode_id')->references('id')->on('postcodes');
            $table->foreign('created_by_id')->references('id')->on('users');
            $table->foreign('updated_by_id')->references('id')->on('users');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
