<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClasses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('class_code');
            $table->string('class_name')->nullable();
            $table->string('capacity')->nullable();
            $table->unsignedBigInteger('program_id');
            $table->unsignedBigInteger('homeroom_teacher_id');
            $table->unsignedBigInteger('institution_id');
            $table->unsignedBigInteger('school_year_id');
            $table->foreign('program_id')->references('id')->on('programs');
            $table->foreign('homeroom_teacher_id')->references('id')->on('users');
            $table->foreign('institution_id')->references('id')->on('institutions');
            $table->foreign('school_year_id')->references('id')->on('school_years');
            $table->timestamps();
        });
        Schema::table('classes', function($table) {
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classes');
    }
}
