<?php

use Illuminate\Database\Seeder;

class UserDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_details')->insert([
            [
                'user_id' => '1',
                'fullname' => 'Ilma Amalia Ainunnajib',
                'email' => 'ilma@gmail.com',
                'address' => 'Jl. Raya Kamasan',
                'self_image' => 'empty_profpict.png',
                'id_card_image' => 'empty_identity.png',
                'birthplace' => 'Tasikmalaya',
                'birthdate' => '1993-06-13',
                'occupation' => 'Karyawan Swasta',
                'salary' => '6000000',
                'postcode_id' => '1',
                'user_status_id' => '1',
                'created_by_id' => '1',
                'updated_by_id' => '1',
            ],
            [
                'user_id' => '2',
                'fullname' => 'Novita Asri Septina Sari',
                'email' => 'novita@gmail.com',
                'address' => 'jl.pungkur',
                'self_image' => 'empty_profpict.png',
                'id_card_image' => 'empty_identity.png',
                'birthplace' => 'Bandung',
                'birthdate' => '1994-05-09',
                'occupation' => 'Karyawan Swasta',
                'salary' => '4000000',
                'postcode_id' => '1',
                'user_status_id' => '1',
                'created_by_id' => '1',
                'updated_by_id' => '1',
            ],
            [
                'user_id' => '3',
                'fullname' => 'Syarif Rokhmat Hidayat, S.Pd ',
                'email' => 'syarif@gmail.com',
                'address' => 'jl.pungkur',
                'self_image' => 'empty_profpict.png',
                'id_card_image' => 'empty_identity.png',
                'birthplace' => 'Bandung',
                'birthdate' => '1994-01-16',
                'occupation' => 'Karyawan Swasta',
                'salary' => '3500000',
                'postcode_id' => '1',
                'user_status_id' => '1',
                'created_by_id' => '1',
                'updated_by_id' => '1',
            ],
            [
                'user_id' => '4',
                'fullname' => 'Amanindita Salma Amira',
                'email' => 'salma@gmail.com',
                'address' => 'jl.pungkur',
                'self_image' => 'empty_profpict.png',
                'id_card_image' => 'empty_identity.png',
                'birthplace' => 'Bandung',
                'birthdate' => '2001-10-21',
                'occupation' => '',
                'salary' => '0',
                'postcode_id' => '1',
                'user_status_id' => '1',
                'created_by_id' => '1',
                'updated_by_id' => '1',
            ],
            [
                'user_id' => '5',
                'fullname' => 'Rinaldi Pradhana. W',
                'email' => 'rinaldi@gmail.com',
                'address' => 'jl.pungkur',
                'self_image' => 'empty_profpict.png',
                'id_card_image' => 'empty_identity.png',
                'birthplace' => 'Tasikmalaya',
                'birthdate' => '1993-06-13',
                'occupation' => '',
                'salary' => '0',
                'postcode_id' => '1',
                'user_status_id' => '1',
                'created_by_id' => '1',
                'updated_by_id' => '1',
            ],
            [
                'user_id' => '6',
                'fullname' => 'Riani Lis Indriarini',
                'email' => 'riani@gmail.com',
                'address' => 'jl.pungkur',
                'self_image' => 'empty_profpict.png',
                'id_card_image' => 'empty_identity.png',
                'birthplace' => 'Bandung',
                'birthdate' => '1996-06-13',
                'occupation' => '',
                'salary' => '0',
                'postcode_id' => '1',
                'user_status_id' => '1',
                'created_by_id' => '1',
                'updated_by_id' => '1',
            ],
        ]);
    }
}
