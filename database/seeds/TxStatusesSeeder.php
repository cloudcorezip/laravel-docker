<?php

use Illuminate\Database\Seeder;

class TxStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tx_statuses')->insert([
            [ 'name' => 'pending' ],
            [ 'name' => 'capture' ],
            [ 'name' => 'deny' ],
            [ 'name' => 'challenge' ],
            [ 'name' => 'error' ],
        ]);
    }
}
