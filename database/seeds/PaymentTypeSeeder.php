<?php

use Illuminate\Database\Seeder;

class PaymentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_types')->insert([
            ['type_name' => 'Biaya Pendidikan'],
            ['type_name' => 'Marketplace'],
        	['type_name' => 'CASH'],
        	['type_name' => 'INSTALLMENT'],
        ]);
    }
}
