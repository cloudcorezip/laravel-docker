<?php

use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            [
                'id' => 26142,
                'name' => 'Bogor',
                'province_id' => 26141,
            ],
            [
                'id' => 26611,
                'name' => 'Sukabumi',
                'province_id' => 26141
            ],
            [
                'id' => 27026,
                'name' => 'Cianjur',
                'province_id' => 26141
            ],
            [
                'id' => 27407,
                'name' => 'Bandung',
                'province_id' => 26141
            ],
            [
                'id' => 27714,
                'name' => 'Garut',
                'province_id' => 26141
            ],
            [
                'id' => 32193,
                'name' => 'Kota Bandung',
                'province_id' => 26141
            ],
        ]);
    }
}
