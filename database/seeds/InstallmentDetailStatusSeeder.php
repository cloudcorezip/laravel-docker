<?php

use Illuminate\Database\Seeder;

class InstallmentDetailStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('installment_detail_statuses')->insert([
            [
                'status' => 'Unpaid',
            ],
            [
                'status' => 'Paid',
            ],
        ]);
    }
}
