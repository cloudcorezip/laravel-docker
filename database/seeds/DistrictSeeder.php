<?php

use Illuminate\Database\Seeder;

class DistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('districts')->insert([
            [
                'id' => 32194,
                'name' => 'SUKASARI',
                'city_id' => 32193,
            ],
            [
                'id' => 32199,
                'name' => 'Coblong',
                'city_id' => 32193,
            ],
            [
                'id' => 32206,
                'name' => 'Babakan Ciparay',
                'city_id' => 32193,
            ],
            [
                'id' => 32213,
                'name' => 'Bojongloa Kaler',
                'city_id' => 32193,
            ],
            [
                'id' => 32219,
                'name' => 'Andir',
                'city_id' => 32193,
            ],
            [
                'id' => 32226,
                'name' => 'Cicendo',
                'city_id' => 32193,
            ],
            [
                'id' => 32233,
                'name' => 'Sukajadi',
                'city_id' => 32193,
            ],
            [
                'id' => 32239,
                'name' => 'Cidadap',
                'city_id' => 32193,
            ],
            [
                'id' => 32243,
                'name' => 'Bandung Wetan',
                'city_id' => 32193,
            ]
        ]);
    }
}
