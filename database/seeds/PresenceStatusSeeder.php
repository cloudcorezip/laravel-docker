<?php

use Illuminate\Database\Seeder;

class PresenceStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('presence_statuses')->insert([
            [
                'name' => 'Hadir',
            ],
            [
                'name' => 'Izin',
            ],
            [
                'name' => 'Sakit',
            ],
            [
                'name' => 'Alpa',
            ],
        ]);
    }
}
