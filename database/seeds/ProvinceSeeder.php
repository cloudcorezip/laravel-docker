<?php

use Illuminate\Database\Seeder;

class ProvinceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('provinces')->insert([
            [
                'id' => 26141,
                'name' => 'Jawa Barat',
            ],
            [
                'id' => 32676,
                'name' => 'Jawa Tengah',
            ],
            [
                'id' => 42385,
                'name' => 'Jawa Timur',
            ],
            [
                'id' => 41863,
                'name' => 'Daerah Istimewa Yogyakarta'
            ],
            [
                'id' => 51578,
                'name' => 'Banten'
            ],
            [
                'id' => 53241,
                'name' => 'Bali'
            ],
            [
                'id' => 54020,
                'name' => 'Nusa Tenggara Barat'
            ],
            [
                'id' => 55065,
                'name' => 'Nusa Tenggara Timur'
            ],
            [
                'id' => 58285,
                'name' => 'Kalimantan Barat'
            ],
            [
                'id' => 60371,
                'name' => 'Kalimantan Tengah'
            ]
        ]);
    }
}
