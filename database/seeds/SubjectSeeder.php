<?php

use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subjects')->insert([
            [
                'name' => 'Matematika',
                'institution_id' => 1,
            ],
            [
                'name' => 'Fisika',
                'institution_id' => 1,
            ],
            [
                'name' => 'Kimia',
                'institution_id' => 1,
            ],
            [
                'name' => 'Biologi',
                'institution_id' => 1,
            ],
            [
                'name' => 'Bahasa Indonesia',
                'institution_id' => 1,
            ],
            [
                'name' => 'Bahasa Inggris',
                'institution_id' => 1,
            ]
        ]);
    }
}
