<?php

use Illuminate\Database\Seeder;

class ProgramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('programs')->insert([
            [
                'name' => 'SUITE',
                'description' => 'Program Untuk SMA',
                'price' => '30000000',
                'discount' => '10',
                'institution_id' => '1',
            ],
            [
                'name' => 'DELUXE',
                'description' => 'Program Untuk SMA & SMP',
                'price' => '15000000',
                'discount' => '10',
                'institution_id' => '1',
            ],
            [
                'name' => 'Velvet',
                'description' => 'Program Untuk SMA & SMP',
                'price' => '10000000',
                'discount' => '10',
                'institution_id' => '1',
            ],
            [
                'name' => 'Flexi Max',
                'description' => 'Program Untuk SMA & SMP',
                'price' => '15000000',
                'discount' => '10',
                'institution_id' => '1',
            ]
        ]);
    }
}
