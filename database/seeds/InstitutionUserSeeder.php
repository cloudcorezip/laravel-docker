<?php

use Illuminate\Database\Seeder;

class InstitutionUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('institution_users')->insert([
            [
                'user_id' => 1,
                'role_id' => 1,
                'institution_id' => 1,
            ],
            [
                'user_id' => 2,
                'role_id' => 2,
                'institution_id' => 1,
            ],
            [
                'user_id' => 3,
                'role_id' => 4,
                'institution_id' => 1,
            ],
            [
                'user_id' => 4,
                'role_id' => 7,
                'institution_id' => 1,
            ],
            [
                'user_id' => 3,
                'role_id' => 5,
                'institution_id' => 1,
            ],
            [
                'user_id' => 5,
                'role_id' => 7,
                'institution_id' => 1,
            ],
            [
                'user_id' => 6,
                'role_id' => 3,
                'institution_id' => 1,
            ],

        ]);
    }
}
