<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'Admin Cluever',
                'created_by_id' => '1',
            ],
            [
                'name' => 'Admin Institusi',
                'created_by_id' => '1',
            ],
            [
                'name' => 'Verifikator',
                'created_by_id' => '1',
            ],
            [
                'name' => 'Educator',
                'created_by_id' => '1',
            ],
            [
                'name' => 'Wali Kelas',
                'created_by_id' => '1',
            ],
            [
                'name' => 'Orang Tua',
                'created_by_id' => '1',
            ],
            [
                'name' => 'Siswa',
                'created_by_id' => '1',
            ],
        ]);
    }
}
