<?php

use Illuminate\Database\Seeder;

class ScheduleTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schedule_types')->insert([
            ['name' => 'Regular', 'admin_input' => 1, 'institution_id' => 1],
            ['name' => 'Roadshow', 'admin_input' => 0, 'institution_id' => 1],
            ['name' => 'Tambahan', 'admin_input' => 1, 'institution_id' => 1],
        ]);
    }
}
