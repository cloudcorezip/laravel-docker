<?php

use Illuminate\Database\Seeder;

class SalarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('salaries')->insert([
            [
                'path' => 'empty_slip.png',
                'user_id' => '2',
                'created_by_id' => '1',
            ],
            [
                'path' => 'empty_slip.png',
                'user_id' => '2',
                'created_by_id' => '1',
            ],
        ]);
    }
}
