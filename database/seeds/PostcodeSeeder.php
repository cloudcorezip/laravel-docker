<?php

use Illuminate\Database\Seeder;

class PostcodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('postcodes')->insert([
            [
                'code' => '40135',
                'name' => 'Cipaganti',
                'district_id' => 32194,
            ],
            [
                'code' => '40154',
                'name' => 'Dago',
                'district_id' => 32194,
            ],
            [
                'code' => '40151',
                'name' => 'Lebak Siliwangi',
                'district_id' => 32194,
            ],
            [
                'code' => '40152',
                'name' => 'Lebakgede (Lebak Gede)',
                'district_id' => 32194,
            ],
            [
                'code' => '40131',
                'name' => 'Sadangserang (Sadang Serang)',
                'district_id' => 32199,
            ],
            [
                'code' => '40135',
                'name' => 'a',
                'district_id' => 32199,
            ],
            [
                'code' => '40132',
                'name' => 'f',
                'district_id' => 32199,
            ],
            [
                'code' => '40132',
                'name' => 'd',
                'district_id' => 32199,
            ],
            [
                'code' => '40133',
                'name' => 'nnn',
                'district_id' => 32199,
            ],
            [
                'code' => '40134',
                'name' => 'sh',
                'district_id' => 32199,
            ],
        ]);
    }
}
