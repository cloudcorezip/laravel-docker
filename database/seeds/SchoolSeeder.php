<?php

use Illuminate\Database\Seeder;

class SchoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schools')->insert([
            ['name' => 'SMA 4 Bandung'],
            ['name' => 'SMA 2 Bandung'],
        ]);
    }
}
