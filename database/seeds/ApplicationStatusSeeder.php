<?php

use Illuminate\Database\Seeder;

class ApplicationStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('application_statuses')->insert([
            [
                'id' => 1,
                'name' => 'Menunggu Verifikasi',
                'created_by_id' => '1',
                'updated_by_id' => '1',
            ],
            [
                'id' => 2,
                'name' => 'Diterima',
                'created_by_id' => '1',
                'updated_by_id' => '1',
            ],
            [
                'id' => 3,
                'name' => 'Ditolak',
                'created_by_id' => '1',
                'updated_by_id' => '1',
            ]
        ]);
    }
}
