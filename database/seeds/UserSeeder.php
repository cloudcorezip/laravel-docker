<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'nik' => '3204135306930002',
                'password_digest' => '$2a$10$P/wHYof2RX8yntx6clk2ye8C4F25rrfPW7mEvMzZVfgsgpqpIECdm',
                'phone_no' => '085211377442',
                'created_by_id' => '1',
                'updated_by_id' => '1',
            ],
            [
                'nik' => '3309074509940001',
                'password_digest' => '$2y$10$P/wHYof2RX8yntx6clk2ye8C4F25rrfPW7mEvMzZVfgsgpqpIECdm',
                'phone_no' => '085211377445',
                'created_by_id' => '1',
                'updated_by_id' => '1',
            ],
            [
                'nik' => '320348585178546',
                'password_digest' => '$2y$10$P/wHYof2RX8yntx6clk2ye8C4F25rrfPW7mEvMzZVfgsgpqpIECdm',
                'phone_no' => '085211377445',
                'created_by_id' => '1',
                'updated_by_id' => '1',
            ],
            [
                'nik' => '320308585178546',
                'password_digest' => '$2y$10$P/wHYof2RX8yntx6clk2ye8C4F25rrfPW7mEvMzZVfgsgpqpIECdm',
                'phone_no' => '085211377445',
                'created_by_id' => '1',
                'updated_by_id' => '1',
            ],
            [
                'nik' => '320308585178544',
                'password_digest' => '$2y$10$P/wHYof2RX8yntx6clk2ye8C4F25rrfPW7mEvMzZVfgsgpqpIECdm',
                'phone_no' => '085211377445',
                'created_by_id' => '1',
                'updated_by_id' => '1',
            ],
            [
                'nik' => '3204135306930003',
                'password_digest' => '$2y$10$P/wHYof2RX8yntx6clk2ye8C4F25rrfPW7mEvMzZVfgsgpqpIECdm',
                'phone_no' => '083822481705',
                'created_by_id' => '1',
                'updated_by_id' => '1',
            ],
        ]);
    }
}
