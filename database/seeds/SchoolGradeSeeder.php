<?php

use Illuminate\Database\Seeder;

class SchoolGradeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('school_grades')->insert([
        	['grade' => '7', 'level' => 'SMP', 'major' =>'' ],
        	['grade' => '8', 'level' => 'SMP', 'major' =>'' ],
        	['grade' => '9', 'level' => 'SMP', 'major' =>'' ],
            ['grade' => '10', 'level' => 'SMA', 'major' => 'IPA' ],
            ['grade' => '11', 'level' => 'SMA', 'major' => 'IPA' ],
            ['grade' => '12', 'level' => 'SMA', 'major' => 'IPA' ],
            ['grade' => '10', 'level' => 'SMA', 'major' => 'IPS' ],
            ['grade' => '11', 'level' => 'SMA', 'major' => 'IPS' ],
            ['grade' => '12', 'level' => 'SMA', 'major' => 'IPS' ],
        ]);
    }
}
