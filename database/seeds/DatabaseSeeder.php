<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call(ProvinceSeeder::class);
        $this->call(CitySeeder::class);
        $this->call(DistrictSeeder::class);
        $this->call(PostcodeSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(UserStatusSeeder::class);
        $this->call(UserDetailSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(RoleUserSeeder::class);
        $this->call(InstitutionTypeSeeder::class);
        $this->call(InstitutionSeeder::class);
        $this->call(InstitutionUserSeeder::class);
        $this->call(InstallmentDetailStatusSeeder::class);
        $this->call(PresenceStatusSeeder::class);
        $this->call(ProgramSeeder::class);
        $this->call(SalarySeeder::class);
        $this->call(PaymentStatusSeeder::class);
        $this->call(PaymentTypeSeeder::class);
        $this->call(ScheduleTypesSeeder::class);
        $this->call(SchoolGradeSeeder::class);
        $this->call(SchoolSeeder::class);
        $this->call(SchoolYearSeeder::class);
        $this->call(SubjectSeeder::class);
        $this->call(TxStatusesSeeder::class);
        $this->call(ApplicationStatusSeeder::class);
    }
}