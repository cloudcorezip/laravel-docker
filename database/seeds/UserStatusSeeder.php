<?php

use Illuminate\Database\Seeder;

class UserStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_statuses')->insert([
            [
                'name' => 'Active',
                'created_by_id' => '1',
            ],
            [
                'name' => 'Not Verified',
                'created_by_id' => '1',
            ],
            [
                'name' => 'Suspended',
                'created_by_id' => '1',
            ],
        ]);
    }
}
