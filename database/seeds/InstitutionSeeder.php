<?php

use Illuminate\Database\Seeder;

class InstitutionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('institutions')->insert([
            [
                'name' => 'Edulab',
                'address' => 'Jl. Kalimantan ',
                'description' => 'PT Kreasi Edulab Indonesia',
                'institution_type_id' => 1,
                'logo_path' => "empty_instlogo.png",
            ],
            [
                'name' => 'SSC',
                'address' => 'Jl. Kalimantan',
                'description' => 'SSC',
                'institution_type_id' => 1,
                'logo_path' => "empty_instlogo.png",
            ],
            [
                'name' => 'Tridaya',
                'address' => 'JL. Kalimantan',
                'description' => 'Tridaya',
                'institution_type_id' => 1,
                'logo_path' => "empty_instlogo.png",
            ],
        ]);
    }
}
