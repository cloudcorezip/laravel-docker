<?php

use Illuminate\Database\Seeder;

class SchoolYearSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('school_years')->insert([
            [
                'name' => '2019/2020',
                'institution_id' => 1
            ],
        ]);
    }
}
