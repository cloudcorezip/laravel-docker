<?php

use Illuminate\Database\Seeder;

class PaymentStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_statuses')->insert([
            ['name' => 'Pending'],
            ['name' => 'Paid'],
            ['name' => 'Expired'],
            ['name' => 'Problem'],
            ['name' => 'Refund'],
        ]);
    }
}
